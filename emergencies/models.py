from __future__ import unicode_literals

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from contacts.models import Address, Contact


@python_2_unicode_compatible
class FirstAidAssociation(models.Model):
    name = models.CharField(_('name'), max_length=200)
    website = models.URLField(_('website'), max_length=200, blank=True)
    email = models.EmailField(_('e-mail'), max_length=200)
    address = GenericRelation(Address, verbose_name=_('address'))
    contact = GenericRelation(Contact, verbose_name=_('contact'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('first aid association')
        verbose_name_plural = _('first aid associations')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class PublicAssistance(models.Model):
    name = models.CharField(_('name'), max_length=200)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('public assistance')
        verbose_name_plural = _('public assistances')

    def __str__(self):
        return self.name
