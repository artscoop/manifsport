from __future__ import unicode_literals

import factory

from .models import FirstAidAssociation, PublicAssistance


class FirstAidAssociationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = FirstAidAssociation

    name = 'Croix Rouge'


class PublicAssistanceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = PublicAssistance

    name = ''
