from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EmergenciesConfig(AppConfig):
    name = 'emergencies'
    verbose_name = _('Emergencies')
