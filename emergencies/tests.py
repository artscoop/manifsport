from __future__ import unicode_literals

from django.test import TestCase

from .factories import FirstAidAssociationFactory
from .factories import PublicAssistanceFactory


class FirstAidAssociationMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the association
        '''
        first_aid_association = FirstAidAssociationFactory.build(
            name='Ordre de Malte',
        )
        self.assertEqual(first_aid_association.__str__(), 'Ordre de Malte')


class PublicAssistanceMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the public assistance
        '''
        public_assistance = PublicAssistanceFactory.build(name='Gendarmerie')
        self.assertEqual(public_assistance.__str__(), 'Gendarmerie')
