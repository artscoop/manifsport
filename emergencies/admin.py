from __future__ import unicode_literals

from django.contrib import admin

from contacts.admin import AddressInline, ContactInline

from .models import FirstAidAssociation
from .models import PublicAssistance


class FirstAidAssociationAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [
        AddressInline,
        ContactInline,
    ]


admin.site.register(FirstAidAssociation, FirstAidAssociationAdmin)
admin.site.register(PublicAssistance)
