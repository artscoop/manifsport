# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FirstAidAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='name')),
                ('website', models.URLField(verbose_name='website', blank=True)),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
            ],
            options={
                'verbose_name': 'first aid association',
                'verbose_name_plural': 'first aid associations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PublicAssistance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='name')),
            ],
            options={
                'verbose_name': 'public assistance',
                'verbose_name_plural': 'public assistances',
            },
            bases=(models.Model,),
        ),
    ]
