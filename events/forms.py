from __future__ import unicode_literals

import copy

from itertools import groupby
from operator import attrgetter

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.forms.widgets import SelectMultiple
from django.utils.translation import ugettext_lazy as _

from ddcs_loire.utils import GenericForm

from bootstrap3_datetime.widgets import DateTimePicker

from localflavor.fr.forms import FRPhoneNumberField

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML

from administrative_division.models import Commune

from sports.models import Activity

from .models import AdditionalData
from .models import Promoter, Structure, StructureType
from .models import DeclaredNonMotorizedEvent
from .models import AuthorizedNonMotorizedEvent
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace


class StructureForm(GenericForm):
    phone = FRPhoneNumberField()

    class Meta:
        model = Structure
        exclude = ('promoter', )

    def __init__(self, *args, **kwargs):
        super(StructureForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', _('save').title()))


class UserUpdateForm(GenericForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', _('save').title()))


class SignupForm(forms.Form):
    first_name = forms.CharField(
        max_length=30,
        label=_('declarer first name').capitalize(),
    )
    last_name = forms.CharField(
        max_length=30,
        label=_('declarer last name').capitalize(),
    )
    cgu = forms.BooleanField(
        label=_('by checking this box, i accept those CGUs').capitalize(),
    )

    # structure fields
    structure_name = forms.CharField(
        label=_('structure name').capitalize(),
        help_text=_('structure name helptext').capitalize(),
        max_length=200,
    )
    type_of_structure = forms.ModelChoiceField(
        label=_('structure form').capitalize(),
        help_text=_('structure form helptext').capitalize(),
        queryset=StructureType.objects.all(),
    )
    address = forms.CharField(
        label=_('address').capitalize(),
        max_length=255,
    )
    commune = forms.ModelChoiceField(
        label=_('commune').capitalize(),
        queryset=Commune.objects.filter(
            arrondissement__department__name=settings.SITE_ID,
        ),
    )
    phone = forms.CharField(
        label=_('phone').capitalize(),
        max_length=14,
    )
    website = forms.URLField(
        label=_('website').capitalize(),
        max_length=200,
        required=False
    )

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['phone'] = FRPhoneNumberField()
        self.fields['username'].help_text = _('username will be used as login id')
        self.helper.layout = Layout(
            Fieldset(
                _('promoter').capitalize(),
                HTML("<div class='well'>{text}</div>".format(
                    text=_('name and firstname helptext')
                ).capitalize()),
                'first_name',
                'last_name',
                'username',
                'email',
                'password1',
                'password2',
                HTML(
                    "<div class='well'>{0} : <br/>"
                    " - <a href='{1}' target='_blank'>{2}</a><br/>"
                    " - <a href='{3}' target='_blank'>{4}</a><br/>"
                    "</div>".format(
                        _('general conditions of use').title(),
                        '/CGU/manifestationsportiveloire',
                        'pour la plateforme manifestationsportive.fr',
                        '/CGU/openrunner',
                        'pour la plateforme openrunner.com',
                    )
                ),
                'cgu',
            ),
            Fieldset(
                _('structure').capitalize(),
                'structure_name',
                'type_of_structure',
                'address',
                'commune',
                'phone',
                'website',
            )
        )
        self.helper.add_input(Submit('submit', _('sign up').title()))

    def clean_structure_name(self):
        structure_name = self.cleaned_data['structure_name']
        if Structure.objects.filter(name=structure_name).exists():
            raise forms.ValidationError(
                _('name "{name}" is already in use.'.format(
                    name=structure_name
                ))
            )
        return structure_name

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.username = self.cleaned_data['username']
        user.email = self.cleaned_data['email']
        cgu = self.cleaned_data['cgu']
        promoter = Promoter(user=user, cgu=cgu)
        promoter.save()
        Structure.objects.create(
            name=self.cleaned_data['structure_name'],
            type_of_structure=self.cleaned_data['type_of_structure'],
            phone=self.cleaned_data['phone'],
            website=self.cleaned_data['website'],
            promoter=promoter,
            address=self.cleaned_data['address'],
            commune=self.cleaned_data['commune'],
        )


class ActivityChoiceField(forms.ModelChoiceField):
    '''
    We override this class in order to display activity sports as optgroups
    '''

    def __init__(self, queryset, **kwargs):
        super(ActivityChoiceField, self).__init__(queryset, **kwargs)

        groups = groupby(queryset, attrgetter('sport'))
        self.choices = (
            (sport, [(a.id, self.label_from_instance(a)) for a in activities])
            for sport, activities in groups
        )


FIELDS = Layout(
    Fieldset(
        _('event').capitalize(),
        'name',
        'begin_date',
        'end_date',
        'description',
        'activity',
        'registered_calendar',
        'is_on_rnr',
        'departure_city',
        'other_departments_crossed',
        'crossed_cities',
        'openrunner_route',
        'number_of_entries',
        'max_audience',
    ),
    Fieldset(
        _('road management').capitalize(),
        'oneway_roads',
        'closed_roads',
    ),
    Fieldset(
        _('files to attache - Do not use accent in the file names').capitalize(),
        'event_rules',
        'promoter_commitment',
        'insurance_certificate',
        'safety_provisions',
        'doctor_attendance',
        'additional_docs',
        'rounds_safety',
    ),
    Fieldset(
        _('markup convention').capitalize(),
        'markup_convention',
    ),
    Fieldset(
        _('type of markup used').capitalize(),
        'chalk_markup',
        'rubalise_markup',
        'bio_rubalise_markup',
        'paneling_markup',
        'ground_markup',
        'sticker_markup',
        'lime_markup',
        'plaster_markup',
        'confetti_markup',
        'paint_markup',
        'acrylic_markup',
        'withdrawn_markup',
    ),
    Fieldset(
        _('natura 2000 impact assessment').capitalize(),
        'big_budget',
        'lucrative',
        'big_title',
        'motor_on_natura2000',
        'motor_on_closed_road',
        'approval_request',
    )
)

ANM_FIELDS = copy.deepcopy(FIELDS)
ANM_FIELDS[0].append('multisport_federation')
ANM_FIELDS[2].pop(6)
ANM_FIELDS[2].append('signalers_list')
ANM_FIELDS[5].pop(5)
ANM_FIELDS[5].pop(4)
ANM_FIELDS[5].pop(3)

DNM_FIELDS = copy.deepcopy(FIELDS)
DNM_FIELDS[0].extend(['grouped_traffic', 'support_vehicles_number'])
DNM_FIELDS[2].pop(6)
DNM_FIELDS[5].pop(5)
DNM_FIELDS[5].pop(4)
DNM_FIELDS[5].pop(3)
DNM_FIELDS[5].pop(2)

MC_FIELDS = copy.deepcopy(FIELDS)
MC_FIELDS[0].extend(['vehicles', 'big_concentration'])
MC_FIELDS[2].append('cartography')
MC_FIELDS[5].pop(5)
MC_FIELDS[5].pop(4)

ME_FIELDS = copy.deepcopy(FIELDS)
ME_FIELDS[0].append('vehicles')
ME_FIELDS[2].extend(['delegate_federation_agr', 'public_zone_map',
                     'commissioners', 'tech_promoter_certificate',
                     'hourly_itinerary', 'cartography'])
ME_FIELDS[5].pop(5)
ME_FIELDS[5].pop(4)

MR_FIELDS = copy.deepcopy(FIELDS)
MR_FIELDS[0].append('support')
MR_FIELDS[2].extend(['mass_map', 'cartography'])
MR_FIELDS[5].pop(5)
MR_FIELDS[5].pop(3)


class EventForm(GenericForm):
    activity = ActivityChoiceField(
        queryset=Activity.objects.all().order_by('sport'),
        label=_('activity').capitalize(),
    )

    def __init__(self, *args, **kwargs):
        self.routes = kwargs.pop('routes', None)
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['openrunner_route'].widget.choices = self.routes
        if not settings.DEPARTMENT_SETTINGS['OPENRUNNER_MAP_OPTIONAL']:
            self.fields['openrunner_route'].required = True
        self.helper.form_tag = False

    def clean_openrunner_route(self):
        openrunner_route = self.cleaned_data['openrunner_route']
        # Here we transform the route list into a suite of integers
        # seraparated by "," in order to match CommaSeparatedInteger
        # field where it is stored
        return ','.join([route.strip("u'") for route in openrunner_route.strip("[]").split(', ')])

    def clean(self):
        cleaned_data = super(EventForm, self).clean()
        begin_date = cleaned_data.get('begin_date')
        end_date = cleaned_data.get('end_date')

        if begin_date and end_date:
            if begin_date >= end_date:
                message = _('end date must be greater than begin date')
                self._errors["end_date"] = self.error_class([message])

        return cleaned_data


WIDGETS = {
    'begin_date': DateTimePicker(
        options={"format": "DD/MM/YYYY HH:mm",
                 "pickSeconds": False}),
    'end_date': DateTimePicker(
        options={"format": "DD/MM/YYYY HH:mm",
                 "pickSeconds": False}),
    'openrunner_route': SelectMultiple(
        attrs={"size": 10}),
    'other_departments_crossed': SelectMultiple(
        attrs={"size": 10}),
    'crossed_cities': SelectMultiple(
        attrs={"size": 15}),
}


class AuthorizedNonMotorizedEventForm(EventForm):

    class Meta:
        model = AuthorizedNonMotorizedEvent
        exclude = ('structure', 'approval_request',
                   'motor_on_closed_road', 'motor_on_natura2000',
                   'rounds_safety')
        widgets = WIDGETS

    def __init__(self, *args, **kwargs):
        super(AuthorizedNonMotorizedEventForm, self).__init__(*args, **kwargs)
        self.helper.layout = ANM_FIELDS


class DeclaredNonMotorizedEventForm(EventForm):

    class Meta:
        model = DeclaredNonMotorizedEvent
        exclude = ('structure', 'big_title', 'approval_request',
                   'motor_on_closed_road', 'motor_on_natura2000',
                   'rounds_safety')
        widgets = WIDGETS
        help_texts = {
            'promoter_commitment': _(
                """a template is available <a target='_blank' """
                """href='/engagement_organisateur/declaration/'>here</a>"""
            )
        }

    def __init__(self, *args, **kwargs):
        super(DeclaredNonMotorizedEventForm, self).__init__(*args, **kwargs)
        self.helper.layout = DNM_FIELDS


class MotorizedConcentrationForm(EventForm):

    class Meta:
        model = MotorizedConcentration
        exclude = ('structure', 'approval_request',
                   'motor_on_closed_road')
        widgets = WIDGETS
        help_texts = {
            'promoter_commitment': _(
                """a template is available : <br />- <a target='_blank' """
                """href='/engagement_organisateur/declaration/'>here</a> """
                """for little event or <br />"""
                """- <a target='_blank'"""
                """href='/engagement_organisateur/autorisation/'>here</a> """
                """for big events event"""
            )
        }

    def __init__(self, *args, **kwargs):
        super(MotorizedConcentrationForm, self).__init__(*args, **kwargs)
        self.helper.layout = MC_FIELDS


class MotorizedEventForm(EventForm):

    class Meta:
        model = MotorizedEvent
        exclude = ('structure', 'approval_request',
                   'motor_on_closed_road')
        widgets = WIDGETS

    def __init__(self, *args, **kwargs):
        super(MotorizedEventForm, self).__init__(*args, **kwargs)
        self.helper.layout = ME_FIELDS


class MotorizedRaceForm(EventForm):

    class Meta:
        model = MotorizedRace
        exclude = ('structure', 'approval_request',
                   'motor_on_natura2000')
        widgets = WIDGETS

    def __init__(self, *args, **kwargs):
        super(MotorizedRaceForm, self).__init__(*args, **kwargs)
        self.helper.layout = MR_FIELDS


class AdditionalDataRequestForm(GenericForm):

    class Meta:
        model = AdditionalData
        fields = ('desired_information', )

    def __init__(self, *args, **kwargs):
        super(AdditionalDataRequestForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', _('request information').title()))


class AdditionalDataProvideForm(GenericForm):

    class Meta:
        model = AdditionalData
        fields = ('attached_document', )

    def __init__(self, *args, **kwargs):
        super(AdditionalDataProvideForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', _('provide document').title()))

    def clean_attached_document(self):
        data = self.cleaned_data['attached_document']
        if data is None:
            raise forms.ValidationError(_('You must select a document to upload'))
        return data
