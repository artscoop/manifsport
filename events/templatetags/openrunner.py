from django import template
from django.conf import settings
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def url(value):
    return ''.join([settings.OPENRUNNER_HOST,
                    settings.OPENRUNNER_ROUTE_DISPLAY,
                    value])
