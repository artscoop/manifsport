from __future__ import unicode_literals

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render

from allauth.account.decorators import verified_email_required

from .models import Promoter, Event


def promoter_required(function=None,
                      login_url=None,
                      redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Restricts page access to promoters only
    """
    def decorator(view_func):
        @verified_email_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.promoter
            except Promoter.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def owner_required(function=None,
                   login_url=None,
                   redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Restricts page access to the owner of the event
    """
    def decorator(view_func):
        @promoter_required()
        def _wrapped_view(request, *args, **kwargs):
            if request.user == Event.objects.get(pk=kwargs['pk']).structure.promoter.user:  # noqa
                return view_func(request, *args, **kwargs)
            else:
                return render(request, 'events/access_restricted.html')
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
