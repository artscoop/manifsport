from __future__ import unicode_literals

import re

from django import forms
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from allauth.account.adapter import DefaultAccountAdapter

from administration.models import Agent
from administration.models import Instructor
from administration.models import SubAgent
from events.models import Promoter


class MyAccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request):
        _result = super(MyAccountAdapter, self).get_login_redirect_url(request)
        try:
            request.user.instructor
        except Instructor.DoesNotExist:
            try:
                request.user.promoter
            except Promoter.DoesNotExist:
                try:
                    request.user.agent
                except Agent.DoesNotExist:
                    try:
                        request.user.subagent
                    except SubAgent.DoesNotExist:
                        pass
                    else:
                        return reverse('sub_agreements:dashboard')
                else:
                    return reverse('agreements:dashboard')
            else:
                return reverse('events:dashboard')
        else:
            return reverse('authorizations:dashboard')

    def clean_username(self, username):
        if not re.match(r'^[a-zA-Z0-9]+$', username):
            raise forms.ValidationError(_("Usernames can only contain "
                                          "non-accentuated letters, digits."))
        return super(MyAccountAdapter, self).clean_username(username)
