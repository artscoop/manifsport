import hashlib

from django.contrib.auth.hashers import BasePasswordHasher, mask_hash
from django.utils.datastructures import SortedDict
from django.utils.crypto import constant_time_compare
from django.utils.translation import ugettext_noop as _


class DrupalPasswordHasher(BasePasswordHasher):
    """
    Hasher mocking Drupal Password Hasher

    See :
    https://api.drupal.org/api/drupal/includes!password.inc/function/_password_crypt/7  # noqa
    https://djangosnippets.org/snippets/2924/
    """

    algorithm = "drupal_sha512"
    iterations = 32768
    digest = hashlib.sha512
    itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'  # noqa

    def _password_base64_encode(self, to_encode, count):
        output = ''
        i = 0
        while True:
            value = ord(to_encode[i])
            i += 1
            output = output + self.itoa64[value & 0x3f]
            if i < count:
                value |= ord(to_encode[i]) << 8
                output = output + self.itoa64[(value >> 6) & 0x3f]

            if i >= count:
                break

            i += 1
            if i < count:
                value |= ord(to_encode[i]) << 16

            output = output + self.itoa64[(value >> 12) & 0x3f]
            if i >= count:
                break

            i += 1
            output = output + self.itoa64[(value >> 18) & 0x3f]
            if i >= count:
                break
        return output

    def encode(self, password, salt, iterations=None):  # pylint: disable=W0221
        assert password is not None
        assert salt and '$' not in salt
        if not iterations:
            iterations = self.iterations
        # pylint: disable=W0622
        if isinstance(password, unicode):
            password = password.encode('ascii')
        hash = hashlib.sha512(salt + password).digest()
        # pylint: enable=W0622
        for _iteration in xrange(iterations):
            hash = hashlib.sha512(hash + password).digest()
        hash_length = len(hash)
        hash = self._password_base64_encode(hash, hash_length)[:43]
        return "%s$%d$%s$%s" % (self.algorithm, iterations, salt, hash)

    def verify(self, password, encoded):
        algorithm, iterations, salt, _hash = encoded.split('$', 3)
        assert algorithm == self.algorithm
        encoded_2 = self.encode(password, salt, int(iterations))
        return constant_time_compare(encoded, encoded_2)

    def safe_summary(self, encoded):
        # pylint: disable=W0622
        algorithm, iterations, salt, hash = encoded.split('$', 3)
        # pylint: enable=W0622
        assert algorithm == self.algorithm
        return SortedDict([
            (_('algorithm'), algorithm),
            (_('iterations'), iterations),
            (_('salt'), mask_hash(salt)),
            (_('hash'), mask_hash(hash)),
        ])

    def must_update(self, encoded):
        _algorithm, iterations, _salt, _hash = encoded.split('$', 3)
        return int(iterations) != self.iterations
