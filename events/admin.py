from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from ckeditor.widgets import CKEditorWidget

from authorizations.admin import EventAuthorizationInline

from contacts.admin import ContactInline

from declarations.admin import EventDeclarationInline

from evaluations.admin import RNREvaluationInline
from evaluations.admin import Natura2000EvaluationInline

from .models import AdditionalData
from .models import Promoter, Structure
from .models import OpenRunnerAccount
from .models import StructureType
from .models import AuthorizedNonMotorizedEvent
from .models import DeclaredNonMotorizedEvent
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace
from .models import News


class StructureAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    list_filter = ['type_of_structure__type_of_structure']
    search_fields = ['name', ]


class OpenRunnerAccountInline(admin.StackedInline):
    model = OpenRunnerAccount


class PromoterAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [
        OpenRunnerAccountInline,
    ]
    list_filter = ['cgu', 'openrunneraccount__created',
                   'openrunneraccount__error_message']
    search_fields = ['user__username',
                     'user__first_name',
                     'user__last_name',
                     'user__email']
    actions = ['create_openrunner_account']

    def create_openrunner_account(self, request, queryset):
        for instance in queryset:
            try:
                instance.openrunneraccount.delete()
            except:
                pass
            instance.create_openrunner_account()
    create_openrunner_account.short_description = _('create openrunner account for selected promoters').capitalize()


class AdditionalDataInline(admin.TabularInline):
    model = AdditionalData
    extra = 0


class ProcessAskedListFilter(admin.SimpleListFilter):

    def lookups(self, request, model_admin):
        return(
            ('yes', _('yes')),
            ('no', _('no')),
        )


class DeclarationAskedListFilter(ProcessAskedListFilter):
    title = _('declaration asked')
    parameter_name = 'declaration_asked'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(eventdeclaration__id__gt=0)
        elif self.value() == 'no':
            return queryset.exclude(eventdeclaration__id__gt=0)


class AuthorizationAskedListFilter(ProcessAskedListFilter):
    title = _('authorization asked')
    parameter_name = 'authorization_asked'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(eventauthorization__id__gt=0)
        if self.value() == 'no':
            return queryset.exclude(eventauthorization__id__gt=0)


class ConcentrationAuthorizationAskedListFilter(ProcessAskedListFilter):
    title = _('authorization or declaration asked')
    parameter_name = 'authorization_or_declaration_asked'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(
                Q(eventauthorization__id__gt=0) |
                Q(eventdeclaration__id__gt=0)
            )
        if self.value() == 'no':
            return queryset.exclude(
                Q(eventauthorization__id__gt=0) |
                Q(eventdeclaration__id__gt=0)
            )


class DeclaredNonMotorizedEventAdmin(admin.ModelAdmin):  # noqa pylint: disable=R0904
    inlines = [ContactInline,
               AdditionalDataInline,
               EventDeclarationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['eventdeclaration__state',
                   DeclarationAskedListFilter]
    search_fields = ['name']


class AuthorizedNonMotorizedEventAdmin(admin.ModelAdmin):  # noqa pylint: disable=R0904
    inlines = [ContactInline,
               AdditionalDataInline,
               EventAuthorizationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['eventauthorization__state',
                   AuthorizationAskedListFilter]
    search_fields = ['name']


class MotorizedConcentrationAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [ContactInline,
               AdditionalDataInline,
               EventAuthorizationInline,
               EventDeclarationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['eventauthorization__state',
                   'eventdeclaration__state',
                   ConcentrationAuthorizationAskedListFilter]
    search_fields = ['name']


class MotorizedEventAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [ContactInline,
               AdditionalDataInline,
               EventAuthorizationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['eventauthorization__state',
                   AuthorizationAskedListFilter]
    search_fields = ['name']


class MotorizedRaceAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [ContactInline,
               AdditionalDataInline,
               EventAuthorizationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['eventauthorization__state',
                   AuthorizationAskedListFilter]
    search_fields = ['name']


class MyFlatpageForm(FlatpageForm):
    content = forms.CharField(widget=CKEditorWidget())


class MyFlatPageAdmin(FlatPageAdmin):  # pylint: disable=R0904
    form = MyFlatpageForm


UserAdmin.list_display = ('username',
                          'email',
                          'first_name',
                          'last_name',
                          'is_active',
                          'is_staff',
                          'date_joined')


admin.site.register(News)
admin.site.register(Promoter, PromoterAdmin)
admin.site.register(StructureType)
admin.site.register(Structure, StructureAdmin)
admin.site.register(DeclaredNonMotorizedEvent, DeclaredNonMotorizedEventAdmin)
admin.site.register(AuthorizedNonMotorizedEvent,
                    AuthorizedNonMotorizedEventAdmin)
admin.site.register(MotorizedConcentration, MotorizedConcentrationAdmin)
admin.site.register(MotorizedEvent, MotorizedEventAdmin)
admin.site.register(MotorizedRace, MotorizedRaceAdmin)

admin.site.unregister(Site)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, MyFlatPageAdmin)
