from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin

from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy

from django.db.models import Count
from django.db.models import Q

from django.http import HttpResponseRedirect

from django.shortcuts import get_object_or_404

from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _

from django.views.generic import ListView, DetailView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from extra_views import CreateWithInlinesView, UpdateWithInlinesView

from administration.models import TownHallAgent

from authorizations.decorators import instructor_required

from contacts.forms import ContactInline

from sports.models import Activity

from .decorators import promoter_required
from .decorators import owner_required

from .forms import AdditionalDataRequestForm
from .forms import AdditionalDataProvideForm
from .forms import UserUpdateForm
from .forms import AuthorizedNonMotorizedEventForm
from .forms import DeclaredNonMotorizedEventForm
from .forms import MotorizedConcentrationForm
from .forms import MotorizedEventForm
from .forms import MotorizedRaceForm
from .forms import StructureForm

from .models import AdditionalData
from .models import Event
from .models import AuthorizedNonMotorizedEvent
from .models import DeclaredNonMotorizedEvent
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace
from .models import Structure
from .models import Promoter
from .models import News

class HomePage(TemplateView):
    template_name = 'events/home.html'


class Dashboard(ListView):  # pylint: disable=R0901
    model = Event


    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        try:
            structure = self.request.user.promoter.structure
        except Structure.DoesNotExist:
            structure = None
        return Event.objects.filter(structure=structure).order_by('-pk')

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['last_news'] = News.objects.last()
        return context


class ProfileDetailView(DetailView):
    model = User

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class UserUpdateView(UpdateView):
    model = User
    form_class = UserUpdateForm
    success_url = '/accounts/profile'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class Calendar(ListView):  # pylint: disable=R0901
    model = Event
    template_name = 'events/calendar.html'

    def get_queryset(self):
        now = timezone.now()
        return Event.objects.filter(
            ~Q(authorizednonmotorizedevent__eventauthorization__id=None) |
            ~Q(declarednonmotorizedevent__eventdeclaration__id=None) |
            ~Q(motorizedevent__eventauthorization__id=None) |
            ~Q(motorizedrace__eventauthorization__id=None) |
            ~Q(motorizedconcentration__eventauthorization__id=None) |
            ~Q(motorizedconcentration__eventdeclaration__id=None),
            begin_date__gt=now,
        ).order_by('begin_date')

    def get_context_data(self, **kwargs):
        context = super(Calendar, self).get_context_data(**kwargs)
        context['activities'] = Activity.objects.annotate(num_events=Count('event')).filter(num_events__gt=0).order_by('name')
        return context


class FilteredCalendar(Calendar):  # pylint: disable=R0901

    def get_queryset(self):
        return super(FilteredCalendar, self).get_queryset().filter(activity=self.kwargs['activity'])

    def get_context_data(self, **kwargs):
        context = super(FilteredCalendar, self).get_context_data(**kwargs)
        context['current_activity'] = int(self.kwargs['activity'])
        return context


class StructureCreate(CreateView):  # pylint: disable=R0901
    model = Structure
    form_class = StructureForm

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(StructureCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.promoter = self.request.user.promoter
        return super(StructureCreate, self).form_valid(form)


class StructureUpdateView(UpdateView):
    model = Structure
    form_class = StructureForm
    success_url = '/accounts/profile'

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(StructureUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user.promoter.structure


###########################################################
# Generic Choice View
###########################################################
class ChoiceView(TemplateView):

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(ChoiceView, self).dispatch(*args, **kwargs)


###########################################################
# Types of Event choices Views
###########################################################
class CreateRoute(ChoiceView):
    template_name = 'events/create_route.html'


class EventChoose(ChoiceView):
    template_name = 'events/event_choose.html'


class NonMotorizedEventChoose(ChoiceView):
    template_name = 'events/nonmotorizedevent_choose.html'


class DeclaredNonMotorizedEventChoose(ChoiceView):
    template_name = 'events/declarednonmotorizedevent_choose.html'


class MotorizedEventChoose(ChoiceView):
    template_name = 'events/motorizedevent_choose.html'


class PublicRoadMotorizedEventChoose(ChoiceView):
    template_name = 'events/publicroadmotorizedevent_choose.html'


###########################################################
# Event Views (Generic)
###########################################################
class EventDetail(DetailView):  # pylint: disable=R0901

    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super(EventDetail, self).dispatch(*args, **kwargs)


class EventCreate(CreateWithInlinesView):  # pylint: disable=R0901
    inlines = [ContactInline]
    template_name = 'events/event_form.html'

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(EventCreate, self).dispatch(*args, **kwargs)

    def forms_valid(self, form, inlines):
        self.object.structure = self.request.user.promoter.structure
        self.object = form.save()
        for formset in inlines:
            formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super(EventCreate, self).get_form_kwargs()
        kwargs['routes'] = self.request.user.promoter.get_openrunner_routes()
        return kwargs


class EventUpdate(UpdateWithInlinesView):  # pylint: disable=R0901
    inlines = [ContactInline]
    template_name = 'events/event_form.html'

    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super(EventUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(EventUpdate, self).get_form_kwargs()
        kwargs['routes'] = self.request.user.promoter.get_openrunner_routes()
        # Here, we have to transform event routes from a string into a list
        # We use strip(',') to avoid errors caused by leading or trailing commas
        if kwargs['instance'].__dict__['openrunner_route']:
            kwargs['instance'].__dict__['openrunner_route'] = [int(route) for route in kwargs['instance'].__dict__['openrunner_route'].strip(',').split(',')]  # noqa
        return kwargs


###########################################################
# Event Public View
###########################################################
class EventPublicDetail(DetailView):  # pylint: disable=R0901
    model = Event
    template_name = 'events/eventpublic_detail.html'


###########################################################
# DeclaredNonMotorizedEvent Views
###########################################################
class DeclaredNonMotorizedEventDetail(EventDetail):  # pylint: disable=R0901
    model = DeclaredNonMotorizedEvent


class DeclaredNonMotorizedEventCreate(EventCreate):  # pylint: disable=R0901
    model = DeclaredNonMotorizedEvent
    form_class = DeclaredNonMotorizedEventForm


class DeclaredNonMotorizedEventUpdate(EventUpdate):  # pylint: disable=R0901
    model = DeclaredNonMotorizedEvent
    form_class = DeclaredNonMotorizedEventForm


###########################################################
# AuthorizedNonMotorizedEvent Views
###########################################################
class AuthorizedNonMotorizedEventDetail(EventDetail):  # pylint: disable=R0901
    model = AuthorizedNonMotorizedEvent


class AuthorizedNonMotorizedEventCreate(EventCreate):  # pylint: disable=R0901
    model = AuthorizedNonMotorizedEvent
    form_class = AuthorizedNonMotorizedEventForm


class AuthorizedNonMotorizedEventUpdate(EventUpdate):  # pylint: disable=R0901
    model = AuthorizedNonMotorizedEvent
    form_class = AuthorizedNonMotorizedEventForm


###########################################################
# MotorizedConcentration Views
###########################################################
class MotorizedConcentrationDetail(EventDetail):  # pylint: disable=R0901
    model = MotorizedConcentration


class MotorizedConcentrationCreate(EventCreate):  # pylint: disable=R0901
    model = MotorizedConcentration
    form_class = MotorizedConcentrationForm


class MotorizedConcentrationUpdate(EventUpdate):  # pylint: disable=R0901
    model = MotorizedConcentration
    form_class = MotorizedConcentrationForm


###########################################################
# MotorizedEvent Views
###########################################################
class MotorizedEventDetail(EventDetail):  # pylint: disable=R0901
    model = MotorizedEvent


class MotorizedEventCreate(EventCreate):  # pylint: disable=R0901
    model = MotorizedEvent
    form_class = MotorizedEventForm


class MotorizedEventUpdate(EventUpdate):  # pylint: disable=R0901
    model = MotorizedEvent
    form_class = MotorizedEventForm


###########################################################
# MotorizedRace Views
###########################################################
class MotorizedRaceDetail(EventDetail):  # pylint: disable=R0901
    model = MotorizedRace


class MotorizedRaceCreate(EventCreate):  # pylint: disable=R0901
    model = MotorizedRace
    form_class = MotorizedRaceForm


class MotorizedRaceUpdate(EventUpdate):  # pylint: disable=R0901
    model = MotorizedRace
    form_class = MotorizedRaceForm


###########################################################
# Additionnal Data Request Views
###########################################################
class AdditionalDataRequestView(SuccessMessageMixin, CreateView):
    model = AdditionalData
    form_class = AdditionalDataRequestForm
    success_message = _('Request successfully sent')

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(AdditionalDataRequestView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.event = get_object_or_404(Event,
                                                pk=self.kwargs['event_pk'])
        return super(AdditionalDataRequestView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AdditionalDataRequestView, self).get_context_data(**kwargs)
        context['event'] = get_object_or_404(Event, pk=self.kwargs['event_pk'])
        return context

    def get_success_url(self):
        try:
            declaration = self.object.event.eventdeclaration
        except:
            authorization = self.object.event.eventauthorization
            return reverse('authorizations:authorization_detail',
                           kwargs={'pk': authorization.pk})
        else:
            return reverse('declarations:declaration_detail',
                           kwargs={'pk': declaration.pk})


class AdditionalDataProvideView(SuccessMessageMixin, UpdateView):
    model = AdditionalData
    form_class = AdditionalDataProvideForm
    success_message = _('Additional data successfully sent')
    success_url = reverse_lazy('events:dashboard')

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(AdditionalDataProvideView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        self.object.provide_data()
        return super(AdditionalDataProvideView, self).form_valid(form)


###########################################################
# MISC
###########################################################
class MailToPromotersView(LoginRequiredMixin, SuperuserRequiredMixin, TemplateView):
    template_name = 'events/mail_to_promoters.html'

    def get_context_data(self, **kwargs):
        context = super(MailToPromotersView, self).get_context_data(**kwargs)
        context['mails'] = ",".join(
            [promoter.user.email for promoter in Promoter.objects.all()]
        )
        return context


class MailToTownHallsView(LoginRequiredMixin, SuperuserRequiredMixin, TemplateView):
    template_name = 'events/mail_to_townhalls.html'

    def get_context_data(self, **kwargs):
        context = super(MailToTownHallsView, self).get_context_data(**kwargs)
        context['mails'] = ",".join(
            [agent.user.email for agent in TownHallAgent.objects.all()]
        )
        return context


class NewsListView(ListView):
    model = News
