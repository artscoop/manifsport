from __future__ import unicode_literals

import calendar
import datetime
import time
import urllib

from django.conf import settings
from django.contrib.auth.hashers import (is_password_usable,
                                         check_password,
                                         make_password,
                                         identify_hasher,
                                         load_hashers)
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.timezone import utc

from administrative_division.factories import DepartmentFactory

from authorizations.factories import EventAuthorizationFactory

from contacts.factories import AddressFactory

from declarations.factories import EventDeclarationFactory

from evaluations.factories import RNREvaluationFactory
from evaluations.factories import Natura2000EvaluationFactory

from notifications.models import Action

from .factories import PromoterFactory, StructureFactory, StructureTypeFactory
from .factories import OpenRunnerAccountFactory
from .factories import AuthorizedNonMotorizedEventFactory
from .factories import DeclaredNonMotorizedEventFactory
from .factories import MotorizedConcentrationFactory
from .factories import MotorizedEventFactory
from .factories import MotorizedRaceFactory


class TestDrupalHashPass(TestCase):  # pylint: disable=R0904

    def setUp(self):
        load_hashers(password_hashers=settings.PASSWORD_HASHERS)

    def test_drupal(self):
        encoded = make_password('formationddcs', 'wX89/crt', 'drupal_sha512')
        self.assertEqual(
            encoded,
            'drupal_sha512$32768$wX89/crt$w46C97GO3Qd/Zkx/qE5BBrtVuj687bmeTIC9b56tpGC'  # noqa
        )
        self.assertTrue(is_password_usable(encoded))
        self.assertTrue(check_password('formationddcs', encoded))
        self.assertFalse(check_password('formationddcse', encoded))
        # pylint: disable=E1103
        self.assertEqual(identify_hasher(encoded).algorithm, "drupal_sha512")
        # pylint: enable=E1103
        # Blank passwords
        blank_encoded = make_password('', 'seasalt', 'drupal_sha512')
        self.assertTrue(blank_encoded.startswith('drupal_sha512$'))
        self.assertTrue(is_password_usable(blank_encoded))
        self.assertTrue(check_password('', blank_encoded))
        self.assertFalse(check_password(' ', blank_encoded))


class PromoterMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the associated user
        '''
        promoter = PromoterFactory.build()
        self.assertEqual(promoter.__str__(), promoter.user.username)

    def test_get_department(self):
        promoter = PromoterFactory.create()
        _structure = StructureFactory.create(promoter=promoter)
        self.assertEqual(
            promoter.get_department(),
            promoter.structure.commune.arrondissement.department.name
        )

    def test_hash_openrunner_secret_key(self):
        settings.OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'
        promoter = PromoterFactory.build()
        self.assertEqual(promoter.hash_openrunner_secret_key(),
                         '2dc072c3dc2140006795fa8a7001a9bd')

    def test_build_parameters(self):
        settings.OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'
        promoter = PromoterFactory.create()
        structure = StructureFactory.create(promoter=promoter)
        _address = AddressFactory.create(
            content_type=ContentType.objects.get_for_model(structure),
            object_id=structure.id
        )
        self.assertEqual(
            promoter.build_parameters(),
            urllib.urlencode({'cddcs': promoter.encrypt_for_openrunner(
                '2dc072c3dc2140006795fa8a7001a9bd',
                ','.join([promoter.user.email,
                          promoter.user.username,
                          promoter.get_department()])
            )})
        )

    def test_get_openrunner_account_creation_url(self):
        settings.OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'
        promoter = PromoterFactory.create()
        structure = StructureFactory.create(promoter=promoter)
        _address = AddressFactory.create(
            content_type=ContentType.objects.get_for_model(structure),
            object_id=structure.id
        )
        self.assertEqual(
            promoter.get_openrunner_account_creation_url(),
            ''.join([settings.OPENRUNNER_HOST,
                     settings.OPENRUNNER_CREATE_ACCOUNT,
                     promoter.build_parameters(),
                     '&t=',
                     str(calendar.timegm(time.gmtime()))])
        )

    def test_create_openrunner_account_failure(self):
        '''
        create_openrunner_account() should create an openrunneraccount
        if OPENRUNNER_HOST is down, should add the related error_message
        and mark the account with created=False
        '''
        settings.OPENRUNNER_HOST = 'http://ddcs.openrunnerrrrr.com'
        promoter = PromoterFactory.create()
        promoter.create_openrunner_account()
        self.assertEqual(promoter.openrunneraccount.error_message,
                         'ddcs.openrunner.com down')
        self.assertFalse(promoter.openrunneraccount.created)

    def test_process_openrunner_positive_response(self):
        '''
        process_openrunner_account() when response code is positive
        should create the account and associates a password
        '''
        promoter = PromoterFactory.create()
        openrunneraccount = OpenRunnerAccountFactory.create(promoter=promoter)
        promoter.process_openrunner_response(
            'ISO-8859-1',
            '1:nJTGyJ2YyA=='.encode('ISO-8859-1'),
            promoter.openrunneraccount,
            True,
        )
        self.assertEqual(openrunneraccount.password, 'j0c\x98ffe')
        self.assertEqual(openrunneraccount.error_message, '')
        self.assertTrue(openrunneraccount.created)

    def test_process_openrunner_negative_response(self):
        '''
        process_openrunner_account() when response code is positive
        should create the account and associates a password
        '''
        promoter = PromoterFactory.create()
        openrunneraccount = OpenRunnerAccountFactory.create(promoter=promoter)
        promoter.process_openrunner_response(
            'ISO-8859-1',
            '0:Login duplicated',
            promoter.openrunneraccount,
            True,
        )
        self.assertEqual(openrunneraccount.password, '')
        self.assertEqual(openrunneraccount.error_message,
                         'Login duplicated')
        self.assertFalse(openrunneraccount.created)

    def test_get_openrunner_routes_without_account(self):
        promoter = PromoterFactory.build()
        self.assertEqual(promoter.get_openrunner_routes(), [])


class OpenRunnerAccountMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the associated promoter
        '''
        openrunner_account = OpenRunnerAccountFactory.build()
        self.assertEqual(openrunner_account.__str__(),
                         openrunner_account.promoter.user.username)

    def test_update_error_message_if_not_created(self):
        '''
        update_error_message() should do nothing if created is False
        (meaning openrunner is allready existing)
        '''
        openrunner_account = OpenRunnerAccountFactory.create()
        created = False
        openrunner_account.update_error_message(created, 'plop')
        self.assertEqual(openrunner_account.error_message, '')

    def test_update_error_message_if_created(self):
        '''
        update_error_message() should update error message and creation date
        '''
        openrunner_account = OpenRunnerAccountFactory.create()
        created = True
        openrunner_account.update_error_message(created, 'plop')
        self.assertEqual(openrunner_account.error_message, 'plop')
        self.assertFalse(openrunner_account.created)

    def test_set_password(self):
        openrunner_account = OpenRunnerAccountFactory.create()
        openrunner_account.set_password('password')
        self.assertEqual(openrunner_account.error_message, '')
        self.assertEqual(openrunner_account.password, 'password')
        self.assertTrue(openrunner_account.created)

    def test_get_routes_request_url(self):
        account = OpenRunnerAccountFactory.build()
        self.assertEqual(account.get_routes_request_url(),
                         ''.join([settings.OPENRUNNER_HOST,
                                  settings.OPENRUNNER_GET_ROUTES,
                                  account.promoter.user.username]))

    def test_get_routes(self):
        settings.OPENRUNNER_HOST = 'http://ddcs.openrunnerrrrr.com'
        account = OpenRunnerAccountFactory.build()
        self.assertEqual(account.get_routes(), {})


class StructureTypeMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the type of structure
        '''
        structure_type = StructureTypeFactory.build()
        self.assertEqual(structure_type.__str__(), 'Association')


class StructureMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the associated structure
        '''
        structure = StructureFactory.build(name='GRS')
        self.assertEqual(structure.__str__(), structure.name.title())


# pylint: disable=R0904
class AuthorizedNonMotorizedEventMethodTests(TestCase):

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        event = AuthorizedNonMotorizedEventFactory.build(pk=3)
        self.assertEqual(event.get_absolute_url(),
                         '/authorizednonmotorizedevents/3/')

    def test_display_natura2000_eval_panel(self):
        event = AuthorizedNonMotorizedEventFactory.build(lucrative=True)
        self.assertTrue(event.display_natura2000_eval_panel())
        event3 = AuthorizedNonMotorizedEventFactory.build(big_budget=True)
        self.assertTrue(event3.display_natura2000_eval_panel())
        event4 = AuthorizedNonMotorizedEventFactory.build(big_title=True)
        self.assertTrue(event4.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        event = AuthorizedNonMotorizedEventFactory.build()
        self.assertFalse(event.display_natura2000_eval_panel())

    def test_log_event_creation(self):
        event = AuthorizedNonMotorizedEventFactory.create()
        action = Action.objects.last()
        self.assertEqual(action.user, event.structure.promoter.user)
        self.assertEqual(action.event, event.event_ptr)
        self.assertEqual(action.action, "event created")
        # when event is updated,
        # no log action is created
        event.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)

    def test_legal_delay(self):
        event = AuthorizedNonMotorizedEventFactory.create()
        self.assertEqual(event.legal_delay(), 60)

    def test_legal_delay_2(self):
        event = AuthorizedNonMotorizedEventFactory.create()
        event.other_departments_crossed.add(DepartmentFactory.create())
        self.assertEqual(event.legal_delay(), 90)

    def test_get_limit_date(self):
        event = AuthorizedNonMotorizedEventFactory.create(
            begin_date=datetime.datetime.utcnow().replace(tzinfo=utc)
        )
        self.assertEqual(
            event.get_limit_date(),
            (event.begin_date - datetime.timedelta(days=60)).replace(
                hour=23,
                minute=59
            )
        )

    def test_delay_exceeded(self):
        event = AuthorizedNonMotorizedEventFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=59))
        )
        self.assertTrue(event.delay_exceeded())

    def test_delay_not_exceeded(self):
        event = AuthorizedNonMotorizedEventFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=61))
        )
        self.assertFalse(event.delay_exceeded())

    def test_not_two_weeks_left(self):
        event = AuthorizedNonMotorizedEventFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=60) +
                        datetime.timedelta(weeks=2))
        )
        self.assertFalse(event.two_weeks_left())

    def test_two_weeks_left(self):
        event = AuthorizedNonMotorizedEventFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=60) +
                        datetime.timedelta(weeks=1))
        )
        self.assertTrue(event.two_weeks_left())

    def test_get_final_breadcrumb_not_sent(self):
        event = AuthorizedNonMotorizedEventFactory.build()
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        event = AuthorizedNonMotorizedEventFactory.create()
        _authorization = EventAuthorizationFactory.create(event=event)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 1)
# pylint: enable=R0904


class DeclaredNonMotorizedEventMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the associated event
        '''
        event = DeclaredNonMotorizedEventFactory.build(
            name='Course des 4 colines'
        )
        self.assertEqual(event.__str__(), event.name.title())

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        event = DeclaredNonMotorizedEventFactory.build(pk=2)
        self.assertEqual(event.get_absolute_url(),
                         '/declarednonmotorizedevents/2/')

    def test_display_rnr_eval_panel(self):
        event = DeclaredNonMotorizedEventFactory.build(is_on_rnr=True)
        self.assertTrue(event.display_rnr_eval_panel())

    def test_not_display_rnr_eval_panel(self):
        event = DeclaredNonMotorizedEventFactory.build(is_on_rnr=False)
        self.assertFalse(event.display_rnr_eval_panel())

    def test_display_natura2000_eval_panel(self):
        event = DeclaredNonMotorizedEventFactory.build(lucrative=True)
        self.assertTrue(event.display_natura2000_eval_panel())
        event3 = DeclaredNonMotorizedEventFactory.build(big_budget=True)
        self.assertTrue(event3.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        event = DeclaredNonMotorizedEventFactory.build()
        self.assertFalse(event.display_natura2000_eval_panel())

    def test_legal_delay(self):
        event = DeclaredNonMotorizedEventFactory.build()
        self.assertEqual(event.legal_delay(), 30)

    def test_get_final_breadcrumb_not_sent(self):
        event = DeclaredNonMotorizedEventFactory.build()
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        event = DeclaredNonMotorizedEventFactory.create()
        _declaration = EventDeclarationFactory.create(event=event)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 1)

    def test_get_breadcrumbs(self):
        event = DeclaredNonMotorizedEventFactory.build()
        self.assertEqual(len(event.get_breadcrumbs()), 2)

    def test_get_breadcrumbs_full(self):
        event = DeclaredNonMotorizedEventFactory.create()
        _rnr = RNREvaluationFactory.create(event=event)
        _natura2000 = Natura2000EvaluationFactory.create(event=event)
        self.assertEqual(len(event.get_breadcrumbs()), 4)

    def test_get_breadcrumbs_full_2(self):
        event = DeclaredNonMotorizedEventFactory.create(is_on_rnr=True,
                                                        lucrative=True)
        self.assertEqual(len(event.get_breadcrumbs()), 4)


class MotorizedConcentrationMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        event = MotorizedConcentrationFactory.build(pk=4)
        self.assertEqual(event.get_absolute_url(),
                         '/motorizedconcentrations/4/')

    def test_display_natura2000_eval_panel(self):
        event = MotorizedConcentrationFactory.build(lucrative=True)
        self.assertTrue(event.display_natura2000_eval_panel())
        event3 = MotorizedConcentrationFactory.build(big_budget=True)
        self.assertTrue(event3.display_natura2000_eval_panel())
        event4 = MotorizedConcentrationFactory.build(big_title=True)
        self.assertTrue(event4.display_natura2000_eval_panel())
        event5 = MotorizedConcentrationFactory.build(motor_on_natura2000=True)
        self.assertTrue(event5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        event = MotorizedConcentrationFactory.build()
        self.assertFalse(event.display_natura2000_eval_panel())

    def test_legal_delay_for_authorized(self):
        event = MotorizedConcentrationFactory.build(big_concentration=True)
        self.assertEqual(event.legal_delay(), 90)

    def test_legal_delay_for_declared(self):
        event = MotorizedConcentrationFactory.build(big_concentration=False)
        self.assertEqual(event.legal_delay(), 60)

    def test_get_final_breadcrumb_not_sent(self):
        event = MotorizedConcentrationFactory.build(big_concentration=True)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_not_sent_2(self):
        event = MotorizedConcentrationFactory.build(big_concentration=False)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        event = MotorizedConcentrationFactory.create(big_concentration=True)
        _authorization = EventAuthorizationFactory.create(event=event)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 1)

    def test_get_final_breadcrumb_sent_2(self):
        event = MotorizedConcentrationFactory.create(big_concentration=False)
        _declaration = EventDeclarationFactory.create(event=event)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 1)


class MotorizedEventMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        event = MotorizedEventFactory.build(pk=4)
        self.assertEqual(event.get_absolute_url(),
                         '/motorizedevents/4/')

    def test_display_natura2000_eval_panel(self):
        event = MotorizedEventFactory.build(lucrative=True)
        self.assertTrue(event.display_natura2000_eval_panel())
        event3 = MotorizedEventFactory.build(big_budget=True)
        self.assertTrue(event3.display_natura2000_eval_panel())
        event4 = MotorizedEventFactory.build(big_title=True)
        self.assertTrue(event4.display_natura2000_eval_panel())
        event5 = MotorizedEventFactory.build(motor_on_natura2000=True)
        self.assertTrue(event5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        event = MotorizedEventFactory.build()
        self.assertFalse(event.display_natura2000_eval_panel())

    def test_legal_delay(self):
        event = MotorizedEventFactory.build()
        self.assertEqual(event.legal_delay(), 90)

    def test_get_final_breadcrumb_not_sent(self):
        event = MotorizedEventFactory.build()
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        event = MotorizedEventFactory.create()
        _authorization = EventAuthorizationFactory.create(event=event)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 1)


class MotorizedRaceMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        event = MotorizedRaceFactory.build(pk=5)
        self.assertEqual(event.get_absolute_url(),
                         '/motorizedraces/5/')

    def test_display_natura2000_eval_panel(self):
        event = MotorizedRaceFactory.build(lucrative=True)
        self.assertTrue(event.display_natura2000_eval_panel())
        event2 = MotorizedRaceFactory.build(approval_request=True)
        self.assertTrue(event2.display_natura2000_eval_panel())
        event3 = MotorizedRaceFactory.build(big_budget=True)
        self.assertTrue(event3.display_natura2000_eval_panel())
        event4 = MotorizedRaceFactory.build(big_title=True)
        self.assertTrue(event4.display_natura2000_eval_panel())
        event5 = MotorizedRaceFactory.build(motor_on_closed_road=True)
        self.assertTrue(event5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        event = MotorizedRaceFactory.build()
        self.assertFalse(event.display_natura2000_eval_panel())

    def test_legal_delay(self):
        event = MotorizedRaceFactory.build()
        self.assertEqual(event.legal_delay(), 90)

    def test_get_final_breadcrumb_not_sent(self):
        event = MotorizedRaceFactory.build()
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        event = MotorizedRaceFactory.create()
        _authorization = EventAuthorizationFactory.create(event=event)
        self.assertEqual(event.get_final_breadcrumb()[1], True)
        self.assertEqual(event.get_final_breadcrumb()[0][1], 1)


class EventViewTests(TestCase):  # pylint: disable=R0904

    def test_index(self):
        response = self.client.get(reverse('events:home_page'))
        self.assertEqual(response.status_code, 200)
