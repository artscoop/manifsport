from __future__ import unicode_literals

import datetime
import factory

from factory.fuzzy import FuzzyDateTime

from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.utils.timezone import utc

from administrative_division.factories import CommuneFactory
from sports.factories import ActivityFactory, MultiSportFederationFactory

from .models import Promoter, Structure
from .models import OpenRunnerAccount
from .models import AuthorizedNonMotorizedEvent
from .models import DeclaredNonMotorizedEvent
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace
from .models import StructureType

from .models import create_openrunner_account


# pylint: disable=W0108
class UserFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = User

    username = factory.Sequence(lambda n: 'jdoe{0}'.format(n))
    email = factory.Sequence(lambda n: 'john.doe{0}@example.com'.format(n))


class PromoterFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Promoter

    user = factory.SubFactory(UserFactory)


class OpenRunnerAccountFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = OpenRunnerAccount

    promoter = factory.SubFactory(PromoterFactory)


class StructureTypeFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = StructureType

    type_of_structure = 'Association'


class StructureFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Structure

    promoter = factory.SubFactory(PromoterFactory)
    name = factory.Sequence(lambda n: 'GRS{0}'.format(n))
    type_of_structure = factory.SubFactory(StructureTypeFactory)
    phone = '06 12 23 45 56'
    website = 'http://www.grs.fr'
    address = factory.Sequence(lambda n: 'address{0}'.format(n))
    commune = factory.SubFactory(CommuneFactory)

    @classmethod
    def _generate(cls, create, attrs):  # pylint: disable=E1002
        """
        Override the default _generate() to disable the post-save signal.
        """
        post_save.disconnect(create_openrunner_account, Structure)
        structure = super(StructureFactory, cls)._generate(create, attrs)
        post_save.connect(create_openrunner_account, Structure)
        return structure


class DeclaredNonMotorizedEventFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = DeclaredNonMotorizedEvent

    name = factory.Sequence(lambda n: 'Non Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activity = factory.SubFactory(ActivityFactory)
    support_vehicles_number = 30
    max_audience = 5000
    grouped_traffic = 'p'


class AuthorizedNonMotorizedEventFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = AuthorizedNonMotorizedEvent

    name = factory.Sequence(lambda n: 'Non Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activity = factory.SubFactory(ActivityFactory)
    multisport_federation = factory.SubFactory(MultiSportFederationFactory)
    max_audience = 5000


class MotorizedConcentrationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = MotorizedConcentration

    name = factory.Sequence(lambda n: 'Motorized Concentration{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activity = factory.SubFactory(ActivityFactory)
    max_audience = 5000


class MotorizedEventFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = MotorizedEvent

    name = factory.Sequence(lambda n: 'Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activity = factory.SubFactory(ActivityFactory)
    max_audience = 5000


class MotorizedRaceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = MotorizedRace

    name = factory.Sequence(lambda n: 'Motorized Race{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activity = factory.SubFactory(ActivityFactory)
    max_audience = 5000
