from __future__ import print_function
from __future__ import unicode_literals

from django.core.management.base import NoArgsCommand

from django.contrib.auth.models import User


class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        duplicated_emails = 0
        for user in User.objects.all():
            current_email = User.objects.filter(email=user.email)
            if current_email.count() > 1:
                duplicated_emails += 1
                print("\n==={} :".format(current_email[0].email))
                print(" - ".join([duplicated.username for duplicated in current_email]))
                print(" - ".join([mail for mail in user.emailaddress_set.all()]))
                print("===")
        print("Duplicated emails : {}".format(duplicated_emails / 2))
