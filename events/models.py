from __future__ import unicode_literals

import base64
import calendar
import datetime
import hashlib
import requests
import time
import urllib

from django.conf import settings

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.models import FlatPage

from django.core.mail import send_mail
from django.core.urlresolvers import reverse

from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save

from django.dispatch import receiver

from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, ugettext, activate

from model_utils.managers import InheritanceManager

from ddcs_loire.utils import add_notification_entry
from ddcs_loire.utils import add_log_entry

from administrative_division.models import Commune, Department
from administrative_division.models import CHOICES

from contacts.models import Contact

from sports.models import Activity, MultiSportFederation


MAIL_LIAISON = _('for event')


@python_2_unicode_compatible
class News(models.Model):
    description = models.TextField(_('description'))
    page = models.ForeignKey(FlatPage, verbose_name=_('page'))

    class Meta:
        verbose_name = _('platform new')
        verbose_name_plural = _('platform news')

    def __str__(self):
        return self.description


@python_2_unicode_compatible
class Promoter(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'))
    cgu = models.BooleanField(_('cgu agreement'), default=False)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('promoter')

    def __str__(self):
        return self.user.get_username()  # pylint: disable=E1101

    def get_department(self):
        # pylint: disable=E1101
        return self.structure.commune.arrondissement.department.name
        # pylint: enable=E1101

    def hash_openrunner_secret_key(self):
        result = hashlib.md5()
        result.update(settings.OPENRUNNER_SECRET_KEY)
        return result.hexdigest()

    def encrypt_for_openrunner(self, hashed_secret_key, string):
        key_index = -1
        result = b''
        for string_index in range(0, len(string)):
            key_index += 1
            if key_index > 31:
                key_index = 0
            one = ord(string[string_index])
            # all remaining chars of the hashed secret key will
            # have the value 0
            if key_index > len(hashed_secret_key) - 1:
                two = 0
            else:
                two = ord(hashed_secret_key[key_index])
            merged_char = one + two
            # if merged char is out of range for ascii representation
            if merged_char > 255:
                merged_char -= 256
            result = b''.join([result, chr(merged_char)])
        return base64.b64encode(result)

    def decrypt_from_openrunner(self, hashed_secret_key, string):
        key_index = -1
        result = b''
        string = base64.b64decode(string)
        for string_index in range(0, len(string)):
            key_index += 1
            if key_index > 31:
                key_index = 0
            one = ord(string[string_index])
            # all remaining chars of the hashed secret key will
            # have the value 0
            if key_index > len(hashed_secret_key) - 1:
                two = 0
            else:
                two = ord(hashed_secret_key[key_index])
            merged_char = one - two
            # if merged char is out of range for ascii representation
            if merged_char < 1:
                merged_char += 256
            result = b''.join([result, chr(merged_char)])
        result = result.decode('iso-8859-1')
        return result

    def build_parameters(self):
        # pylint: disable=E1101
        parameters = ','.join([self.user.email,
                               self.user.username,
                               self.get_department()])
        # pylint: enable=E1101
        parameters = self.encrypt_for_openrunner(
            self.hash_openrunner_secret_key(),
            parameters
        )
        return urllib.urlencode({'cddcs': parameters})

    def get_openrunner_account_creation_url(self):
        url = ''.join([settings.OPENRUNNER_HOST,
                       settings.OPENRUNNER_CREATE_ACCOUNT,
                       self.build_parameters(),
                       '&t=',
                       str(calendar.timegm(time.gmtime()))])
        return url

    def process_openrunner_response(self, response_encoding, response_text,
                                    account, created):
        response_code = int(response_text.split(':')[0])
        response_value = response_text.split(':')[1].decode(
            response_encoding
        )
        if response_code == 1:
            # we save the account
            # 2 cases :
            # - account has just been created (created=False by default)
            # - account was previously created with an error
            openrunner_password = self.decrypt_from_openrunner(
                self.hash_openrunner_secret_key(),
                response_value,
            )
            account.set_password(openrunner_password)
            account.notify_creation()
        else:
            account.update_error_message(created, response_value)

    def create_openrunner_account(self):
        # first we look if an openrunner account for this promoter
        # allready exists
        # if not, openrunner account is created, else, we get it
        account, created = OpenRunnerAccount.objects.get_or_create(
            promoter=self,
        )
        if created:
            try:
                response = requests.get(
                    self.get_openrunner_account_creation_url()
                )
            except:
                account.update_error_message(created,
                                             'ddcs.openrunner.com down')
            else:
                self.process_openrunner_response(response.encoding,
                                                 response.text,
                                                 account, created)

    def get_openrunner_routes(self):
        try:
            # pylint: disable=E1101
            routes = self.openrunneraccount.get_routes()
            # pylint: enable=E1101
        except OpenRunnerAccount.DoesNotExist:
            routes = {}
        if routes == []:
            routes = {}
        return [(int(route[0]), route[1]) for route in routes.items()]


@python_2_unicode_compatible
class OpenRunnerAccount(models.Model):
    promoter = models.OneToOneField(Promoter, verbose_name=_('promoter'))
    created = models.BooleanField(_('created'), default=False)
    creation_date = models.DateTimeField(
        _('creation date'),
        blank=True,
        null=True,
    )
    password = models.CharField(
        _('password'),
        max_length=255,
        blank=True
    )
    error_message = models.CharField(
        _('error message'),
        max_length=255,
        blank=True
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('openrunner account')
        verbose_name_plural = _('openrunner accounts')

    def __str__(self):
        return self.promoter.user.username  # pylint: disable=E1101

    def update_error_message(self, created, error_message):
        # we update the account only if it was not existing before
        if created:
            self.creation_date = timezone.now()
            self.error_message = error_message
            self.save()

    def set_password(self, openrunner_password):
        self.created = True
        self.creation_date = timezone.now()
        self.password = openrunner_password
        self.error_message = ''
        self.save()

    def notify_creation(self):
        activate(settings.LANGUAGE_CODE)
        subject = _('Creation of your Openrunner Account')
        message = ''.join([
            ugettext('Your Openrunner Account has been created successfully.\n'),
            ugettext('Below are the data you need to login on '),
            settings.OPENRUNNER_HOST,
            ugettext('\nLogin : '),
            self.promoter.user.username,
            ugettext('\nPassword : '),
            self.password,
        ])
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL,
                  [self.promoter.user.email])  # pylint: disable=E1101

    def get_routes_request_url(self):
        url = ''.join([settings.OPENRUNNER_HOST,
                       settings.OPENRUNNER_GET_ROUTES,
                       self.promoter.user.username])  # pylint: disable=E1101
        return url

    def get_routes(self):
        try:
            response = requests.get(self.get_routes_request_url())
        except:
            return {}
        else:
            return response.json()['routelist']


@python_2_unicode_compatible
class StructureType(models.Model):
    type_of_structure = models.CharField(_('type of structure'),
                                         max_length=255)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('type of structure')
        verbose_name_plural = _('types of structure')

    def __str__(self):
        return self.type_of_structure


@python_2_unicode_compatible
class Structure(models.Model):
    name = models.CharField(
        _('name'),
        help_text=_('structure name helptext'),
        max_length=200,
        unique=True,
    )
    type_of_structure = models.ForeignKey(
        StructureType,
        verbose_name=_('structure form'),
        help_text=_('structure form helptext'),
    )
    address = models.CharField(_('address'), max_length=255)
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )
    phone = models.CharField(_('phone number'), max_length=14)
    website = models.URLField(_('website'), max_length=200, blank=True)
    promoter = models.OneToOneField(Promoter, verbose_name=_('promoter'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('structure')

    def __str__(self):
        return self.name.title()

    def get_absolute_url(self):
        url = ''.join(['events:dashboard'])
        return reverse(url)


@receiver(post_save, sender=Structure)
def create_openrunner_account(created, instance, **kwargs):
    if created:
        instance.promoter.create_openrunner_account()


@python_2_unicode_compatible
class Event(models.Model):
    creation_date = models.DateField(_('creation date'), auto_now_add=True, blank=True, null=True)
    structure = models.ForeignKey(
        Structure,
        verbose_name=_('structure'),
    )
    name = models.CharField(_('event name'), max_length=255, unique=True)
    begin_date = models.DateTimeField(_('begin date'))
    end_date = models.DateTimeField(_('end date'))
    description = models.TextField(_('description'),
                                   help_text=_('description helptext'))
    activity = models.ForeignKey(Activity, verbose_name=_('activity'))
    registered_calendar = models.BooleanField(
        _("event is registered on federations's calendar"),
        default=False,
    )
    departure_city = models.ForeignKey(
        Commune,
        verbose_name=_('departure city'),
        limit_choices_to=CHOICES,
    )
    other_departments_crossed = models.ManyToManyField(
        Department,
        limit_choices_to={'name__regex': r'^(?!42)'},
        verbose_name=_('other departments crossed'),
        null=True,
        blank=True,
    )
    crossed_cities = models.ManyToManyField(
        Commune,
        verbose_name=_('crossed cities'),
        related_name="%(app_label)s_%(class)s_crossed_by",
        limit_choices_to=CHOICES,
    )
    openrunner_route = models.CommaSeparatedIntegerField(
        _('openrunner route'),
        max_length=255,
        blank=True,
        help_text=_('openrunner route help text'),
    )
    contact = GenericRelation(Contact, verbose_name=_('contact'))
    number_of_entries = models.IntegerField(
        _('number of entries'),
        help_text=_('number of entries helptext'))
    max_audience = models.PositiveIntegerField(_('max audience'))

    # markup info
    markup_convention = models.BooleanField(
        _('you are signatory of the 42 markup temporary convention'),
        help_text="Voir : <a href='/charteecob/' target='_blank'>Charte Ecobalisage</a>",
        default=False,
    )
    rubalise_markup = models.BooleanField(_('rubalise markup'), default=False)
    bio_rubalise_markup = models.BooleanField(_('bio rubalise markup'),
                                              default=False)
    chalk_markup = models.BooleanField(_('chalk markup'), default=False)
    paneling_markup = models.BooleanField(_('paneling markup'), default=False)
    ground_markup = models.BooleanField(_('ground markup'), default=False)
    sticker_markup = models.BooleanField(_('sticker markup'), default=False)
    lime_markup = models.BooleanField(_('lime markup'), default=False)
    plaster_markup = models.BooleanField(_('plaster markup'), default=False)
    confetti_markup = models.BooleanField(_('confetti markup'), default=False)
    paint_markup = models.BooleanField(_('paint markup'), default=False)
    acrylic_markup = models.BooleanField(_('acrylic markup'), default=False)
    withdrawn_markup = models.BooleanField(_('withdrawn markup'),
                                           default=False)
    oneway_roads = models.TextField(_('one way roads'), blank=True,
                                    help_text=_('one way roads help text'))
    closed_roads = models.TextField(_('closed roads'), blank=True,
                                    help_text=_('closed roads help text'))

    # natura 2000 related questions
    big_budget = models.BooleanField(
        _("event's budget is over 100 000 euros"),
        default=False,
    )
    big_title = models.BooleanField(
        _('(inter)national title delivred'),
        default=False,
    )
    lucrative = models.BooleanField(
        _('event of a lucrative type and gathering more than 1500 people'),
        default=False,
    )
    motor_on_closed_road = models.BooleanField(
        _('event with motorized vehicules on closed roads'),
        default=False,
    )
    motor_on_natura2000 = models.BooleanField(
        _('event with motorized vehicules on open roads'
          ' and on natura 2000 perimeter'),
        default=False,
    )
    approval_request = models.BooleanField(
        _('event with racetrack approval request'),
        default=False,
    )

    is_on_rnr = models.BooleanField(
        _('is on rnr'),
        default=False,
        help_text=_('*RNR : Regional Natural Reserve'))
    # attached files
    cartography = models.FileField(
        upload_to='cartographie/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('cartography'),
        max_length=100,
    )
    event_rules = models.FileField(
        upload_to='reglements/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('event rules'),
        max_length=100,
    )
    promoter_commitment = models.FileField(
        upload_to='engagements/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('promoter commitment'),
        help_text=_(
            """a template is available <a target='_blank' """
            """href='/engagement_organisateur/autorisation/'>here</a>"""
        ),
        max_length=100,
    )
    insurance_certificate = models.FileField(
        upload_to='attestation_assurance/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('insurance certificate'),
        help_text=_('insurance policy helptext'),
        max_length=150,
    )
    safety_provisions = models.FileField(
        upload_to='securite/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('safety provisions'),
        help_text=_('safety provisions helptext'),
        max_length=100,
    )
    doctor_attendance = models.FileField(
        upload_to='attestations_presence_medecin/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('doctor attendance'),
        max_length=100,
    )
    rounds_safety = models.FileField(
        upload_to='securisation_epreuves/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('rounds safety'),
        help_text=_('rounds safety help text'),
        max_length=100,
    )
    additional_docs = models.FileField(
        upload_to='doc_complementaires/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('additional docs'),
        max_length=100,
    )
    objects = InheritanceManager()

    class Meta:  # pylint: disable=C1001
        verbose_name = _('event')

    def __str__(self):
        return self.name.title()

    def get_absolute_url(self):
        url = ''.join(['events:',
                       ContentType.objects.get_for_model(self).model,
                       '_detail'])
        return reverse(url, kwargs={'pk': self.pk})

    def has_natura2000evaluation(self):
        return hasattr(self, 'natura2000evaluation')
    has_natura2000evaluation.boolean = True
    has_natura2000evaluation.short_description = _('subject to natura 2000 evaluation')

    def has_rnrevaluation(self):
        return hasattr(self, 'rnrevaluation')
    has_rnrevaluation.boolean = True
    has_rnrevaluation.short_description = _('subject to rnr evaluation')

    def legal_delay(self):
        '''
        Returns the legal delay for declaring or sending authorization request
        in days
        '''
        return 30

    def get_limit_date(self):
        '''
        Returns limit date for declaring or sending authorization request
        '''
        return (self.begin_date - datetime.timedelta(
            days=self.legal_delay()
        )).replace(hour=23, minute=59)

    def two_weeks_left(self):
        '''
        Returns true or false according to the number of weeks left
        (under 2 weeks or more than 2 weeks)
        '''
        if self.get_limit_date().date() < (datetime.date.today() +
                                           datetime.timedelta(weeks=2)):
            return True
        else:
            return False

    def delay_exceeded(self):
        '''
        If official delay to declare or send the authorization request
        is passed, return True, else, return False
        '''
        if datetime.date.today() > self.get_limit_date().date():
            return True
        else:
            return False

    def get_event_routes(self):
        # pylint: disable=E1101
        promoter_routes = self.structure.promoter.openrunneraccount.get_routes()  # noqa
        return {route: promoter_routes[route] for route in self.openrunner_route.split(',') if route in promoter_routes}  # noqa
        # pylint: enable=E1101

    def get_same_day_events(self):
        '''
        Returns all events taking place the same days
        '''
        return Event.objects.filter(
            Q(begin_date__range=(self.begin_date, self.end_date)) |
            Q(begin_date__lte=self.begin_date, end_date__gte=self.begin_date)
        )

    def get_same_day_same_town_events(self):
        '''
        Returns all events taking place :
        - the same days
        - in the same cities
        '''
        return self.get_same_day_events().filter(
            Q(departure_city=self.departure_city) |
            Q(crossed_cities__in=self.crossed_cities.all())  # noqa pylint: disable=E1101
        ).distinct()

    def get_conflicts_map_url(self):
        '''
        Builds the url to display events routes on the same openrunner map
        '''
        possible_conflicts = self.get_same_day_same_town_events()
        routes_ids = [route for event in possible_conflicts if event.openrunner_route for route in event.openrunner_route.split(',')]  # noqa
        if possible_conflicts.count() > 1:
            if routes_ids:
                return ''.join([
                    settings.OPENRUNNER_HOST,
                    settings.OPENRUNNER_MULTI_ROUTES_DISPLAY,
                    ','.join(routes_ids),
                ])
            else:
                return False
        else:
            return False

    def conflicts_maybe_incomplete(self):
        '''
        Returns True if one or more concerned events don't have
        any openrunner route
        '''
        result = []
        possible_conflicts = self.get_same_day_same_town_events()
        for event in possible_conflicts:
            if not event.openrunner_route:
                result.append(event.name)
        return result

    # pylint: disable=E1101
    def display_rnr_eval_panel(self):
        if self.is_on_rnr and not hasattr(self, 'rnrevaluation'):
            return True
        else:
            return False

    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluation') and
                (self.big_budget or self.lucrative)):
            return True
        else:
            return False
    # pylint: enable=E1101

    def get_breadcrumbs(self):
        '''
        Returns breadcrumbs representing the advencement of the file
        displayed to the user
        '''
        breadcrumbs = [[_('description'), 1]]
        # pylint: disable=E1101
        if self.display_rnr_eval_panel():
            breadcrumbs.append([_('RNR evaluation'), 0])
        elif hasattr(self, 'rnrevaluation'):
            breadcrumbs.append([_('RNR evaluation'), 1])
        if self.display_natura2000_eval_panel():
            breadcrumbs.append([_('natura2000 evaluation'), 0])
        elif hasattr(self, 'natura2000evaluation'):
            breadcrumbs.append([_('natura2000 evaluation'), 1])
        final, exists = self.get_final_breadcrumb()
        # pylint: enable=E1101
        if exists:
            breadcrumbs.append(final)
        return breadcrumbs

    def ready_for_instruction(self):
        ready = True
        if not self.event_rules:
            ready = False
        if not self.promoter_commitment:
            ready = False
        if not self.safety_provisions:
            ready = False
        return ready

    def processing(self):
        try:
            self.eventauthorization  # pylint: disable=E1101
        except:
            try:
                self.eventdeclaration  # pylint: disable=E1101
            except:
                return False
            return True
        return True
    processing.boolean = True
    processing.short_description = _('form validated')

    def missing_additional_data(self):
        return self.additionaldata_set.filter(attached_document='')


@python_2_unicode_compatible
class AdditionalData(models.Model):
    event = models.ForeignKey(Event)
    desired_information = models.CharField(_('desired information'),
                                           max_length=255)
    attached_document = models.FileField(
        upload_to='additional_data/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('document'),
        max_length=100,
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('additionnal data')
        verbose_name_plural = _('additionnal datas')

    def __str__(self):
        return self.desired_information

    def log_data_provision(self):
        add_log_entry(
            agents=[self.event.structure.promoter],
            action=_('additional data provided : ') + self.desired_information,
            event=self.event
        )

    def log_data_request(self, prefecture):
        add_log_entry(
            agents=prefecture.instructor_set.all(),
            action=_('additional data requested : ') + self.desired_information,
            event=self.event
        )

    def notify_data_provision(self, prefecture):
        add_notification_entry(
            event=self.event,
            agents=prefecture.instructor_set.all(),
            subject=_('additional data provided : ') + self.desired_information,
            content_object=self.event.structure,
            institutional=prefecture,
        )

    def notify_data_request(self, prefecture):
        add_notification_entry(
            agents=[self.event.structure.promoter],
            event=self.event,
            subject=_('additional data requested : ') + self.desired_information,
            content_object=prefecture,
        )

    def get_prefecture(self):
        return self.event.departure_city.arrondissement.prefecture

    def request_data(self):
        prefecture = self.get_prefecture()
        self.notify_data_request(prefecture)
        self.log_data_request(prefecture)

    def provide_data(self):
        prefecture = self.get_prefecture()
        self.notify_data_provision(prefecture)
        self.log_data_provision()


@receiver(post_save, sender=AdditionalData)
def log_data_request_creation(created, instance, **kwargs):
    if created:
        instance.request_data()


class DeclaredNonMotorizedEvent(Event):
    GROUPED_TRAFFIC_CHOICES = (
        ('p', _('more than 75 pedestrians')),
        ('c', _('more than 50 cycles or other vehicles (motorized or not)')),
        ('h', _('more than 25 horses or other animals')),
    )

    grouped_traffic = models.CharField(
        _('grouped traffic'), max_length=1,
        choices=GROUPED_TRAFFIC_CHOICES,
    )
    support_vehicles_number = models.PositiveIntegerField(
        _('number of support vehicles'),
        help_text=_('number of support vehicles'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('declared non motorized event')
        verbose_name_plural = _('declared non motorized events')

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.eventdeclaration  # pylint: disable=E1101
        except:
            breadcrumb = [_('declaration not sent'), 0]
        else:
            breadcrumb = [_('declaration sent'), 1]
        return breadcrumb, exists


class AuthorizedNonMotorizedEvent(Event):
    multisport_federation = models.ForeignKey(
        MultiSportFederation,
        verbose_name=_('multi-sport federation'),
        blank=True,
        null=True,
    )
    signalers_list = models.FileField(
        upload_to='listes_signaleurs/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('signalers list'),
        max_length=100,
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('authorized non motorized event')
        verbose_name_plural = _('authorized non motorized events')

    def legal_delay(self):
        # pylint: disable=E1101
        if self.other_departments_crossed.count() == 0:
            return 60
        else:
            return 90
        # pylint: enable=E1101

    def display_natura2000_eval_panel(self):
        # pylint: disable=E1101
        if (not hasattr(self, 'natura2000evaluation') and
                (self.big_budget or self.lucrative or self.big_title)):
            return True
        else:
            return False
        # pylint: enable=E1101

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.eventauthorization  # pylint: disable=E1101
        except:
            breadcrumb = [_('authorization request not sent'), 0]
        else:
            breadcrumb = [_('authorization request sent'), 1]
        return breadcrumb, exists

    def ready_for_instruction(self):  # pylint: disable=E1002
        ready = super(AuthorizedNonMotorizedEvent,
                      self).ready_for_instruction()
        if not self.signalers_list:
            ready = False
        return ready


class MotorizedConcentration(Event):
    vehicles = models.TextField(_('type and number of vehicles'))
    big_concentration = models.BooleanField(_('more than 200 cars'
                                              ' or 400 mv'),
                                            default=False)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('motorized concentration')
        verbose_name_plural = _('motorized concentrations')

    def legal_delay(self):
        if self.big_concentration:
            return 90
        else:
            return 60

    # pylint: disable=E1101
    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluation') and
                (self.big_budget or self.lucrative or
                 self.big_title or self.motor_on_natura2000)):
            return True
        else:
            return False
    # pylint: enable=E1101

    def get_final_breadcrumb(self):
        # pylint: disable=E1101
        if self.big_concentration:
            try:
                self.eventauthorization
            except:
                breadcrumb = [_('authorization request not sent'), 0]
            else:
                breadcrumb = [_('authorization request sent'), 1]
        else:
            try:
                self.eventdeclaration
            except:
                breadcrumb = [_('declaration not sent'), 0]
            else:
                breadcrumb = [_('declaration sent'), 1]
        # pylint: enable=E1101
        return breadcrumb, True

    def ready_for_instruction(self):  # pylint: disable=E1002
        ready = super(MotorizedConcentration, self).ready_for_instruction()
        if self.big_concentration:
            if not self.rounds_safety:
                ready = False
        return ready


class MotorizedEvent(Event):
    vehicles = models.TextField(_('type and number of vehicles'))
    delegate_federation_agr = models.FileField(
        upload_to='avis_federation_delegataire/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('delegate federation agreement'),
        max_length=100,
    )
    public_zone_map = models.FileField(
        upload_to='carte_zone_publique/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('public zone map'),
        max_length=100,
    )
    commissioners = models.FileField(
        upload_to='postes_commissaires/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('list of commissioners posts'),
        max_length=100,
    )
    tech_promoter_certificate = models.FileField(
        upload_to='attestations_orga_tech/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('technical promoter certificate'),
        max_length=100,
    )
    hourly_itinerary = models.FileField(
        upload_to='itineraires_horaires/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('hourly itinerary'),
        max_length=100,
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('motorized event')
        verbose_name_plural = _('motorized events')

    def legal_delay(self):
        return 90

    # pylint: disable=E1101
    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluation') and
                (self.big_budget or self.lucrative or
                 self.big_title or self.motor_on_natura2000)):
            return True
        else:
            return False
    # pylint: enable=E1101

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.eventauthorization  # pylint: disable=E1101
        except:
            breadcrumb = [_('authorization request not sent'), 0]
        else:
            breadcrumb = [_('authorization request sent'), 1]
        return breadcrumb, exists

    def ready_for_instruction(self):  # pylint: disable=E1002
        ready = super(MotorizedEvent, self).ready_for_instruction()
        if not self.commissioners:
            ready = False
        if not self.tech_promoter_certificate:
            ready = False
        if not self.hourly_itinerary:
            ready = False
        return ready


class MotorizedRace(Event):
    SUPPORT_CHOICES = (
        ('s', _('speedway')),
        ('g', _('ground')),
        ('r', _('route')),
    )

    support = models.CharField(_('type of support'), max_length=1,
                               choices=SUPPORT_CHOICES)
    mass_map = models.FileField(
        upload_to='plan_masse/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('mass map'),
        max_length=100,
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('motorized race')
        verbose_name_plural = _('motorized races')

    def legal_delay(self):
        return 90

    # pylint: disable=E1101
    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluation') and
                (self.big_budget or self.lucrative or
                 self.big_title or self.motor_on_closed_road or
                 self.approval_request)):
            return True
        else:
            return False
    # pylint: enable=E1101

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.eventauthorization  # pylint: disable=E1101
        except:
            breadcrumb = [_('authorization request not sent'), 0]
        else:
            breadcrumb = [_('authorization request sent'), 1]
        return breadcrumb, exists


@receiver(post_save, sender=AuthorizedNonMotorizedEvent)
@receiver(post_save, sender=DeclaredNonMotorizedEvent)
@receiver(post_save, sender=MotorizedConcentration)
@receiver(post_save, sender=MotorizedEvent)
@receiver(post_save, sender=MotorizedRace)
def log_event_creation(created, instance, **kwargs):
    # disable log creation during fixture loading
    if kwargs['raw']:
        return
    if created:
        instance.action_set.create(user=instance.structure.promoter.user,
                                   action=_('event created'))
