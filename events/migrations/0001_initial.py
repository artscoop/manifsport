# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('administrative_division', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sports', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='name')),
                ('begin_date', models.DateTimeField(verbose_name='begin date')),
                ('end_date', models.DateTimeField(verbose_name='end date')),
                ('description', models.TextField(help_text='description helptext', verbose_name='description')),
                ('registered_calendar', models.BooleanField(default=False, verbose_name="event is registered on federations's calendar")),
                ('openrunner_route', models.CommaSeparatedIntegerField(help_text='openrunner route help text', max_length=255, verbose_name='openrunner route', blank=True)),
                ('number_of_entries', models.IntegerField(help_text='number of entries helptext', verbose_name='number of entries')),
                ('max_audience', models.PositiveIntegerField(verbose_name='max audience')),
                ('markup_convention', models.BooleanField(default=False, verbose_name='you are signatory of the 42 markup temporary convention')),
                ('rubalise_markup', models.BooleanField(default=False, verbose_name='rubalise markup')),
                ('bio_rubalise_markup', models.BooleanField(default=False, verbose_name='bio rubalise markup')),
                ('paneling_markup', models.BooleanField(default=False, verbose_name='paneling markup')),
                ('ground_markup', models.BooleanField(default=False, verbose_name='ground markup')),
                ('sticker_markup', models.BooleanField(default=False, verbose_name='sticker markup')),
                ('lime_markup', models.BooleanField(default=False, verbose_name='lime markup')),
                ('plaster_markup', models.BooleanField(default=False, verbose_name='plaster markup')),
                ('confetti_markup', models.BooleanField(default=False, verbose_name='confetti markup')),
                ('paint_markup', models.BooleanField(default=False, verbose_name='paint markup')),
                ('acrylic_markup', models.BooleanField(default=False, verbose_name='acrylic markup')),
                ('withdrawn_markup', models.BooleanField(default=False, verbose_name='withdrawn markup')),
                ('big_budget', models.BooleanField(default=False, verbose_name="event's budget is over 100 000 euros")),
                ('big_title', models.BooleanField(default=False, verbose_name='(inter)national title delivred')),
                ('lucrative', models.BooleanField(default=False, verbose_name='event of a lucrative type and gathering more than 1500 people')),
                ('motor_on_closed_road', models.BooleanField(default=False, verbose_name='event with motorized vehicules on closed roads')),
                ('motor_on_natura2000', models.BooleanField(default=False, verbose_name='event with motorized vehicules on open roads and on natura 2000 perimeter')),
                ('approval_request', models.BooleanField(default=False, verbose_name='event with racetrack approval request')),
                ('is_on_rnr', models.BooleanField(default=False, help_text='*RNR : Regional Natural Reserve', verbose_name='is on rnr')),
                ('cartography', models.FileField(upload_to='cartographie/%Y/%m/%d', null=True, verbose_name='cartography', blank=True)),
                ('event_rules', models.FileField(upload_to='reglements/%Y/%m/%d', null=True, verbose_name='event rules', blank=True)),
                ('promoter_commitment', models.FileField(help_text="a template is available <a target='_blank' href='http://www.manifestationsportiveloire.fr/sites/default/files/files/engagorg/engagement_organisateur_A.pdf'>here</a>", upload_to='engagements/%Y/%m/%d', null=True, verbose_name='promoter commitment', blank=True)),
                ('insurance_certificate', models.FileField(upload_to='attestation_assurance/%Y/%m/%d', max_length=150, blank=True, help_text='insurance policy helptext', null=True, verbose_name='insurance certificate')),
                ('safety_provisions', models.FileField(help_text='safety provisions helptext', upload_to='securite/%Y/%m/%d', null=True, verbose_name='safety provisions', blank=True)),
                ('doctor_attendance', models.FileField(upload_to='attestations_presence_medecin/%Y/%m/%d', null=True, verbose_name='doctor attendance', blank=True)),
                ('rounds_safety', models.FileField(help_text='rounds safety help text', upload_to='securisation_epreuves/%Y/%m/%d', null=True, verbose_name='rounds safety', blank=True)),
                ('additional_docs', models.FileField(upload_to='doc_complementaires/%Y/%m/%d', null=True, verbose_name='additional docs', blank=True)),
                ('dps_needed', models.BooleanField(default=True, verbose_name='dps needed')),
            ],
            options={
                'verbose_name': 'event',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeclaredNonMotorizedEvent',
            fields=[
                ('event_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='events.Event')),
                ('grouped_traffic', models.CharField(max_length=1, verbose_name='grouped traffic', choices=[('p', 'more than 75 pedestrians'), ('c', 'more than 50 cycles or other vehicles (motorized or not)'), ('h', 'more than 25 horses or other animals')])),
                ('support_vehicles_number', models.PositiveIntegerField(help_text='number of support vehicles helptext', verbose_name='number of support vehicles')),
            ],
            options={
                'verbose_name': 'declared non motorized event',
                'verbose_name_plural': 'declared non motorized events',
            },
            bases=('events.event',),
        ),
        migrations.CreateModel(
            name='AuthorizedNonMotorizedEvent',
            fields=[
                ('event_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='events.Event')),
                ('signalers_list', models.FileField(upload_to='listes_signaleurs/%Y/%m/%d', null=True, verbose_name='signalers list', blank=True)),
                ('multisport_federation', models.ForeignKey(verbose_name='multi-sport federation', blank=True, to='sports.MultiSportFederation', null=True)),
            ],
            options={
                'verbose_name': 'authorized non motorized event',
                'verbose_name_plural': 'authorized non motorized events',
            },
            bases=('events.event',),
        ),
        migrations.CreateModel(
            name='MotorizedConcentration',
            fields=[
                ('event_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='events.Event')),
                ('vehicles', models.TextField(verbose_name='type and number of vehicles')),
                ('big_concentration', models.BooleanField(default=False, verbose_name='more than 200 cars or 400 mv')),
            ],
            options={
                'verbose_name': 'motorized concentration',
                'verbose_name_plural': 'motorized concentrations',
            },
            bases=('events.event',),
        ),
        migrations.CreateModel(
            name='MotorizedEvent',
            fields=[
                ('event_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='events.Event')),
                ('vehicles', models.TextField(verbose_name='type and number of vehicles')),
                ('delegate_federation_agr', models.FileField(upload_to='avis_federation_delegataire/%Y/%m/%d', null=True, verbose_name='delegate federation agreement', blank=True)),
                ('public_zone_map', models.FileField(upload_to='carte_zone_publique/%Y/%m/%d', null=True, verbose_name='public zone map', blank=True)),
                ('commissioners', models.FileField(upload_to='postes_commissaires/%Y/%m/%d', null=True, verbose_name='list of commissioners posts', blank=True)),
                ('tech_promoter_certificate', models.FileField(upload_to='attestations_orga_tech/%Y/%m/%d', null=True, verbose_name='technical promoter certificate', blank=True)),
                ('hourly_itinerary', models.FileField(upload_to='itineraires_horaires/%Y/%m/%d', null=True, verbose_name='hourly itinerary', blank=True)),
            ],
            options={
                'verbose_name': 'motorized event',
                'verbose_name_plural': 'motorized events',
            },
            bases=('events.event',),
        ),
        migrations.CreateModel(
            name='MotorizedRace',
            fields=[
                ('event_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='events.Event')),
                ('support', models.CharField(max_length=1, verbose_name='type of support', choices=[('s', 'speedway'), ('g', 'ground'), ('r', 'route')])),
                ('mass_map', models.FileField(upload_to='plan_masse/%Y/%m/%d', null=True, verbose_name='mass map', blank=True)),
            ],
            options={
                'verbose_name': 'motorized race',
                'verbose_name_plural': 'motorized races',
            },
            bases=('events.event',),
        ),
        migrations.CreateModel(
            name='OpenRunnerAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.BooleanField(default=False, verbose_name='created')),
                ('creation_date', models.DateTimeField(null=True, verbose_name='creation date', blank=True)),
                ('password', models.CharField(max_length=255, verbose_name='password', blank=True)),
                ('error_message', models.CharField(max_length=255, verbose_name='error message', blank=True)),
            ],
            options={
                'verbose_name': 'openrunner account',
                'verbose_name_plural': 'openrunner accounts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Promoter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cgu', models.BooleanField(default=False, verbose_name='cgu agreement')),
                ('user', models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'promoter',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Structure',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='structure name helptext', unique=True, max_length=200, verbose_name='name')),
                ('address', models.CharField(max_length=255, verbose_name='address')),
                ('phone', models.CharField(max_length=14, verbose_name='phone number')),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
                ('website', models.URLField(verbose_name='website', blank=True)),
                ('commune', models.ForeignKey(verbose_name='commune', to='administrative_division.Commune')),
                ('promoter', models.OneToOneField(verbose_name='promoter', to='events.Promoter')),
            ],
            options={
                'verbose_name': 'structure',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StructureType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_of_structure', models.CharField(max_length=255, verbose_name='type of structure')),
            ],
            options={
                'verbose_name': 'type of structure',
                'verbose_name_plural': 'types of structure',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='structure',
            name='type_of_structure',
            field=models.ForeignKey(verbose_name='structure form', to='events.StructureType', help_text='structure form helptext'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='openrunneraccount',
            name='promoter',
            field=models.OneToOneField(verbose_name='promoter', to='events.Promoter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='activity',
            field=models.ForeignKey(verbose_name='activity', to='sports.Activity'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='crossed_cities',
            field=models.ManyToManyField(related_name='events_event_crossed_by', verbose_name='crossed cities', to='administrative_division.Commune'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='departure_city',
            field=models.ForeignKey(related_name='events_event_hosts', verbose_name='departure city', to='administrative_division.Commune'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='other_departments_crossed',
            field=models.ManyToManyField(related_name='events_event_crossed', null=True, verbose_name='other departments crossed', to='administrative_division.Department', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='structure',
            field=models.ForeignKey(related_name='events_event_organize', verbose_name='structure', to='events.Structure'),
            preserve_default=True,
        ),
    ]
