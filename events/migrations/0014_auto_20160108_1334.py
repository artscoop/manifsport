# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0013_event_creation_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='declarednonmotorizedevent',
            name='support_vehicles_number',
            field=models.PositiveIntegerField(help_text='number of support vehicles', verbose_name='number of support vehicles'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='name',
            field=models.CharField(unique=True, max_length=255, verbose_name='event name'),
            preserve_default=True,
        ),
    ]
