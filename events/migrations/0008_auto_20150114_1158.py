# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0007_additionaldata'),
    ]

    operations = [
        migrations.AlterField(
            model_name='additionaldata',
            name='attached_document',
            field=models.FileField(upload_to='additional_data/%Y/%m/%d', null=True, verbose_name='document', blank=True),
            preserve_default=True,
        ),
    ]
