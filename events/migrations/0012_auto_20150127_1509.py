# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0011_auto_20150121_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='promoter_commitment',
            field=models.FileField(help_text="a template is available <a target='_blank' href='/engagement_organisateur/autorisation/'>here</a>", upload_to='engagements/%Y/%m/%d', null=True, verbose_name='promoter commitment', blank=True),
            preserve_default=True,
        ),
    ]
