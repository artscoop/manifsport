# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_auto_20150109_1444'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('desired_information', models.CharField(max_length=255, verbose_name='desired information')),
                ('attached_document', models.FileField(upload_to='additional_data/%Y/%m/%d', null=True, verbose_name='attached_document', blank=True)),
                ('event', models.ForeignKey(to='events.Event')),
            ],
            options={
                'verbose_name': 'additionnal data',
                'verbose_name_plural': 'additionnal datas',
            },
            bases=(models.Model,),
        ),
    ]
