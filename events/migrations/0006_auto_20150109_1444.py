# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_remove_structure_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='markup_convention',
            field=models.BooleanField(default=False, help_text="Voir : <a href='/charteecob/' target='_blank'>Charte Ecobalisage</a>", verbose_name='you are signatory of the 42 markup temporary convention'),
            preserve_default=True,
        ),
    ]
