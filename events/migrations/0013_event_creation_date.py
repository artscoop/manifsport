# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0012_auto_20150127_1509'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='creation_date',
            field=models.DateField(auto_now_add=True, verbose_name='creation date', null=True),
            preserve_default=True,
        ),
    ]
