# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_remove_event_dps_needed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='structure',
            name='email',
        ),
    ]
