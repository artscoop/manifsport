# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0008_auto_20150114_1158'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='closed_roads',
            field=models.TextField(help_text='closed roads help text', verbose_name='closed roads', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='oneway_roads',
            field=models.TextField(help_text='one way roads help text', verbose_name='one way roads', blank=True),
            preserve_default=True,
        ),
    ]
