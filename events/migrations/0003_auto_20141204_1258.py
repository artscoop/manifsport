# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_event_chalk_markup'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='departure_city',
            field=models.ForeignKey(verbose_name='departure city', to='administrative_division.Commune'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='other_departments_crossed',
            field=models.ManyToManyField(to='administrative_division.Department', null=True, verbose_name='other departments crossed', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='structure',
            field=models.ForeignKey(verbose_name='structure', to='events.Structure'),
            preserve_default=True,
        ),
    ]
