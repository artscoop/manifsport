# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='chalk_markup',
            field=models.BooleanField(default=False, verbose_name='chalk markup'),
            preserve_default=True,
        ),
    ]
