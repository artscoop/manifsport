# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20141204_1258'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='dps_needed',
        ),
    ]
