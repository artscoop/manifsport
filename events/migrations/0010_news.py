# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
        ('events', '0009_auto_20150118_1511'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(verbose_name='description')),
                ('page', models.ForeignKey(verbose_name='page', to='flatpages.FlatPage')),
            ],
            options={
                'verbose_name': 'platform news',
            },
            bases=(models.Model,),
        ),
    ]
