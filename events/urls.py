from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import HomePage
from .views import Dashboard
from .views import Calendar
from .views import FilteredCalendar
from .views import StructureCreate
from .views import CreateRoute
from .views import EventChoose
from .views import MotorizedEventChoose
from .views import NonMotorizedEventChoose
from .views import DeclaredNonMotorizedEventChoose
from .views import PublicRoadMotorizedEventChoose
from .views import EventPublicDetail

from .views import DeclaredNonMotorizedEventCreate
from .views import DeclaredNonMotorizedEventDetail
from .views import DeclaredNonMotorizedEventUpdate

from .views import AuthorizedNonMotorizedEventCreate
from .views import AuthorizedNonMotorizedEventDetail
from .views import AuthorizedNonMotorizedEventUpdate

from .views import MotorizedConcentrationCreate
from .views import MotorizedConcentrationDetail
from .views import MotorizedConcentrationUpdate

from .views import MotorizedEventCreate
from .views import MotorizedEventDetail
from .views import MotorizedEventUpdate

from .views import MotorizedRaceCreate
from .views import MotorizedRaceDetail
from .views import MotorizedRaceUpdate

from .views import AdditionalDataRequestView
from .views import AdditionalDataProvideView

from .views import MailToPromotersView
from .views import MailToTownHallsView
from .views import NewsListView

urlpatterns = patterns(
    # pylint: disable=E1120
    '',
    url(r'^$', HomePage.as_view(),
        name='home_page'),
    url(r'^mail_to_promoters/$', MailToPromotersView.as_view(),
        name='mail_to_promoters'),
    url(r'^mail_to_townhalls/$', MailToTownHallsView.as_view(),
        name='mail_to_townhalls'),
    url(r'^dashboard/$', Dashboard.as_view(),
        name="dashboard"),
    url(r'^calendar/$', Calendar.as_view(),
        name="calendar"),
    url(r'^news/$', NewsListView.as_view(), name="news_list"),
    url(r'^calendar/(?P<activity>\d+)/$', FilteredCalendar.as_view(),
        name="filtered_calendar"),
    url(r'^structure/add', StructureCreate.as_view(), name='structure_add'),

    url(r'^events/create_route/$', CreateRoute.as_view(),
        name='create_route'),
    url(r'^events/choose/$', EventChoose.as_view(),
        name='event_choose'),
    url(r'^motorizedevents/choose', MotorizedEventChoose.as_view(),
        name="motorizedevent_choose"),
    url(r'^nonmotorizedevents/choose', NonMotorizedEventChoose.as_view(),
        name="nonmotorizedevent_choose"),
    url(r'^declarednonmotorizedevents/choose',
        DeclaredNonMotorizedEventChoose.as_view(),
        name="declarednonmotorizedevent_choose"),
    url(r'^publicroadmotorizedevents/choose',
        PublicRoadMotorizedEventChoose.as_view(),
        name="publicroadmotorizedevent_choose"),
    url(r'^events/(?P<pk>\d+)/$',
        EventPublicDetail.as_view(),
        name="event_detail"),

    # DeclaredNonMotorizedEvent urls
    url(r'^declarednonmotorizedevents/add/$',
        DeclaredNonMotorizedEventCreate.as_view(),
        name="declarednonmotorizedevent_add"),
    url(r'^declarednonmotorizedevents/(?P<pk>\d+)/$',
        DeclaredNonMotorizedEventDetail.as_view(),
        name="declarednonmotorizedevent_detail"),
    url(r'^declarednonmotorizedevents/(?P<pk>\d+)/edit/$',
        DeclaredNonMotorizedEventUpdate.as_view(),
        name="declarednonmotorizedevent_update"),

    # AuthorizedNonMotorizedEvent urls
    url(r'^authorizednonmotorizedevents/add/$',
        AuthorizedNonMotorizedEventCreate.as_view(),
        name="authorizednonmotorizedevent_add"),
    url(r'^authorizednonmotorizedevents/(?P<pk>\d+)/$',
        AuthorizedNonMotorizedEventDetail.as_view(),
        name="authorizednonmotorizedevent_detail"),
    url(r'^authorizednonmotorizedevents/(?P<pk>\d+)/edit/$',
        AuthorizedNonMotorizedEventUpdate.as_view(),
        name="authorizednonmotorizedevent_update"),

    # MotorizedConcentration urls
    url(r'^motorizedconcentrations/add/$',
        MotorizedConcentrationCreate.as_view(),
        name="motorizedconcentration_add"),
    url(r'^motorizedconcentrations/(?P<pk>\d+)/$',
        MotorizedConcentrationDetail.as_view(),
        name="motorizedconcentration_detail"),
    url(r'^motorizedconcentrations/(?P<pk>\d+)/edit/$',
        MotorizedConcentrationUpdate.as_view(),
        name="motorizedconcentration_update"),

    # MotorizedEvent urls
    url(r'^motorizedevents/add/$',
        MotorizedEventCreate.as_view(),
        name="motorizedevent_add"),
    url(r'^motorizedevents/(?P<pk>\d+)/$',
        MotorizedEventDetail.as_view(),
        name="motorizedevent_detail"),
    url(r'^motorizedevents/(?P<pk>\d+)/edit/$',
        MotorizedEventUpdate.as_view(),
        name="motorizedevent_update"),

    # MotorizedRace urls
    url(r'^motorizedraces/add/$',
        MotorizedRaceCreate.as_view(),
        name="motorizedrace_add"),
    url(r'^motorizedraces/(?P<pk>\d+)/$',
        MotorizedRaceDetail.as_view(),
        name="motorizedrace_detail"),
    url(r'^motorizedraces/(?P<pk>\d+)/edit/$',
        MotorizedRaceUpdate.as_view(),
        name="motorizedrace_update"),
    url(r'^events/(?P<event_pk>\d+)/additional_data/add$',
        AdditionalDataRequestView.as_view(),
        name="additional_data_add"),
    url(r'^additional_data/(?P<pk>\d+)/$',
        AdditionalDataProvideView.as_view(),
        name="additional_data_provide"),
    # pylint: enable=E1120
)
