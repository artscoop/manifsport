from __future__ import unicode_literals

from django.contrib import admin

from .models import Prefecture

from .models import RoadSafetySquadron
from .models import FireAndRescueService
from .models import PublicSafetyDirection
from .models import GeneralCouncil
from .models import DepartmentalGendarmerieGroup
from .models import DepartmentalFireOpCenter

from .models import Instructor
from .models import Brigade
from .models import CGAgent
from .models import CGSuperior
from .models import CGServiceAgent
from .models import GGDAgent
from .models import DDSPAgent
from .models import EDSRAgent
from .models import BrigadeAgent
from .models import SDISAgent
from .models import CODISAgent
from .models import CISAgent
from .models import ServiceAgent
from .models import FederationAgent
from .models import TownHallAgent
from .models import FireService

from .models import CGDAgent
from .models import CompanyAgent
from .models import CommissariatAgent

from .models import CGService
from .models import Service

from .models import GendarmerieCompany
from .models import Company
from .models import Commissariat

from .models import Rescuer


admin.site.register(Prefecture)

# Prefecture related
admin.site.register(CGService)
admin.site.register(Service)

# Services
admin.site.register(RoadSafetySquadron)
admin.site.register(FireAndRescueService)
admin.site.register(PublicSafetyDirection)
admin.site.register(GeneralCouncil)
admin.site.register(DepartmentalGendarmerieGroup)
admin.site.register(DepartmentalFireOpCenter)
admin.site.register(Brigade)
admin.site.register(GendarmerieCompany)
admin.site.register(Company)
admin.site.register(Commissariat)
admin.site.register(FireService)

# Agents
admin.site.register(Instructor)
admin.site.register(CGAgent)
admin.site.register(CGSuperior)
admin.site.register(CGServiceAgent)
admin.site.register(GGDAgent)
admin.site.register(DDSPAgent)
admin.site.register(EDSRAgent)
admin.site.register(BrigadeAgent)
admin.site.register(SDISAgent)
admin.site.register(CODISAgent)
admin.site.register(CISAgent)
admin.site.register(ServiceAgent)
admin.site.register(FederationAgent)
admin.site.register(TownHallAgent)
admin.site.register(CGDAgent)
admin.site.register(CompanyAgent)
admin.site.register(CommissariatAgent)

# Observers
admin.site.register(Rescuer)
