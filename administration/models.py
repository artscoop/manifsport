from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from administrative_division.models import Arrondissement
from administrative_division.models import Commune
from administrative_division.models import Department
from administrative_division.models import CHOICES
from emergencies.models import FirstAidAssociation
from sports.models import Federation


@python_2_unicode_compatible
class DepartmentalGendarmerieGroup(models.Model):
    department = models.OneToOneField(Department, verbose_name=_('department'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('GGD')
        verbose_name_plural = _('GGD')

    def __str__(self):
        return ' '.join(['GGD', self.department.name])


@python_2_unicode_compatible
class GeneralCouncil(models.Model):
    department = models.OneToOneField(Department, verbose_name=_('department'))
    email = models.EmailField(_('e-mail'), max_length=200)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CG')
        verbose_name_plural = _('CG')

    def __str__(self):
        return ' '.join(['CG', self.department.name])


@python_2_unicode_compatible
class PublicSafetyDirection(models.Model):
    department = models.OneToOneField(Department, verbose_name=_('department'))
    email = models.EmailField(_('e-mail'), max_length=200)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('DDSP')
        verbose_name_plural = _('DDSP')

    def __str__(self):
        return ' '.join(['DDSP', self.department.name])


@python_2_unicode_compatible
class FireAndRescueService(models.Model):
    department = models.OneToOneField(Department, verbose_name=_('department'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS')
        verbose_name_plural = _('SDIS')

    def __str__(self):
        return ' '.join(['SDIS', self.department.name])


@python_2_unicode_compatible
class DepartmentalFireOpCenter(models.Model):
    department = models.OneToOneField(Department, verbose_name=_('department'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - CODIS')
        verbose_name_plural = _('SDIS - CODIS')

    def __str__(self):
        return ' '.join(['CODIS', self.department.name])


@python_2_unicode_compatible
class RoadSafetySquadron(models.Model):
    department = models.OneToOneField(Department, verbose_name=_('department'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('EDSR')
        verbose_name_plural = _('EDSR')

    def __str__(self):
        return ' '.join(['EDSR', self.department.name])


@python_2_unicode_compatible
class Prefecture(models.Model):
    sub_prefecture = models.BooleanField(_('sub-prefecture'), default=False)
    arrondissement = models.OneToOneField(Arrondissement,
                                          verbose_name=_('arrondissement'))
    email = models.EmailField(_('e-mail'), max_length=200)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('Prefecture')
        verbose_name_plural = _('Prefectures')

    def __str__(self):
        if self.sub_prefecture:
            return ' '.join([_('sub-prefecture of').capitalize(),
                             self.arrondissement.name])
        else:
            return ' '.join([_('prefecture of').capitalize(),
                             self.arrondissement.name])


@python_2_unicode_compatible
class Instructor(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'))
    prefecture = models.ForeignKey(Prefecture, verbose_name=_('prefecture'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('Prefecture - Instructeur')
        verbose_name_plural = _('Prefecture - Instructeurs')

    def __str__(self):
        return self.user.get_username()  # pylint: disable=E1101


@python_2_unicode_compatible
class Agent(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('agent')

    def __str__(self):
        return self.user.get_username()  # pylint: disable=E1101


@python_2_unicode_compatible
class SubAgent(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('sub-agent')

    def __str__(self):
        return self.user.get_username()  # pylint: disable=E1101


@python_2_unicode_compatible
class Observer(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('observer')

    def __str__(self):
        return self.user.get_username()  # pylint: disable=E1101


class Rescuer(Observer):
    association = models.ForeignKey(
        FirstAidAssociation,
        verbose_name=_('first aid association'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('rescuer')


###########################################################
# EDSR Related models
###########################################################
class GGDAgent(Agent):
    ggd = models.ForeignKey(
        DepartmentalGendarmerieGroup,
        verbose_name=_('departmental gendarmerie group'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('GGD - Agent')
        verbose_name_plural = _('GGD - Agents')


class EDSRAgent(Agent):
    road_safety_squadron = models.ForeignKey(
        RoadSafetySquadron,
        verbose_name=_('road safety squadron'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('EDSR - Agent')
        verbose_name_plural = _('EDSR - Agents')


@python_2_unicode_compatible
class GendarmerieCompany(models.Model):
    arrondissement = models.OneToOneField(Arrondissement,
                                          verbose_name=_('arrondissement'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CGD')
        verbose_name_plural = _('CGD')

    def __str__(self):
        return ' - '.join(['CGD', self.arrondissement.name])


class CGDAgent(SubAgent):
    cgd = models.ForeignKey(GendarmerieCompany, verbose_name=_('cgd'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CGD - Agent')
        verbose_name_plural = _('CGD - Agents')


@python_2_unicode_compatible
class Brigade(models.Model):
    KIND_CHOICES = (
        ('cob', _('community brigade')),
        ('bta', _('autonomous brigade')),
    )

    cgd = models.ForeignKey(GendarmerieCompany, verbose_name=_('cgd'))
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )
    kind = models.CharField(_('kind'), max_length=3, choices=KIND_CHOICES)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('EDSR - Brigade')
        verbose_name_plural = _('EDSR - Brigades')

    def __str__(self):
        return ' - '.join([self.kind.upper(),  # pylint: disable=E1101
                           self.commune.name])


class BrigadeAgent(Agent):
    brigade = models.ForeignKey(
        Brigade,
        verbose_name=_('brigade'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('EDSR - Brigade - Agent')
        verbose_name_plural = _('EDSR - Brigade - Agents')


###########################################################
# SDIS Related models
###########################################################
class CODISAgent(Agent):
    departmental_fire_op_center = models.ForeignKey(
        DepartmentalFireOpCenter,
        verbose_name=_('CODIS'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - CODIS - Agent')
        verbose_name_plural = _('SDIS - CODIS - Agents')


class SDISAgent(Agent):
    fire_and_rescue_service = models.ForeignKey(
        FireAndRescueService,
        verbose_name=_('fire and rescue service'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - Agent')
        verbose_name_plural = _('SDIS - Agents')


@python_2_unicode_compatible
class Company(models.Model):
    fire_and_rescue_service = models.ForeignKey(
        FireAndRescueService,
        verbose_name=_('fire and rescue service'),
    )
    number = models.CharField(_('company number'), max_length=3)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - Compagnie')
        verbose_name_plural = _('SDIS - Compagnies')
        ordering = ['number']

    def __str__(self):
        return ' '.join(['compagnie', self.number])


class CompanyAgent(SubAgent):
    company = models.ForeignKey(Company, verbose_name=_('company'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - Compagnie - Agent')
        verbose_name_plural = _('SDIS - Compagnie - Agents')


@python_2_unicode_compatible
class FireService(models.Model):
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )
    company = models.ForeignKey(
        Company,
        verbose_name=_('company'),
    )
    name = models.CharField(_('name'), max_length=255, blank=True)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - CIS')
        verbose_name_plural = _('SDIS - CIS')

    def __str__(self):
        return ' '.join(
            [sub_name for sub_name in [
                'CIS',
                self.commune.name.capitalize(),
                self.name] if sub_name]
            )


class CISAgent(Agent):
    fire_service = models.ForeignKey(
        FireService,
        verbose_name=_('CIS'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('SDIS - CIS - Agent')
        verbose_name_plural = _('SDIS - CIS - Agents')


###########################################################
# DDSP Related models
###########################################################
class DDSPAgent(Agent):
    public_safety_direction = models.ForeignKey(
        PublicSafetyDirection,
        verbose_name=_('public safety direction'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('DDSP - Agent')
        verbose_name_plural = _('DDSP - Agents')


@python_2_unicode_compatible
class Commissariat(models.Model):
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('DDSP - Commissariat')
        verbose_name_plural = _('DDSP - Commissariats')

    def __str__(self):
        return ' '.join(['commissariat', self.commune.name])


class CommissariatAgent(SubAgent):
    commissariat = models.ForeignKey(Commissariat,
                                     verbose_name=_('commissariat'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('DDSP - Commissariat - Agent')
        verbose_name_plural = _('DDSP - Commissariat - Agents')


###########################################################
# Services Related models
###########################################################
@python_2_unicode_compatible
class Service(models.Model):
    name = models.CharField(_('name'), max_length=255)
    department = models.ForeignKey(Department, verbose_name=_('department'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('Autre service')
        verbose_name_plural = _('Autres services')
        unique_together = (("name", "department"),)

    def __str__(self):
        # pylint: disable=E1101
        return ' - '.join([self.department.name, self.name])
        # pylint: enable=E1101


class ServiceAgent(Agent):
    service = models.ForeignKey(Service, verbose_name=_('service'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('Autre service - Agent')
        verbose_name_plural = _('Autre service - Agents')


###########################################################
# Federation Related models
###########################################################
class FederationAgent(Agent):
    federation = models.ForeignKey(Federation, verbose_name=_('federation'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('Federation - Agent')
        verbose_name_plural = _('Federation - Agents')


###########################################################
# TownHall Related models
###########################################################
class TownHallAgent(Agent):
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('municipal agent')
        verbose_name_plural = _('municipal agents')


###########################################################
# GeneralConcil Related models
###########################################################
class CGAgent(Agent):
    general_council = models.ForeignKey(
        GeneralCouncil,
        verbose_name=_('general council'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CG - Agent')
        verbose_name_plural = _('CG - Agents')


class CGSuperior(Agent):
    general_council = models.ForeignKey(
        GeneralCouncil,
        verbose_name=_('general council'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CG - Agent n+1')
        verbose_name_plural = _('CG - Agents n+1')


@python_2_unicode_compatible
class CGService(models.Model):
    SERVICE_TYPE_CHOICES = (
        ('STD', _('departmental technical service')),
        ('DTM', _('transports and mobility direction')),
    )

    general_council = models.ForeignKey(
        GeneralCouncil,
        verbose_name=_('general council'),
    )
    name = models.CharField(_('name'), max_length=255)
    service_type = models.CharField(_('service type'),
                                    max_length=3,
                                    choices=SERVICE_TYPE_CHOICES)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CG - Service')
        verbose_name_plural = _('CG - Services')

    def __str__(self):
        return ' '.join([self.service_type, self.name])


class CGServiceAgent(SubAgent):
    cg_service = models.ForeignKey(
        CGService,
        verbose_name=_('CG service'),
    )

    class Meta:  # pylint: disable=C1001
        verbose_name = _('CG - Service - Agent')
        verbose_name_plural = _('CG - Service - Agents')
