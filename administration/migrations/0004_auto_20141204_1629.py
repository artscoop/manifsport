# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_service_names(apps, schema_editor):
    Service = apps.get_model('administration', 'Service')
    names = {
        'cricr': 'CRICR',
        'hunt': 'Fédération de Chasse',
        'edf': 'EDF',
        'ia': 'Inspection Académique',
        'sncf': 'SNCF',
        'dirce': 'DIRCE',
        'samu': 'SAMU',
        'ars': 'ARS',
        'pnr': 'PNR',
        'oncfs': 'ONCFS',
        'onf': 'ONF',
        'epl': 'EPL',
        'brl': 'BRL',
        'rff': 'RFF',
        'sem': 'SEM',
        'roan': 'Roannais exploitation',
        'crs': 'CRS',
        'ddcs': 'DDCS',
        'see': 'DDT SEE-PNCV',
        'sat': 'DDT SAT Sécurité Routière',
        'cr': 'CR DCESE',
        'cdsr': 'CDSR',
    }
    for service in Service.objects.all():
        service.name = names[service.name]
        service.save()


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0003_auto_20141204_1629'),
    ]

    operations = [
        migrations.RunPython(update_service_names),
    ]
