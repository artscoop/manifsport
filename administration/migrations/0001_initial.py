# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('administrative_division', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sports', '0001_initial'),
        ('emergencies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'agent',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Brigade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kind', models.CharField(max_length=3, verbose_name='kind', choices=[('cob', 'community brigade'), ('bta', 'autonomous brigade')])),
            ],
            options={
                'verbose_name': 'brigade',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BrigadeAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
                ('brigade', models.ForeignKey(verbose_name='brigade', to='administration.Brigade')),
            ],
            options={
                'verbose_name': 'brigade agent',
                'verbose_name_plural': 'brigade agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='CGAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
            ],
            options={
                'verbose_name': 'cg agent',
                'verbose_name_plural': 'cg agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='CGService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('service_type', models.CharField(max_length=3, verbose_name='service type', choices=[('STD', 'departmental technical service'), ('DTM', 'transports and mobility direction')])),
            ],
            options={
                'verbose_name': 'CG service',
                'verbose_name_plural': 'CG services',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CGSuperior',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
            ],
            options={
                'verbose_name': 'cg superior',
                'verbose_name_plural': 'cg superiors',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='CISAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
            ],
            options={
                'verbose_name': 'cis agent',
                'verbose_name_plural': 'cis agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='CODISAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
            ],
            options={
                'verbose_name': 'codis agent',
                'verbose_name_plural': 'codis agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='Commissariat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commune', models.ForeignKey(verbose_name='commune', to='administrative_division.Commune')),
            ],
            options={
                'verbose_name': 'commissariat',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=3, verbose_name='company number')),
            ],
            options={
                'ordering': ['number'],
                'verbose_name': 'company',
                'verbose_name_plural': 'companies',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DDSPAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
            ],
            options={
                'verbose_name': 'ddsp agent',
                'verbose_name_plural': 'ddsp agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='DepartmentalFireOpCenter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('department', models.OneToOneField(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'CODIS',
                'verbose_name_plural': 'CODIS',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DepartmentalGendarmerieGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('department', models.OneToOneField(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'departmental gendarmerie group',
                'verbose_name_plural': 'departmental gendarmerie groups',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EDSRAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
            ],
            options={
                'verbose_name': 'edsr agent',
                'verbose_name_plural': 'edsr agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='FederationAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
                ('federation', models.ForeignKey(verbose_name='federation', to='sports.Federation')),
            ],
            options={
                'verbose_name': 'federal agent',
                'verbose_name_plural': 'federal agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='FireAndRescueService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('department', models.OneToOneField(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'fire and rescue service',
                'verbose_name_plural': 'fire and rescue services',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FireService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='name', blank=True)),
                ('commune', models.ForeignKey(verbose_name='commune', to='administrative_division.Commune')),
                ('company', models.ForeignKey(verbose_name='company', to='administration.Company')),
            ],
            options={
                'verbose_name': 'fire service',
                'verbose_name_plural': 'fire services',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GendarmerieCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arrondissement', models.OneToOneField(verbose_name='arrondissement', to='administrative_division.Arrondissement')),
            ],
            options={
                'verbose_name': 'gendarmerie company',
                'verbose_name_plural': 'gendarmerie companies',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GeneralCouncil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
                ('department', models.OneToOneField(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'general council',
                'verbose_name_plural': 'general councils',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GGDAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
                ('ggd', models.ForeignKey(verbose_name='departmental gendarmerie group', to='administration.DepartmentalGendarmerieGroup')),
            ],
            options={
                'verbose_name': 'ggd agent',
                'verbose_name_plural': 'ggd agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='Instructor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'instructor',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Observer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'observer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Prefecture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sub_prefecture', models.BooleanField(default=False, verbose_name='sub-prefecture')),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
                ('arrondissement', models.OneToOneField(verbose_name='arrondissement', to='administrative_division.Arrondissement')),
            ],
            options={
                'verbose_name': 'prefecture',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PublicSafetyDirection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
                ('department', models.OneToOneField(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'public safety direction',
                'verbose_name_plural': 'public safety directions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rescuer',
            fields=[
                ('observer_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Observer')),
                ('association', models.ForeignKey(verbose_name='first aid association', to='emergencies.FirstAidAssociation')),
            ],
            options={
                'verbose_name': 'rescuer',
            },
            bases=('administration.observer',),
        ),
        migrations.CreateModel(
            name='RoadSafetySquadron',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('department', models.OneToOneField(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'road safety squadron',
                'verbose_name_plural': 'road safety squadrons',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SDISAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
                ('fire_and_rescue_service', models.ForeignKey(verbose_name='fire and rescue service', to='administration.FireAndRescueService')),
            ],
            options={
                'verbose_name': 'sdis agent',
                'verbose_name_plural': 'sdis agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=5, verbose_name='name', choices=[('cricr', 'cricr'), ('hunt', 'hunt federation'), ('edf', 'edf'), ('ia', 'academic inspection'), ('sncf', 'sncf'), ('dirce', 'dirce'), ('samu', 'samu'), ('ars', 'ars'), ('pnr', 'pnr'), ('oncfs', 'oncfs'), ('onf', 'onf'), ('epl', 'epl'), ('brl', 'brl'), ('rff', 'rff'), ('sem', 'sem'), ('roan', 'roan'), ('crs', 'crs'), ('ddcs', 'ddcs'), ('see', 'ddt see-pncv'), ('sat', 'ddt sat road safety'), ('cr', 'cr dcese')])),
                ('department', models.ForeignKey(verbose_name='department', to='administrative_division.Department')),
            ],
            options={
                'verbose_name': 'service',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ServiceAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
                ('service', models.ForeignKey(verbose_name='service', to='administration.Service')),
            ],
            options={
                'verbose_name': 'service agent',
                'verbose_name_plural': 'service agents',
            },
            bases=('administration.agent',),
        ),
        migrations.CreateModel(
            name='SubAgent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'sub-agent',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CompanyAgent',
            fields=[
                ('subagent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.SubAgent')),
                ('company', models.ForeignKey(verbose_name='company', to='administration.Company')),
            ],
            options={
                'verbose_name': 'company agent',
                'verbose_name_plural': 'company agents',
            },
            bases=('administration.subagent',),
        ),
        migrations.CreateModel(
            name='CommissariatAgent',
            fields=[
                ('subagent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.SubAgent')),
                ('commissariat', models.ForeignKey(verbose_name='commissariat', to='administration.Commissariat')),
            ],
            options={
                'verbose_name': 'commissariat agent',
                'verbose_name_plural': 'commissariat agents',
            },
            bases=('administration.subagent',),
        ),
        migrations.CreateModel(
            name='CGServiceAgent',
            fields=[
                ('subagent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.SubAgent')),
                ('cg_service', models.ForeignKey(verbose_name='CG service', to='administration.CGService')),
            ],
            options={
                'verbose_name': 'cg service agent',
                'verbose_name_plural': 'cg service agents',
            },
            bases=('administration.subagent',),
        ),
        migrations.CreateModel(
            name='CGDAgent',
            fields=[
                ('subagent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.SubAgent')),
                ('cgd', models.ForeignKey(verbose_name='cgd', to='administration.GendarmerieCompany')),
            ],
            options={
                'verbose_name': 'cgd agent',
                'verbose_name_plural': 'cgd agents',
            },
            bases=('administration.subagent',),
        ),
        migrations.CreateModel(
            name='TownHallAgent',
            fields=[
                ('agent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='administration.Agent')),
                ('commune', models.ForeignKey(verbose_name='commune', to='administrative_division.Commune')),
            ],
            options={
                'verbose_name': 'municipal agent',
                'verbose_name_plural': 'municipal agents',
            },
            bases=('administration.agent',),
        ),
        migrations.AddField(
            model_name='subagent',
            name='user',
            field=models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='service',
            unique_together=set([('name', 'department')]),
        ),
        migrations.AddField(
            model_name='observer',
            name='user',
            field=models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='instructor',
            name='prefecture',
            field=models.ForeignKey(verbose_name='prefecture', to='administration.Prefecture'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='instructor',
            name='user',
            field=models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='edsragent',
            name='road_safety_squadron',
            field=models.ForeignKey(verbose_name='road safety squadron', to='administration.RoadSafetySquadron'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ddspagent',
            name='public_safety_direction',
            field=models.ForeignKey(verbose_name='public safety direction', to='administration.PublicSafetyDirection'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='fire_and_rescue_service',
            field=models.ForeignKey(verbose_name='fire and rescue service', to='administration.FireAndRescueService'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='codisagent',
            name='departmental_fire_op_center',
            field=models.ForeignKey(verbose_name='CODIS', to='administration.DepartmentalFireOpCenter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cisagent',
            name='fire_service',
            field=models.ForeignKey(verbose_name='CIS', to='administration.FireService'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cgsuperior',
            name='general_council',
            field=models.ForeignKey(verbose_name='general council', to='administration.GeneralCouncil'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cgservice',
            name='general_council',
            field=models.ForeignKey(verbose_name='general council', to='administration.GeneralCouncil'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cgagent',
            name='general_council',
            field=models.ForeignKey(verbose_name='general council', to='administration.GeneralCouncil'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='brigade',
            name='cgd',
            field=models.ForeignKey(verbose_name='cgd', to='administration.GendarmerieCompany'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='brigade',
            name='commune',
            field=models.ForeignKey(verbose_name='commune', to='administrative_division.Commune'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='agent',
            name='user',
            field=models.OneToOneField(verbose_name='user', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
