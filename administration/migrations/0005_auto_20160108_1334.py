# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0004_auto_20141204_1629'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='brigade',
            options={'verbose_name': 'EDSR - Brigade', 'verbose_name_plural': 'EDSR - Brigades'},
        ),
        migrations.AlterModelOptions(
            name='brigadeagent',
            options={'verbose_name': 'EDSR - Brigade - Agent', 'verbose_name_plural': 'EDSR - Brigade - Agents'},
        ),
        migrations.AlterModelOptions(
            name='cgagent',
            options={'verbose_name': 'CG - Agent', 'verbose_name_plural': 'CG - Agents'},
        ),
        migrations.AlterModelOptions(
            name='cgdagent',
            options={'verbose_name': 'CGD - Agent', 'verbose_name_plural': 'CGD - Agents'},
        ),
        migrations.AlterModelOptions(
            name='cgservice',
            options={'verbose_name': 'CG - Service', 'verbose_name_plural': 'CG - Services'},
        ),
        migrations.AlterModelOptions(
            name='cgserviceagent',
            options={'verbose_name': 'CG - Service - Agent', 'verbose_name_plural': 'CG - Service - Agents'},
        ),
        migrations.AlterModelOptions(
            name='cgsuperior',
            options={'verbose_name': 'CG - Agent n+1', 'verbose_name_plural': 'CG - Agents n+1'},
        ),
        migrations.AlterModelOptions(
            name='cisagent',
            options={'verbose_name': 'SDIS - CIS - Agent', 'verbose_name_plural': 'SDIS - CIS - Agents'},
        ),
        migrations.AlterModelOptions(
            name='codisagent',
            options={'verbose_name': 'SDIS - CODIS - Agent', 'verbose_name_plural': 'SDIS - CODIS - Agents'},
        ),
        migrations.AlterModelOptions(
            name='commissariat',
            options={'verbose_name': 'DDSP - Commissariat', 'verbose_name_plural': 'DDSP - Commissariats'},
        ),
        migrations.AlterModelOptions(
            name='commissariatagent',
            options={'verbose_name': 'DDSP - Commissariat - Agent', 'verbose_name_plural': 'DDSP - Commissariat - Agents'},
        ),
        migrations.AlterModelOptions(
            name='company',
            options={'ordering': ['number'], 'verbose_name': 'SDIS - Compagnie', 'verbose_name_plural': 'SDIS - Compagnies'},
        ),
        migrations.AlterModelOptions(
            name='companyagent',
            options={'verbose_name': 'SDIS - Compagnie - Agent', 'verbose_name_plural': 'SDIS - Compagnie - Agents'},
        ),
        migrations.AlterModelOptions(
            name='ddspagent',
            options={'verbose_name': 'DDSP - Agent', 'verbose_name_plural': 'DDSP - Agents'},
        ),
        migrations.AlterModelOptions(
            name='departmentalfireopcenter',
            options={'verbose_name': 'SDIS - CODIS', 'verbose_name_plural': 'SDIS - CODIS'},
        ),
        migrations.AlterModelOptions(
            name='departmentalgendarmeriegroup',
            options={'verbose_name': 'GGD', 'verbose_name_plural': 'GGD'},
        ),
        migrations.AlterModelOptions(
            name='edsragent',
            options={'verbose_name': 'EDSR - Agent', 'verbose_name_plural': 'EDSR - Agents'},
        ),
        migrations.AlterModelOptions(
            name='federationagent',
            options={'verbose_name': 'Federation - Agent', 'verbose_name_plural': 'Federation - Agents'},
        ),
        migrations.AlterModelOptions(
            name='fireandrescueservice',
            options={'verbose_name': 'SDIS', 'verbose_name_plural': 'SDIS'},
        ),
        migrations.AlterModelOptions(
            name='fireservice',
            options={'verbose_name': 'SDIS - CIS', 'verbose_name_plural': 'SDIS - CIS'},
        ),
        migrations.AlterModelOptions(
            name='gendarmeriecompany',
            options={'verbose_name': 'CGD', 'verbose_name_plural': 'CGD'},
        ),
        migrations.AlterModelOptions(
            name='generalcouncil',
            options={'verbose_name': 'CG', 'verbose_name_plural': 'CG'},
        ),
        migrations.AlterModelOptions(
            name='ggdagent',
            options={'verbose_name': 'GGD - Agent', 'verbose_name_plural': 'GGD - Agents'},
        ),
        migrations.AlterModelOptions(
            name='instructor',
            options={'verbose_name': 'Prefecture - Instructeur', 'verbose_name_plural': 'Prefecture - Instructeurs'},
        ),
        migrations.AlterModelOptions(
            name='prefecture',
            options={'verbose_name': 'Prefecture', 'verbose_name_plural': 'Prefectures'},
        ),
        migrations.AlterModelOptions(
            name='publicsafetydirection',
            options={'verbose_name': 'DDSP', 'verbose_name_plural': 'DDSP'},
        ),
        migrations.AlterModelOptions(
            name='roadsafetysquadron',
            options={'verbose_name': 'EDSR', 'verbose_name_plural': 'EDSR'},
        ),
        migrations.AlterModelOptions(
            name='sdisagent',
            options={'verbose_name': 'SDIS - Agent', 'verbose_name_plural': 'SDIS - Agents'},
        ),
        migrations.AlterModelOptions(
            name='service',
            options={'verbose_name': 'Autre service', 'verbose_name_plural': 'Autres services'},
        ),
        migrations.AlterModelOptions(
            name='serviceagent',
            options={'verbose_name': 'Autre service - Agent', 'verbose_name_plural': 'Autre service - Agents'},
        ),
    ]
