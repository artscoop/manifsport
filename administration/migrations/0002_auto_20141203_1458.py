# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='name',
            field=models.CharField(max_length=5, verbose_name='name', choices=[('cricr', 'cricr'), ('hunt', 'hunt federation'), ('edf', 'edf'), ('ia', 'academic inspection'), ('sncf', 'sncf'), ('dirce', 'dirce'), ('samu', 'samu'), ('ars', 'ars'), ('pnr', 'pnr'), ('oncfs', 'oncfs'), ('onf', 'onf'), ('epl', 'epl'), ('brl', 'brl'), ('rff', 'rff'), ('sem', 'sem'), ('roan', 'roan'), ('crs', 'crs'), ('ddcs', 'ddcs'), ('see', 'ddt see-pncv'), ('sat', 'ddt sat road safety'), ('cr', 'cr dcese'), ('cdsr  ', 'cdsr')]),
            preserve_default=True,
        ),
    ]
