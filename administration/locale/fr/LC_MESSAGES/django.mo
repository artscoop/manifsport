��    R      �  m   <      �     �  
                        !     '     6     I     Q     _     n  	   w     �     �     �     �     �  	   �  
   �  	   �  
   �     �     �               $     8     @  	   R     \     d     r     �  
   �     �  
   �     �     �     �     	  
   	     "	     .	     <	  
   K	     V	     n	     �	     �	     �	     �	     �	     �	     �	  	   
  
   
  
   
     #
     (
     8
     I
     N
  
   W
     b
     p
     �
     �
     �
     �
  
   �
     �
     �
     �
            	        '     6  !   H     j  �  o       
   !     ,     8     <     B     H     W     u     }     �     �  	   �     �     �     �     �     �  	     
     	     
         +     7     D     Q     g     ~     �  
   �  	   �     �     �     �  
   �     �     �  )     *   6      a     �  
   �     �     �     �     �  /   �  1   �     1      Q     r  )   �  (   �     �     �  	     
              ,     1     A     S     W     c     o  3     5   �  
   �  /   �  1   $  
   V     a     m     u     �     �  
   �     �     �  +   �              B                           (       <      )      O          9   I   .   D      *       L   A   N      '                ;   %   E   H   :   4   =   C   1      0             @               $          "           J      ,   M       2   3          7       	   ?   &   /             Q             P          5   6       8      F   !          K       -   +       
             >   R       #   G           Administration CG service CG services CIS CODIS agent arrondissement autonomous brigade brigade brigade agent brigade agents cg agent cg agents cg service agent cg service agents cg superior cg superiors cgd cgd agent cgd agents cis agent cis agents codis agent codis agents commissariat commissariat agent commissariat agents commune community brigade companies company company agent company agents company number ddsp agent ddsp agents department departmental gendarmerie group departmental gendarmerie groups departmental technical service e-mail edsr agent edsr agents federal agent federal agents federation fire and rescue service fire and rescue services fire service fire services first aid association gendarmerie companies gendarmerie company general council general councils ggd agent ggd agents instructor kind municipal agent municipal agents name observer prefecture prefecture of public safety direction public safety directions rescuer road safety squadron road safety squadrons sdis agent sdis agents service service agent service agents service type sub-agent sub-prefecture sub-prefecture of transports and mobility direction user Project-Id-Version: administration
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-01-22 12:31+0100
PO-Revision-Date: 2015-01-22 12:39+0100
Last-Translator: Simon Panay <simon.panay@openscop.fr>
Language-Team: LANGUAGE FR <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.5.4
 Services de l'Administration service CG services CG CIS CODIS agent arrondissement brigade territoriale autonome brigade agent de brigade agents de brigade agent CG agents CG agent de service du CG agents de service du CG agent N+1 CG agents N+1 CG CGD agent CGD agents CGD agent CIS agents CIS agent CODIS agents CODIS commissariat agent de commissariat agents de commissariat commune communauté de brigade compagnies compagnie agent compagnie agents de compagnie numéro compagnie agent DDSP agents DDSP département groupement de gendarmerie départementale groupements de gendarmerie départementale service technique départemental e-mail agent EDSR agents EDSR agent fédéral agents fédéraux fédération service départemental d'incendie et de secours services départementaux d'incendie et de secours centre d'incendie et de secours centres d'incendie et de secours association de premiers secours compagnies de gendarmerie départementale compagnie de gendarmerie départementale conseil général conseils généraux agent GGD agents GGD instructeur type agent municipal agents municipaux nom observateur préfecture préfécture de direction départementale de la sécurité publique directions départementales de la sécurité publique secouriste escadron départemental de sécurité routière escadrons départementaux de sécurité routière agent SDIS agents SDIS service agent de service agents de service type de service sous-agent sous-préfecture sous-préfecture de direction des transports et de la mobilité utilisateur 