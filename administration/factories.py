from __future__ import unicode_literals

import factory

from administrative_division.factories import CommuneFactory
from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import DepartmentFactory
from events.factories import UserFactory
from sports.factories import FederationFactory

from .models import Instructor
from .models import FederationAgent

from .models import Prefecture
from .models import Service
from .models import ServiceAgent
from .models import Commissariat
from .models import GendarmerieCompany
from .models import Company
from .models import CGService
from .models import TownHallAgent
from .models import FireService

from .models import DepartmentalGendarmerieGroup
from .models import FireAndRescueService
from .models import GeneralCouncil
from .models import PublicSafetyDirection
from .models import RoadSafetySquadron


# pylint: disable=W0108
class DepartmentalGendarmerieGroupFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = DepartmentalGendarmerieGroup

    department = factory.SubFactory(DepartmentFactory)


class FireAndRescueServiceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = FireAndRescueService

    department = factory.SubFactory(DepartmentFactory)


class GeneralCouncilFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = GeneralCouncil

    department = factory.SubFactory(DepartmentFactory)


class PublicSafetyDirectionFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = PublicSafetyDirection

    department = factory.SubFactory(DepartmentFactory)


class RoadSafetySquadronFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = RoadSafetySquadron

    department = factory.SubFactory(DepartmentFactory)


class PrefectureFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Prefecture

    sub_prefecture = True
    arrondissement = factory.SubFactory(ArrondissementFactory)
    email = factory.Sequence(lambda n: 'pref{0}@loire.gouv.fr'.format(n))


class InstructorFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Instructor

    user = factory.SubFactory(UserFactory)
    prefecture = factory.SubFactory(PrefectureFactory)


class ServiceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Service

    name = 'sncf'
    department = factory.SubFactory(DepartmentFactory)


class ServiceAgentFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = ServiceAgent

    user = factory.SubFactory(UserFactory)
    service = factory.SubFactory(ServiceFactory)


class TownHallAgentFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = TownHallAgent

    user = factory.SubFactory(UserFactory)
    commune = factory.SubFactory(CommuneFactory)


class FederationAgentFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = FederationAgent

    user = factory.SubFactory(UserFactory)
    federation = factory.SubFactory(FederationFactory)


class CommissariatFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Commissariat

    commune = factory.SubFactory(CommuneFactory)


class GendarmerieCompanyFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = GendarmerieCompany

    arrondissement = factory.SubFactory(ArrondissementFactory)


class CompanyFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Company

    fire_and_rescue_service = factory.SubFactory(FireAndRescueServiceFactory)
    number = factory.Sequence(lambda n: '%d.%d' % (n, n))


class FireServiceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = FireService

    commune = factory.SubFactory(CommuneFactory)
    company = factory.SubFactory(CompanyFactory)


class CGServiceFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = CGService

    general_council = factory.SubFactory(GeneralCouncilFactory)
    name = factory.Sequence(lambda n: 'service_name.%d' % n)
    service_type = factory.Iterator(CGService.SERVICE_TYPE_CHOICES,
                                    getter=lambda c: c[0])
