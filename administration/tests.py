from __future__ import unicode_literals

from django.test import TestCase


from .factories import FireServiceFactory
from .factories import InstructorFactory
from .factories import PrefectureFactory
from .factories import ServiceFactory


class PrefectureMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_sub_prefecture(self):
        '''
        __str__() should return sub-prefecture + name
        '''
        sub_prefecture = PrefectureFactory.build(sub_prefecture=True)
        self.assertEqual(sub_prefecture.__str__(), ''.join([
            'Sub-prefecture of ', sub_prefecture.arrondissement.__str__()
        ]))

    def test_str_prefecture(self):
        '''
        __str__() should return sub-prefecture + name
        '''
        prefecture = PrefectureFactory.build(sub_prefecture=False)
        self.assertEqual(prefecture.__str__(), ''.join([
            'Prefecture of ',
            prefecture.arrondissement.__str__()
        ]))


class InstructorMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the username of the instructor
        '''
        instructor = InstructorFactory.build(user__username='jdupont')
        self.assertEqual(instructor.__str__(), 'jdupont')


class ServiceMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the username of the instructor
        '''
        service = ServiceFactory.create()
        self.assertEqual(
            service.__str__(),
            ' - '.join([service.department.name, service.name])
        )


class FireServiceMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return "CIS name_of_the_commune"
        '''
        fire_service = FireServiceFactory.build()
        self.assertEqual(fire_service.__str__(), ''.join([
            'CIS ',
            fire_service.commune.name
        ]))
