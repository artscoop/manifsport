# coding: utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from localflavor.fr.fr_department import DEPARTMENT_CHOICES


CHOICES = {'arrondissement__department__name': settings.DEPARTMENT_SETTINGS['DEPARTMENT_NUMBER']}


@python_2_unicode_compatible
class Department(models.Model):
    """ Division de niveau 2 (département) : 96 en France métro """
    name = models.CharField(_('department'),
                            unique=True,
                            max_length=3,
                            choices=DEPARTMENT_CHOICES)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('department')
        ordering = ['name']

    def __str__(self):
        return self.get_name_display()  # pylint: disable=E1101


@python_2_unicode_compatible
class Arrondissement(models.Model):
    """ Division de niveau 3 (arrondissement, 330 en France Métropolitaine """
    code = models.CharField(_('code'), unique=True, max_length=4)
    name = models.CharField(_('name'), max_length=255)
    department = models.ForeignKey(Department, verbose_name=_('department'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('arrondissement')
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Commune(models.Model):
    """ Division de niveau 4 (commune) """
    code = models.CharField(_('code'), unique=True, max_length=5)
    name = models.CharField(_('name'), max_length=255)
    ascii_name = models.CharField(_('ascii_name'), max_length=255)
    zip_code = models.CharField(_('zip code'), max_length=5)
    arrondissement = models.ForeignKey(Arrondissement,
                                       verbose_name=_('arrondissement'))
    latitude = models.DecimalField(_('latitude'),
                                   max_digits=8,
                                   decimal_places=5,
                                   null=True, blank=True)
    longitude = models.DecimalField(_('longitude'),
                                    max_digits=8,
                                    decimal_places=5,
                                    null=True, blank=True)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('commune')
        ordering = ['ascii_name']

    def __str__(self):
        return self.name
