from __future__ import unicode_literals

from django.test import TestCase

from .factories import ArrondissementFactory, CommuneFactory
from .factories import DepartmentFactory


class DepartmentMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the department
        '''
        department = DepartmentFactory.build(name='42')
        self.assertEqual(department.__str__(), department.get_name_display())


class ArrondissementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the arrondissement
        '''
        arrondissement = ArrondissementFactory.build(name='Roanne')
        self.assertEqual(arrondissement.__str__(), 'Roanne')


class CommuneMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the commune
        '''
        commune = CommuneFactory.build(name='Roanne')
        self.assertEqual(commune.__str__(), 'Roanne')
