# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Arrondissement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=4, verbose_name='code')),
                ('name', models.CharField(max_length=255, verbose_name='name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'arrondissement',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Commune',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=5, verbose_name='code')),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('ascii_name', models.CharField(max_length=255, verbose_name='ascii_name')),
                ('zip_code', models.CharField(max_length=5, verbose_name='zip code')),
                ('latitude', models.DecimalField(null=True, verbose_name='latitude', max_digits=8, decimal_places=5, blank=True)),
                ('longitude', models.DecimalField(null=True, verbose_name='longitude', max_digits=8, decimal_places=5, blank=True)),
                ('arrondissement', models.ForeignKey(verbose_name='arrondissement', to='administrative_division.Arrondissement')),
            ],
            options={
                'ordering': ['ascii_name'],
                'verbose_name': 'commune',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=3, verbose_name='department', choices=[('01', '01 - Ain'), ('02', '02 - Aisne'), ('03', '03 - Allier'), ('04', '04 - Alpes-de-Haute-Provence'), ('05', '05 - Hautes-Alpes'), ('06', '06 - Alpes-Maritimes'), ('07', '07 - Ard\xe8che'), ('08', '08 - Ardennes'), ('09', '09 - Ari\xe8ge'), ('10', '10 - Aube'), ('11', '11 - Aude'), ('12', '12 - Aveyron'), ('13', '13 - Bouches-du-Rh\xf4ne'), ('14', '14 - Calvados'), ('15', '15 - Cantal'), ('16', '16 - Charente'), ('17', '17 - Charente-Maritime'), ('18', '18 - Cher'), ('19', '19 - Corr\xe8ze'), ('2A', '2A - Corse-du-Sud'), ('2B', '2B - Haute-Corse'), ('21', "21 - C\xf4te-d'Or"), ('22', "22 - C\xf4tes-d'Armor"), ('23', '23 - Creuse'), ('24', '24 - Dordogne'), ('25', '25 - Doubs'), ('26', '26 - Dr\xf4me'), ('27', '27 - Eure'), ('28', '28 - Eure-et-Loir'), ('29', '29 - Finist\xe8re'), ('30', '30 - Gard'), ('31', '31 - Haute-Garonne'), ('32', '32 - Gers'), ('33', '33 - Gironde'), ('34', '34 - H\xe9rault'), ('35', '35 - Ille-et-Vilaine'), ('36', '36 - Indre'), ('37', '37 - Indre-et-Loire'), ('38', '38 - Is\xe8re'), ('39', '39 - Jura'), ('40', '40 - Landes'), ('41', '41 - Loir-et-Cher'), ('42', '42 - Loire'), ('43', '43 - Haute-Loire'), ('44', '44 - Loire-Atlantique'), ('45', '45 - Loiret'), ('46', '46 - Lot'), ('47', '47 - Lot-et-Garonne'), ('48', '48 - Loz\xe8re'), ('49', '49 - Maine-et-Loire'), ('50', '50 - Manche'), ('51', '51 - Marne'), ('52', '52 - Haute-Marne'), ('53', '53 - Mayenne'), ('54', '54 - Meurthe-et-Moselle'), ('55', '55 - Meuse'), ('56', '56 - Morbihan'), ('57', '57 - Moselle'), ('58', '58 - Ni\xe8vre'), ('59', '59 - Nord'), ('60', '60 - Oise'), ('61', '61 - Orne'), ('62', '62 - Pas-de-Calais'), ('63', '63 - Puy-de-D\xf4me'), ('64', '64 - Pyr\xe9n\xe9es-Atlantiques'), ('65', '65 - Hautes-Pyr\xe9n\xe9es'), ('66', '66 - Pyr\xe9n\xe9es-Orientales'), ('67', '67 - Bas-Rhin'), ('68', '68 - Haut-Rhin'), ('69', '69 - Rh\xf4ne'), ('70', '70 - Haute-Sa\xf4ne'), ('71', '71 - Sa\xf4ne-et-Loire'), ('72', '72 - Sarthe'), ('73', '73 - Savoie'), ('74', '74 - Haute-Savoie'), ('75', '75 - Paris'), ('76', '76 - Seine-Maritime'), ('77', '77 - Seine-et-Marne'), ('78', '78 - Yvelines'), ('79', '79 - Deux-S\xe8vres'), ('80', '80 - Somme'), ('81', '81 - Tarn'), ('82', '82 - Tarn-et-Garonne'), ('83', '83 - Var'), ('84', '84 - Vaucluse'), ('85', '85 - Vend\xe9e'), ('86', '86 - Vienne'), ('87', '87 - Haute-Vienne'), ('88', '88 - Vosges'), ('89', '89 - Yonne'), ('90', '90 - Territoire de Belfort'), ('91', '91 - Essonne'), ('92', '92 - Hauts-de-Seine'), ('93', '93 - Seine-Saint-Denis'), ('94', '94 - Val-de-Marne'), ('95', "95 - Val-d'Oise"), ('971', '971 - Guadeloupe'), ('972', '972 - Martinique'), ('973', '973 - Guyane'), ('974', '974 - La R\xe9union'), ('975', '975 - Saint-Pierre-et-Miquelon'), ('976', '976 - Mayotte'), ('977', '977 - Saint-Barth\xe9lemy'), ('978', '978 - Saint-Martin'), ('984', '984 - Terres australes et antarctiques fran\xe7aises'), ('986', '986 - Wallis et Futuna'), ('987', '987 - Polyn\xe9sie fran\xe7aise'), ('988', '988 - Nouvelle-Cal\xe9donie'), ('989', '989 - \xcele de Clipperton')])),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'department',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='arrondissement',
            name='department',
            field=models.ForeignKey(verbose_name='department', to='administrative_division.Department'),
            preserve_default=True,
        ),
    ]
