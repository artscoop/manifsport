'''
These forms are normally used by Commune and Department models
in the admin interface.
But for now, there are disabled in order to limit the number of
models displayed
'''
from __future__ import unicode_literals

from django import forms

from localflavor.fr.forms import FRZipCodeField, FRDepartmentField

from administrative_division.models import Commune, Department


class CommuneForm(forms.ModelForm):
    zip_code = FRZipCodeField()

    class Meta:  # pylint: disable=C1001
        model = Commune
        fields = '__all__'


class DepartmentForm(forms.ModelForm):
    name = FRDepartmentField()

    class Meta:  # pylint: disable=C1001
        model = Department
        fields = '__all__'
