from __future__ import unicode_literals

from django.contrib import admin

from .forms import CommuneForm, DepartmentForm

from .models import Arrondissement, Commune, Department


class ArrondissementAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    list_filter = ['department__name', ]
    search_fields = ['name', 'department__name']


class CommuneAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    form = CommuneForm
    list_display = ['name', 'zip_code', 'arrondissement']
    search_fields = ['name', 'zip_code', 'ascii_name']


class DepartmentAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    form = DepartmentForm
    search_fields = ['name']


admin.site.register(Department, DepartmentAdmin)
admin.site.register(Arrondissement, ArrondissementAdmin)
admin.site.register(Commune, CommuneAdmin)
