from __future__ import unicode_literals

import factory

from factory.fuzzy import FuzzyDecimal

from .models import Arrondissement
from .models import Commune
from .models import Department


# pylint: disable=W0108
class DepartmentFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Department

    name = factory.Sequence(lambda n: 'Department{0}'.format(n))


class ArrondissementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Arrondissement

    code = factory.Sequence(lambda n: '{0}{0}{0}'.format(n))
    name = factory.Sequence(lambda n: 'Arrondissement{0}'.format(n))
    department = factory.SubFactory(DepartmentFactory)


class CommuneFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Commune

    name = factory.Sequence(lambda n: 'Commune{0}'.format(n))
    ascii_name = factory.Sequence(lambda n: 'Commune{0}'.format(n))
    code = factory.Sequence(lambda n: '{0}{0}{0}{0}'.format(n))
    zip_code = '42300'
    arrondissement = factory.SubFactory(ArrondissementFactory)
    latitude = FuzzyDecimal(180.0)
    longitude = FuzzyDecimal(180.0)
