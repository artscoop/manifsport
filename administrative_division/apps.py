from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AdministrativeDivisionConfig(AppConfig):
    name = 'administrative_division'
    verbose_name = _('Administrative Division')
