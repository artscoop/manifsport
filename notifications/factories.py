from __future__ import unicode_literals

import factory

from events.factories import UserFactory
from events.factories import MotorizedRaceFactory

from .models import Action
from .models import Notification


class ActionFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Action

    user = factory.SubFactory(UserFactory)
    event = factory.SubFactory(MotorizedRaceFactory)


class NotificationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Notification

    user = factory.SubFactory(UserFactory)
    event = factory.SubFactory(MotorizedRaceFactory)
