from __future__ import unicode_literals

import datetime

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from events.models import Event


class LogQuerySet(models.QuerySet):
    def in_processing(self):
        return self.filter(event__end_date__gte=datetime.date.today() - datetime.timedelta(days=1))

    def last_first(self):
        return self.order_by('-creation_date')


@python_2_unicode_compatible
class Action(models.Model):
    creation_date = models.DateTimeField(_('date'), auto_now_add=True)
    action = models.CharField(_('action'), max_length=255)
    event = models.ForeignKey(Event, verbose_name=_('event'))
    user = models.ForeignKey(User, verbose_name=_('user'))
    objects = LogQuerySet.as_manager()

    class Meta:  # pylint: disable=C1001
        verbose_name = _('action')

    def __str__(self):
        return ' - '.join([self.user.username, self.event.name, self.action])


@python_2_unicode_compatible
class Notification(models.Model):
    creation_date = models.DateTimeField(_('date'), auto_now_add=True)
    subject = models.CharField(_('subject'), max_length=255)
    event = models.ForeignKey(Event, verbose_name=_('event'))
    user = models.ForeignKey(User, verbose_name=_('user'))
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    read = models.BooleanField(_('read'), default=False)
    objects = LogQuerySet.as_manager()

    class Meta:  # pylint: disable=C1001
        verbose_name = _('notification')

    def __str__(self):
        return ' - '.join([self.user.username, self.event.name, self.subject])
