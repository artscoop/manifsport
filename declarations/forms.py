from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Submit, Layout, Fieldset
from crispy_forms.bootstrap import FormActions

from .models import EventDeclaration


class DeclarationForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = EventDeclaration
        fields = ()

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(DeclarationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            FormActions(
                Submit('save', _('submit declaration').capitalize())
            )
        )


class DeclarationPublishReceiptForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = EventDeclaration
        fields = ('receipt', )

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(DeclarationPublishReceiptForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                _('choose the receipt file').capitalize(),
                'receipt',
            ),
            FormActions(
                Submit('save', _('publish receipt').capitalize())
            )
        )

    def clean_receipt(self):
        data = self.cleaned_data['receipt']
        if not data:
            raise forms.ValidationError(_('You must specify a receipt file'))
        return data
