from __future__ import unicode_literals

from django.contrib import admin

from .models import EventDeclaration


class EventDeclarationInline(admin.StackedInline):
    model = EventDeclaration
    extra = 0
    max_num = 1
    readonly_fields = ('creation_date',)
