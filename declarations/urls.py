from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import DeclarationCreateView
from .views import DeclarationDetailView
from .views import DeclarationPublishReceiptView

urlpatterns = patterns(
    # pylint: disable=E1120
    '',
    url(r'^(?P<pk>\d+)/publish/$', DeclarationPublishReceiptView.as_view(), name='declaration_publish'),
    url(r'^add/(?P<event_pk>\d+)$', DeclarationCreateView.as_view(), name='declaration_add'),
    url(r'^(?P<pk>\d+)/$', DeclarationDetailView.as_view(), name='declaration_detail'),
    # pylint: enable=E1120
)
