from __future__ import unicode_literals

import factory

from events.factories import DeclaredNonMotorizedEventFactory

from .models import EventDeclaration


class EventDeclarationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = EventDeclaration

    event = factory.SubFactory(DeclaredNonMotorizedEventFactory)
