��          �   %   �      @     A     N     n     �     �     �     �     �     �     �               &     8     K  5   X     �     �     �     �     �     �     �       �       �  8   �     �  *        7     I     V     v     �     �     �     �     �  '        )  L   E     �  '   �     �  $   �  +        B     Z     w     	                                                                                        
                                 Declarations You must specify a receipt file actions available choose the receipt file creation date declaration declaration dispatch date declaration received declaration sent declaration state dispatch date event creation date event declaration event declarations event detail find here all actions available related to this event go back to the dashboard publish receipt receipt receipt published receipt publishing related event request more information submit declaration Project-Id-Version: declarations
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-12 00:18+0100
PO-Revision-Date: 2015-11-12 00:20+0100
Last-Translator: Simon Panay <simon.panay@openscop.fr>
Language-Team: LANGUAGE FR <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.4
 Déclarations Vous devez sélectionner un récépissé de déclaration actions disponibles sélection du récépissé de déclaration date de création déclaration date d'envoi de la déclaration déclaration reçue déclaration envoyée état de la déclaration date d'envoi date de création du dossier déclaration de manifestation déclarations de manifestation sportive détail de la manifestation veuillez trouver ici toutes les actions disponibles pour cette manifestation retour au tableau de bord publier le récépissé de déclaration récépissé de déclaration récépissé de déclaration publié publication du récépissé de déclaration manifestation associée demander plus d'informations soumettre la déclaration 