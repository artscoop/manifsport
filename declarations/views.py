from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView

from authorizations.decorators import instructor_required

from events.decorators import promoter_required
from events.models import Event

from .forms import DeclarationPublishReceiptForm
from .forms import DeclarationForm

from .models import EventDeclaration


class DeclarationCreateView(CreateView):  # pylint: disable=R0901
    model = EventDeclaration
    form_class = DeclarationForm

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(DeclarationCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.event = get_object_or_404(Event,
                                                pk=self.kwargs['event_pk'])
        return super(DeclarationCreateView, self).form_valid(form)

    def get_success_url(self):
        try:
            self.object.event.declarednonmotorizedevent
        except:
            return reverse('events:motorizedconcentration_detail', kwargs={'pk': self.object.event.pk})
        else:
            return reverse('events:declarednonmotorizedevent_detail', kwargs={'pk': self.object.event.pk})


class DeclarationDetailView(DetailView):  # pylint: disable=R0901
    model = EventDeclaration

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(DeclarationDetailView, self).dispatch(*args, **kwargs)


class DeclarationPublishReceiptView(UpdateView):  # pylint: disable=R0901
    model = EventDeclaration
    form_class = DeclarationPublishReceiptForm
    template_name_suffix = '_publish_form'

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(DeclarationPublishReceiptView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.publish()
        return super(DeclarationPublishReceiptView, self).form_valid(form)
