from __future__ import unicode_literals

from django.test import TestCase

from notifications.models import Action

from .factories import EventDeclarationFactory


class EventDeclarationMethodTests(TestCase):

    def test_log_declaration_creation(self):
        declaration = EventDeclarationFactory.create()
        action = Action.objects.last()
        self.assertEqual(action.user, declaration.event.structure.promoter.user)
        self.assertEqual(action.event, declaration.event.event_ptr)
        self.assertEqual(action.action, "declaration sent")
        # when declaration is only updated,
        # no log action is created
        declaration.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)
