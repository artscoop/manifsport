from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DeclarationsConfig(AppConfig):
    name = 'declarations'
    verbose_name = _('Declarations')
