# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventDeclaration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateField(auto_now_add=True, verbose_name='creation date')),
                ('dispatch_date', models.DateField(null=True, verbose_name='dispatch date', blank=True)),
                ('receipt', models.FileField(upload_to='recepisses/%Y/%m/%d', null=True, verbose_name='receipt', blank=True)),
                ('state', django_fsm.FSMField(default='created', max_length=50, verbose_name='declaration state')),
            ],
            options={
                'verbose_name': 'event declaration',
                'verbose_name_plural': 'event declarations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MotorizedConcentrationDeclaration',
            fields=[
                ('eventdeclaration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='declarations.EventDeclaration')),
                ('related_event', models.OneToOneField(verbose_name='related event', to='events.MotorizedConcentration')),
            ],
            options={
                'verbose_name': 'motorized concentration declaration',
                'verbose_name_plural': 'motorized concentration declarations',
            },
            bases=('declarations.eventdeclaration',),
        ),
        migrations.CreateModel(
            name='NonMotorizedEventDeclaration',
            fields=[
                ('eventdeclaration_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='declarations.EventDeclaration')),
                ('related_event', models.OneToOneField(verbose_name='related event', to='events.DeclaredNonMotorizedEvent')),
            ],
            options={
                'verbose_name': 'non motorized event declaration',
                'verbose_name_plural': 'non motorized event declarations',
            },
            bases=('declarations.eventdeclaration',),
        ),
    ]
