# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def move_event_relation_to_declaration(apps, schema_editor):
    NonMotorizedEventDeclaration = apps.get_model('declarations', 'NonMotorizedEventDeclaration')
    MotorizedConcentrationDeclaration = apps.get_model('declarations', 'MotorizedConcentrationDeclaration')
    for declaration in NonMotorizedEventDeclaration.objects.all():
        declaration.event = declaration.related_event
        declaration.save()
    for declaration in MotorizedConcentrationDeclaration.objects.all():
        declaration.event = declaration.related_event
        declaration.save()


def move_event_relation_to_declaration_subclass(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('declarations', '0002_eventdeclaration_event'),
    ]

    operations = [
        migrations.RunPython(move_event_relation_to_declaration, move_event_relation_to_declaration_subclass)
    ]
