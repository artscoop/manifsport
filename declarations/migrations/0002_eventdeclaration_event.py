# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20141204_1258'),
        ('declarations', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventdeclaration',
            name='event',
            field=models.OneToOneField(null=True, blank=True, to='events.Event', verbose_name='related event'),
            preserve_default=True,
        ),
    ]
