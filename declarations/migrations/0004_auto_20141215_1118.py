# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('declarations', '0003_auto_20141215_1056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventdeclaration',
            name='event',
            field=models.OneToOneField(verbose_name='related event', to='events.Event'),
            preserve_default=True,
        ),
    ]
