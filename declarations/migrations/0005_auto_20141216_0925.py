# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('declarations', '0004_auto_20141215_1118'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='motorizedconcentrationdeclaration',
            name='eventdeclaration_ptr',
        ),
        migrations.RemoveField(
            model_name='motorizedconcentrationdeclaration',
            name='related_event',
        ),
        migrations.DeleteModel(
            name='MotorizedConcentrationDeclaration',
        ),
        migrations.RemoveField(
            model_name='nonmotorizedeventdeclaration',
            name='eventdeclaration_ptr',
        ),
        migrations.RemoveField(
            model_name='nonmotorizedeventdeclaration',
            name='related_event',
        ),
        migrations.DeleteModel(
            name='NonMotorizedEventDeclaration',
        ),
    ]
