from __future__ import unicode_literals

from django.core.urlresolvers import reverse

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from django_fsm import FSMField, transition

from administration.models import Prefecture

from events.models import Event

from ddcs_loire.utils import add_notification_entry
from ddcs_loire.utils import add_log_entry


class EventDeclarationQuerySet(models.QuerySet):
    def for_instructor(self, instructor):
        return self.filter(event__departure_city__arrondissement=instructor.prefecture.arrondissement)

    def to_process(self):
        return self.filter(event__end_date__gt=timezone.now())

    def closest_first(self):
        return self.order_by('event__begin_date')


class EventDeclaration(models.Model):
    creation_date = models.DateField(_('creation date'), auto_now_add=True)
    dispatch_date = models.DateField(_('dispatch date'), blank=True, null=True)
    event = models.OneToOneField(Event, verbose_name=_('related event'))
    receipt = models.FileField(
        upload_to='recepisses/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('receipt'),
        max_length=100,
    )
    state = FSMField(default='created', verbose_name=_('declaration state'))
    objects = EventDeclarationQuerySet.as_manager()

    class Meta:  # pylint: disable=C1001
        verbose_name = _('event declaration')
        verbose_name_plural = _('event declarations')

    def get_absolute_url(self):
        url = 'authorizations:dashboard'
        return reverse(url)

    def get_concerned_prefecture(self):
        return self.event.departure_city.arrondissement.prefecture

    def log_creation(self):
        add_log_entry(agents=[self.event.structure.promoter],
                      action=_('declaration sent'),
                      event=self.event)

    def log_publish(self, prefecture):
        add_log_entry(agents=prefecture.instructor_set.all(),
                      action=_('receipt published'),
                      event=self.event)

    def notify_creation(self):
        try:
            prefecture = self.get_concerned_prefecture()
        except Prefecture.DoesNotExist:
            pass
        else:
            add_notification_entry(
                event=self.event,
                agents=prefecture.instructor_set.all(),
                subject=_('declaration received'),
                content_object=self.event.structure,
                institutional=prefecture,
            )

    def notify_publish(self, prefecture):
        add_notification_entry(
            agents=[self.event.structure.promoter],
            event=self.event,
            subject=_('receipt published'),
            content_object=prefecture,
        )

    @transition(field=state, source='created', target='published')
    def publish(self):
        try:
            prefecture = self.get_concerned_prefecture()
        except Prefecture.DoesNotExist:
            pass
        else:
            self.notify_publish(prefecture)
            self.log_publish(prefecture)


@receiver(post_save, sender=EventDeclaration)
def log_declaration_creation(created, instance, **kwargs):
    if kwargs['raw']:
        return
    if created:
        instance.log_creation()
        instance.notify_creation()
