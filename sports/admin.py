from __future__ import unicode_literals

from django.contrib import admin

from .models import Activity
from .models import Federation
from .models import MultiSportFederation
from .models import Sport


admin.site.register(Federation)
admin.site.register(MultiSportFederation)
admin.site.register(Sport)
admin.site.register(Activity)
