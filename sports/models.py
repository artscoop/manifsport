from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Federation(models.Model):
    short_name = models.CharField(_('short name'), max_length=200,
                                  unique=True)
    name = models.CharField(_('name'), max_length=200, unique=True)
    email = models.EmailField(_('e-mail'), max_length=200, unique=True)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('federation')

    def __str__(self):
        return self.short_name


class MultiSportFederation(Federation):

    class Meta:  # pylint: disable=C1001
        verbose_name = _('multi-sports federation')
        verbose_name_plural = _('multi-sports federations')


@python_2_unicode_compatible
class Sport(models.Model):
    name = models.CharField(_('name'), max_length=200, unique=True)
    federation = models.OneToOneField(Federation,
                                      verbose_name=_('federation'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('sport')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Activity(models.Model):
    name = models.CharField(_('name'), max_length=200, unique=True)
    sport = models.ForeignKey(Sport,
                              verbose_name=_('sport'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('activity')
        verbose_name_plural = _('activities')

    def __str__(self):
        return self.name
