from __future__ import unicode_literals

import factory

from .models import Federation, Sport, Activity, MultiSportFederation


# pylint: disable=W0108
class FederationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Federation

    name = factory.Sequence(lambda n: 'Federation{0}'.format(n))
    short_name = factory.Sequence(lambda n: 'F{0}'.format(n))
    email = factory.Sequence(lambda n: 'fede{0}@federation.fr'.format(n))


class MultiSportFederationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = MultiSportFederation

    name = factory.Sequence(lambda n: 'MultiSport Federation{0}'.format(n))
    short_name = factory.Sequence(lambda n: 'MSF{0}'.format(n))
    email = factory.Sequence(lambda n: 'multifede{0}@federation.fr'.format(n))


class SportFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Sport

    name = factory.Sequence(lambda n: 'Sport{0}'.format(n))
    federation = factory.SubFactory(FederationFactory)


class ActivityFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Activity

    name = factory.Sequence(lambda n: 'Activity{0}'.format(n))
    sport = factory.SubFactory(SportFactory)
