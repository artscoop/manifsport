from __future__ import unicode_literals

from django.test import TestCase

from .factories import FederationFactory, SportFactory, ActivityFactory
from .factories import MultiSportFederationFactory


class FederationMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the short name of the federation
        '''
        federation = FederationFactory.build()
        self.assertEqual(federation.__str__(), federation.short_name)


class MultiSportFederationMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the short name of the federation
        '''
        federation = MultiSportFederationFactory.build()
        self.assertEqual(federation.__str__(), federation.short_name)


class SportMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the sort
        '''
        sport = SportFactory.build(name='Athletisme')
        self.assertEqual(sport.__str__(), 'Athletisme')


class ActivityMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the activity
        '''
        activity = ActivityFactory.build(name='Cross')
        self.assertEqual(activity.__str__(), 'Cross')
