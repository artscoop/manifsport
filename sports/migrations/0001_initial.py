# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=200, verbose_name='name')),
            ],
            options={
                'verbose_name': 'activity',
                'verbose_name_plural': 'activities',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Federation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_name', models.CharField(unique=True, max_length=200, verbose_name='short name')),
                ('name', models.CharField(unique=True, max_length=200, verbose_name='name')),
                ('email', models.EmailField(unique=True, max_length=200, verbose_name='e-mail')),
            ],
            options={
                'verbose_name': 'federation',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MultiSportFederation',
            fields=[
                ('federation_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sports.Federation')),
            ],
            options={
                'verbose_name': 'multi-sports federation',
                'verbose_name_plural': 'multi-sports federations',
            },
            bases=('sports.federation',),
        ),
        migrations.CreateModel(
            name='Sport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=200, verbose_name='name')),
                ('federation', models.OneToOneField(verbose_name='federation', to='sports.Federation')),
            ],
            options={
                'verbose_name': 'sport',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='activity',
            name='sport',
            field=models.ForeignKey(verbose_name='sport', to='sports.Sport'),
            preserve_default=True,
        ),
    ]
