from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SportsConfig(AppConfig):
    name = 'sports'
    verbose_name = _('Sports')
