from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.encoding import python_2_unicode_compatible, smart_text
from django.utils.translation import ugettext_lazy as _

from protected_areas.models import Natura2000Site, RegionalNaturalReserve

from events.models import Event


class EventEvaluation(models.Model):
    COST_CHOICES = (
        ('0', _('less than 5000 euros')),
        ('1', _('between 5000 and 20000 euros')),
        ('2', _('between 20000 and 100000 euros')),
        ('3', _('more than 100000 euros')),
    )
    IMPACT_CHOICES = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )

    content_type = models.ForeignKey(ContentType, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    event = models.OneToOneField(Event, verbose_name=_('event'))
    # Description
    place = models.CharField(_('place'), max_length=255, blank=True)
    # Frequency
    every_year = models.BooleanField(_('event takes place every year'),
                                     default=False)
    first_edition = models.BooleanField(_('first edition of the event'),
                                        default=False)
    other_frequency = models.CharField(_('other frequency'),
                                       max_length=255, blank=True)
    # Budget
    cost = models.CharField(_('cost'), max_length=1, choices=COST_CHOICES)
    # Localization
    track_total_length = models.PositiveIntegerField(_('track total length'))
    # Related facilities
    night_lighting = models.BooleanField(_('night lighting'), default=False)
    night_lighting_desc = models.TextField(_('description'), blank=True)
    trails_marks = models.BooleanField(
        _('trails marks'),
        default=False,
        help_text=_('trails marks help text'),
    )
    trails_marks_desc = models.TextField(_('description'), blank=True)
    other_facilities = models.BooleanField(_('other facilities'),
                                           default=False)
    other_facilities_desc = models.TextField(_('description'), blank=True)
    # Potential implications
    noise_emissions = models.BooleanField(
        _('noise emissions'),
        default=False,
        help_text=_('noise emissions help text'),
    )
    waste_management = models.BooleanField(
        _('waste management'),
        default=False,
        help_text=_('waste management help text'),
    )
    waste_management_desc = models.TextField(_('description'), blank=True)
    awareness = models.BooleanField(
        _('awareness'),
        default=False,
        help_text=_('awareness help text'),
    )
    impact_reduction = models.BooleanField(
        _('impact_reduction'),
        default=False,
        help_text=_('impact_reduction help text'),
    )
    impact_reduction_desc = models.TextField(_('description'), blank=True)
    # Conclusions
    estimated_impact = models.CharField(
        _('estimated impact'),
        max_length=1,
        choices=IMPACT_CHOICES,
        help_text=_('estimated impact help text'),
    )

    class Meta:  # pylint: disable=C1001
        abstract = True
        verbose_name = _('event evaluation')
        verbose_name_plural = _('event evaluations')

    def get_absolute_url(self):
        return Event.objects.get_subclass(id=self.event.id).get_absolute_url()


@python_2_unicode_compatible
class RNREvaluation(EventEvaluation):
    DURATION_CHOICES = (
        ('1', _('request of authorization for 1 year')),
        ('3', _('request of authorization for 3 years')),
    )
    # Frequency
    request_duration = models.CharField(_('request duration'), max_length=1,
                                        choices=DURATION_CHOICES)
    # Localization
    sites = models.ManyToManyField(RegionalNaturalReserve,
                                   verbose_name=_('in rnr'))
    # Attendance
    rnr_entries = models.PositiveIntegerField(
        _('entries concerned by the RNR'),
    )
    rnr_audience = models.PositiveIntegerField(
        _('audience concerned by the RNR'),
    )
    # Provisions taken
    administrator_contact_date = models.DateField(
        _('administrator contact date'),
    )
    sensitization = models.CharField(_('sensitization'), max_length=255)
    forbidden_areas = models.CharField(_('forbidden areas'), max_length=255)
    administrator_attendance = models.BooleanField(
        _('administrator attendance desired'),
        default=False,
    )
    # Potential implications
    plane_attendance = models.BooleanField(_('plane attendance'),
                                           default=False)
    plane_attendance_desc = models.TextField(
        _('plane attendance management'),
        blank=True,
    )
    trampling = models.BooleanField(_('trampling'), default=False)
    trampling_desc = models.TextField(_('trampling management'), blank=True)
    markup = models.BooleanField(_('markup'), default=False)
    markup_desc = models.TextField(_('markup management'), blank=True)
    # Conclusion
    regulatory_compliance = models.BooleanField(
        _('regulatory compliance'),
        default=False,
        help_text=_('see above'),
    )
    rnr_conclusion = models.BooleanField(_('rnr conclusion'), default=False)

    class Meta(EventEvaluation.Meta):  # pylint: disable=C1001
        verbose_name = _('RNR evaluation')
        verbose_name_plural = _('RNR evaluations')

    def __str__(self):
        return smart_text(self.event.__str__())


@python_2_unicode_compatible
class Natura2000Evaluation(EventEvaluation):
    # Description
    diurnal = models.BooleanField(_('diurnal'), default=False)
    nocturnal = models.BooleanField(_('nocturnal'), default=False)
    # Localization
    sites = models.ManyToManyField(Natura2000Site,
                                   verbose_name=_('in natura 2000 site'))
    range_from_site = models.PositiveIntegerField(_('range from site'))
    # Area of influence
    rnn = models.BooleanField(_('rnn'), default=False)
    rnr = models.BooleanField(_('rnr'), default=False)
    biotope_area = models.BooleanField(_('biotope area'), default=False)
    classified_site = models.BooleanField(_('classified site'), default=False)
    registered_site = models.BooleanField(_('registered site'), default=False)
    pnr = models.BooleanField(_('pnr'), default=False)
    znieff = models.BooleanField(_('znieff'), default=False)

    # Related facilities
    parking_lot = models.BooleanField(_('parking lot'), default=False)
    parking_lot_desc = models.TextField(_('description'), blank=True)
    reception_area = models.BooleanField(_('reception area'), default=False)
    reception_area_desc = models.TextField(_('description'), blank=True)
    supplying_area = models.BooleanField(_('supplying area'), default=False)
    supplying_area_desc = models.TextField(_('description'), blank=True)
    storage_area = models.BooleanField(_('storage area'), default=False)
    storage_area_desc = models.TextField(_('description'), blank=True)
    awards_stage = models.BooleanField(_('awards stage'), default=False)
    awards_stage_desc = models.TextField(_('description'), blank=True)
    bivouac_area = models.BooleanField(_('bivouac area'), default=False)
    bivouac_area_desc = models.TextField(_('description'), blank=True)
    noise = models.BooleanField(_('noise'), default=False)
    noise_desc = models.TextField(_('description'), blank=True)
    # Impacts on natural habitats
    lawn = models.BooleanField(_('lawn'), default=False)
    lawn_comments = models.CharField(_('comments'), blank=True,
                                     max_length=255)
    semi_wooded_lawn = models.BooleanField(_('semi-wooded lawn'),
                                           default=False)
    semi_wooded_lawn_comments = models.CharField(_('comments'), blank=True,
                                                 max_length=255)
    moor = models.BooleanField(_('moor'), default=False)
    moor_comments = models.CharField(_('comments'), blank=True,
                                     max_length=255)
    scrubland_maquis = models.BooleanField(_('scrubland/maquis'),
                                           default=False)
    scrubland_maquis_comments = models.CharField(_('comments'), blank=True,
                                                 max_length=255)

    coniferous_forest = models.BooleanField(_('coniferous forest'),
                                            default=False)
    coniferous_forest_comments = models.CharField(_('comments'), blank=True,
                                                  max_length=255)
    deciduous_forest = models.BooleanField(_('deciduous forest'),
                                           default=False)
    deciduous_forest_comments = models.CharField(_('comments'), blank=True,
                                                 max_length=255)
    mixed_forest = models.BooleanField(_('mixed forest'), default=False)
    mixed_forest_comments = models.CharField(_('comments'), blank=True,
                                             max_length=255)
    plantation = models.BooleanField(_('plantation'), default=False)
    plantation_comments = models.CharField(_('comments'), blank=True,
                                           max_length=255)

    cliff = models.BooleanField(_('cliff'), default=False)
    cliff_comments = models.CharField(_('comments'), blank=True,
                                      max_length=255)
    outcrop = models.BooleanField(_('outcrop'), default=False)
    outcrop_comments = models.CharField(_('comments'), blank=True,
                                        max_length=255)
    scree = models.BooleanField(_('scree'), default=False)
    scree_comments = models.CharField(_('comments'), blank=True,
                                      max_length=255)
    blocks = models.BooleanField(_('blocks'), default=False)
    blocks_comments = models.CharField(_('comments'), blank=True,
                                       max_length=255)

    ditch = models.BooleanField(_('ditch'), default=False)
    ditch_comments = models.CharField(_('comments'), blank=True,
                                      max_length=255)
    watercourse = models.BooleanField(_('watercourse'), default=False)
    watercourse_comments = models.CharField(_('comments'), blank=True,
                                            max_length=255)
    pound = models.BooleanField(_('pound'), default=False)
    pound_comments = models.CharField(_('comments'), blank=True,
                                      max_length=255)
    bog = models.BooleanField(_('bog'), default=False)
    bog_comments = models.CharField(_('comments'), blank=True,
                                    max_length=255)
    gravel = models.BooleanField(_('gravel'), default=False)
    gravel_comments = models.CharField(_('comments'), blank=True,
                                       max_length=255)
    wet_meadow = models.BooleanField(_('wet meadow'), default=False)
    wet_meadow_comments = models.CharField(_('comments'), blank=True,
                                           max_length=255)

    # Impacts on species
    amphibia = models.BooleanField(_('amphibia'), default=False)
    amphibia_species = models.CharField(_('species'), blank=True,
                                        max_length=255)
    reptiles = models.BooleanField(_('reptiles'), default=False)
    reptiles_species = models.CharField(_('species'), blank=True,
                                        max_length=255)
    crustaceans = models.BooleanField(_('crustaceans'), default=False)
    crustaceans_species = models.CharField(_('species'), blank=True,
                                           max_length=255)
    insects = models.BooleanField(_('insects'), default=False)
    insects_species = models.CharField(_('species'), blank=True,
                                       max_length=255)
    terrestrial_mammals = models.BooleanField(_('terrestrial mammals'),
                                              default=False)
    terrestrial_mammals_species = models.CharField(_('species'), blank=True,
                                                   max_length=255)
    birds = models.BooleanField(_('birds'), default=False)
    birds_species = models.CharField(_('species'), blank=True,
                                     max_length=255)
    plants = models.BooleanField(_('plants'), default=False)
    plants_species = models.CharField(_('species'), blank=True,
                                      max_length=255)
    fish = models.BooleanField(_('fish'), default=False)
    fish_species = models.CharField(_('species'), blank=True,
                                    max_length=255)

    # Potential implications
    water_crossing = models.BooleanField(
        _('water crossing'),
        default=False,
        help_text=_('water crossing help text'),
    )
    off_trail = models.BooleanField(
        _('off trail'),
        default=False,
        help_text=_('off trail help text'),
    )
    bio_implications = models.BooleanField(
        _('bio implications'),
        default=False,
        help_text=_('bio implications help text'),
    )
    related_facilities = models.BooleanField(
        _('related facilities'),
        default=False,
        help_text=_('related facilities help text'),
    )
    light_emissions = models.BooleanField(
        _('light emissions'),
        default=False,
        help_text=_('light emissions help text'),
    )
    # Conclusion
    natura_2000_conclusion = models.BooleanField(
        _('natura 2000 conclusion'),
        default=False,
        help_text=_('natura 2000 evaluation help text'),
    )

    class Meta(EventEvaluation.Meta):  # pylint: disable=C1001
        verbose_name = _('natura 2000 evaluation')
        verbose_name_plural = _('natura 2000 evaluations')

    def __str__(self):
        return smart_text(self.event.__str__())
