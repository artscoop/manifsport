from __future__ import unicode_literals

from django.contrib import admin

from .models import RNREvaluation
from .models import Natura2000Evaluation


class RNREvaluationInline(admin.StackedInline):
    model = RNREvaluation
    extra = 0


class Natura2000EvaluationInline(admin.StackedInline):
    model = Natura2000Evaluation
    extra = 0
