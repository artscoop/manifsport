from __future__ import unicode_literals

from django.test import TestCase

from .factories import RNREvaluationFactory
from .factories import Natura2000EvaluationFactory


class RNREvaluationMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the evaluation
        '''
        rnr_evaluation = RNREvaluationFactory.build()
        self.assertEqual(rnr_evaluation.__str__(), rnr_evaluation.event.__str__())


class Natura2000EvaluationMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the evaluation
        '''
        n2000_evaluation = Natura2000EvaluationFactory.build()
        self.assertEqual(n2000_evaluation.__str__(), n2000_evaluation.event.__str__())
