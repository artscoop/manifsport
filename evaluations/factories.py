from __future__ import unicode_literals

import datetime
import factory

from factory.fuzzy import FuzzyDate

from events.factories import DeclaredNonMotorizedEventFactory

from .models import RNREvaluation
from .models import Natura2000Evaluation


class RNREvaluationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = RNREvaluation

    administrator_contact_date = FuzzyDate(datetime.date(2010, 1, 1))
    rnr_entries = 1
    rnr_audience = 1
    track_total_length = 1
    event = factory.SubFactory(DeclaredNonMotorizedEventFactory)


class Natura2000EvaluationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Natura2000Evaluation

    range_from_site = 5
    track_total_length = 1
    event = factory.SubFactory(DeclaredNonMotorizedEventFactory)
