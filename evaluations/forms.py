from __future__ import unicode_literals

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from crispy_forms.bootstrap import FormActions, AppendedText

from bootstrap3_datetime.widgets import DateTimePicker

from ddcs_loire.utils import GenericForm

from .models import Natura2000Evaluation, RNREvaluation


IMPACTS_HELP = HTML(
    "<div class='well'>"
    "<p>{0}</p><a href={1} target='_blank'>{2}</a></div>".format(
        _('fill in this table').capitalize(),
        settings.DDT_SITE,
        _('site list per commune').capitalize(),
    )
)

NATURAL_IMPACTS_FIELDSET = (
    'lawn',
    'lawn_comments',
    'semi_wooded_lawn',
    'semi_wooded_lawn_comments',
    'moor',
    'moor_comments',
    'scrubland_maquis',
    'scrubland_maquis_comments',
    'coniferous_forest',
    'coniferous_forest_comments',
    'deciduous_forest',
    'deciduous_forest_comments',
    'mixed_forest',
    'mixed_forest_comments',
    'plantation',
    'plantation_comments',
    'cliff',
    'cliff_comments',
    'outcrop',
    'outcrop_comments',
    'scree',
    'scree_comments',
    'blocks',
    'blocks_comments',
    'ditch',
    'ditch_comments',
    'watercourse',
    'watercourse_comments',
    'pound',
    'pound_comments',
    'bog',
    'bog_comments',
    'gravel',
    'gravel_comments',
    'wet_meadow',
    'wet_meadow_comments',
)
SPECIES_IMPACTS_FIELDSET = (
    'amphibia',
    'amphibia_species',
    'reptiles',
    'reptiles_species',
    'crustaceans',
    'crustaceans_species',
    'insects',
    'insects_species',
    'terrestrial_mammals',
    'terrestrial_mammals_species',
    'birds',
    'birds_species',
    'plants',
    'plants_species',
    'fish',
    'fish_species',
)
FACILITIES_FIELDSET_N2000 = (
    'parking_lot',
    'parking_lot_desc',
    'reception_area',
    'reception_area_desc',
    'supplying_area',
    'supplying_area_desc',
    'storage_area',
    'storage_area_desc',
    'awards_stage',
    'awards_stage_desc',
    'bivouac_area',
    'bivouac_area_desc',
    'noise',
    'noise_desc',
    'night_lighting',
    'night_lighting_desc',
    'trails_marks',
    'trails_marks_desc',
    'other_facilities',
    'other_facilities_desc',
)

FACILITIES_FIELDSET_RNR = (
    'night_lighting',
    'night_lighting_desc',
    'trails_marks',
    'trails_marks_desc',
    'other_facilities',
    'other_facilities_desc',
)

N2K_IMPLICATIONS_FIELDSET = (
    'water_crossing',
    'off_trail',
    'bio_implications',
    'related_facilities',
    'noise_emissions',
    'light_emissions',
    'waste_management',
    'waste_management_desc',
    'awareness',
    'impact_reduction',
    'impact_reduction_desc',
)
RNR_IMPLICATIONS_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<p>{paragraph}</p>"
        "<ul>"
        "<li>{parking}</li>"
        "<li>{trash}</li>"
        "<li>{picking}</li>"
        "<li>{light}</li>"
        "<li>{noise}</li>"
        "<li>{animals}</li>"
        "</ul>"
        "</div>".format(
            paragraph=_("are forbidden on the RNR").capitalize(),
            parking=_("parking and wandering").capitalize(),
            trash=_("trash disposal").capitalize(),
            picking=_("picking").capitalize(),
            light=_("use of artificial lighting").capitalize(),
            noise=_("too much noise").capitalize(),
            animals=_("domestic animals").capitalize(),
        )
    ),
    'noise_emissions',
    'waste_management',
    'waste_management_desc',
    'awareness',
    'impact_reduction',
    'impact_reduction_desc',
    'plane_attendance',
    'plane_attendance_desc',
    'trampling',
    'trampling_desc',
    'markup',
    'markup_desc',
)
AREA_OF_INFLUENCE_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<p>1. {0} <a href={1} target='_blank'>{2}</a> {3}</p>"
        "<p>2. {4}</p>"
        "<p>3. {5}</p>"
        "<p>4. {6}</p>"
        "<p></p>"
        "<p><a href={7} target='_blank'>{8}</a></p>"
        "</div>".format(
            _('access to the').capitalize(),
            settings.CARMEN_WEBSITE,
            _('carmen').upper(),  # pylint: disable=E1101
            _('of the dreal rhones alps'),
            _('select the department and the commune').capitalize(),
            _('spot the event zone').capitalize(),
            _('select asked items').capitalize(),
            settings.LOIRE_PROTECTED_AREAS,
            _('protected natural sites in the department').capitalize(),
        )
    ),
    'rnn',
    'rnr',
    'biotope_area',
    'classified_site',
    'registered_site',
    'pnr',
    'znieff',
)
N2K_CONCLUSIONS_FIELDSET = (
    'estimated_impact',
    'natura_2000_conclusion',
)
N2K_EVALUATION_FIELDSET = (
    'place',
    'diurnal',
    'nocturnal',
)
FREQUENCY_FIELDSET = (
    'every_year',
    'first_edition',
    'other_frequency',
)
RNR_CONCLUSIONS_FIELDSET = (
    HTML(
        "<div class='well'>"
        "{0} "
        "<a href='/rnr/reglement/gorges-de-la-loire/' target='_blank'>"
        "{1}</a></div>".format(
            _('regulation of rnr'),
            _('gorges de la loire'),
        )
    ),
    'regulatory_compliance',
    'rnr_conclusion',
    'estimated_impact',
)
PROVISIONS_FIELDSET = (
    'administrator_contact_date',
    'sensitization',
    'forbidden_areas',
    'administrator_attendance',
)
ATTENDANCE_FIELDSET = (
    AppendedText('rnr_entries', 'personnes'),
    AppendedText('rnr_audience', 'personnes'),
)
LOCALIZATION_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<a href='/protected_areas/regionalnaturalreserve/' target='_blank'>"
        "{contacts}</a></div>".format(
            contacts=_("regional natural reserves contacts")
        )
    ),
    'sites',
    AppendedText('track_total_length', 'km'),
)
DESCRIPTION_FIELDSET = (
    'place',
    'request_duration',
)


class Natura2000EvaluationForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = Natura2000Evaluation
        exclude = ('content_type', 'object_id', 'event')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(Natura2000EvaluationForm,
              self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                _('natura 2000 evaluation form').capitalize(),
                *N2K_EVALUATION_FIELDSET
            ),
            Fieldset(
                _('frequency').capitalize(),
                *FREQUENCY_FIELDSET
            ),
            Fieldset(
                _('budget').capitalize(),
                'cost',
            ),
            Fieldset(
                _('natura 2000 relative localization').capitalize(),
                HTML(
                    "<div class='well'>"
                    "<a href='/protected_areas/natura2000site/'"
                    " target='_blank'>"
                    "{contacts}</a></div>".format(
                        contacts=_('natura 2000 sites contacts')
                    )
                ),
                'sites',
                AppendedText('track_total_length', 'km'),
                AppendedText('range_from_site', 'km'),
            ),
            Fieldset(
                _('area of influence').capitalize(),
                *AREA_OF_INFLUENCE_FIELDSET
            ),
            Fieldset(
                _('impacts on natural habitats').capitalize(),
                IMPACTS_HELP,
                *NATURAL_IMPACTS_FIELDSET
            ),
            Fieldset(
                _('impacts on species').capitalize(),
                IMPACTS_HELP,
                *SPECIES_IMPACTS_FIELDSET
            ),
            Fieldset(
                _('related facilities').capitalize(),
                *FACILITIES_FIELDSET_N2000
            ),
            Fieldset(
                _('natura 2000 potential implications').capitalize(),
                *N2K_IMPLICATIONS_FIELDSET
            ),
            Fieldset(
                _('conclusions').capitalize(),
                *N2K_CONCLUSIONS_FIELDSET
            ),
            FormActions(
                Submit('save', _('submit').capitalize())
            )
        )


class RNREvaluationForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = RNREvaluation
        exclude = ('content_type', 'object_id', 'event')
        labels = {
            'track_total_length': _('track total length in rnr'),
        }
        widgets = {
            'administrator_contact_date': DateTimePicker(
                options={"format": "DD/MM/YYYY",
                         "pickTime": False}
                ),
        }

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(RNREvaluationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                _('RNR evaluation form').capitalize(),
                *DESCRIPTION_FIELDSET
            ),
            Fieldset(
                _('frequency').capitalize(),
                *FREQUENCY_FIELDSET
            ),
            Fieldset(
                _('rnr relative localization').capitalize(),
                *LOCALIZATION_FIELDSET
            ),
            Fieldset(
                _('attendance').capitalize(),
                *ATTENDANCE_FIELDSET
            ),
            Fieldset(
                _('budget').capitalize(),
                'cost',
            ),
            Fieldset(
                _('provisions taken').capitalize(),
                *PROVISIONS_FIELDSET
            ),
            Fieldset(
                _('related facilities').capitalize(),
                *FACILITIES_FIELDSET_RNR
            ),
            Fieldset(
                _('rnr potential implications').capitalize(),
                *RNR_IMPLICATIONS_FIELDSET
            ),
            Fieldset(
                _('conclusion').capitalize(),
                *RNR_CONCLUSIONS_FIELDSET
            ),
            FormActions(
                Submit('save', _('submit').capitalize())
            )
        )
