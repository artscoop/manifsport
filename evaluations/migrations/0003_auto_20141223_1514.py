# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def change_event_to_foreign_key(apps, schema_editor):
    Event = apps.get_model('events', 'Event')
    RNREvaluation = apps.get_model('evaluations', 'RNREvaluation')
    Natura2000Evaluation = apps.get_model('evaluations', 'Natura2000Evaluation')
    for rnr in RNREvaluation.objects.all():
        rnr.event = Event.objects.get(pk=rnr.object_id)
        rnr.save()
    for natura2000 in Natura2000Evaluation.objects.all():
        natura2000.event = Event.objects.get(pk=natura2000.object_id)
        natura2000.save()


def change_event_to_generic_relation(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('evaluations', '0002_auto_20141223_1513'),
    ]

    operations = [
        migrations.RunPython(change_event_to_foreign_key, change_event_to_generic_relation)
    ]
