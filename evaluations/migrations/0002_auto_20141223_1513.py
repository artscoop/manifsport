# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_remove_event_dps_needed'),
        ('evaluations', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='natura2000evaluation',
            name='event',
            field=models.OneToOneField(null=True, blank=True, to='events.Event', verbose_name='event'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rnrevaluation',
            name='event',
            field=models.OneToOneField(null=True, blank=True, to='events.Event', verbose_name='event'),
            preserve_default=True,
        ),
    ]
