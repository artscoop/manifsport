# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evaluations', '0003_auto_20141223_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='natura2000evaluation',
            name='event',
            field=models.OneToOneField(verbose_name='event', to='events.Event'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rnrevaluation',
            name='event',
            field=models.OneToOneField(verbose_name='event', to='events.Event'),
            preserve_default=True,
        ),
    ]
