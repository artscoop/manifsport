from __future__ import unicode_literals

from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from events.decorators import promoter_required
from events.models import Event

from .models import Natura2000Evaluation, RNREvaluation
from .forms import Natura2000EvaluationForm, RNREvaluationForm


class GenericCreateView(CreateView):  # pylint: disable=R0901

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(GenericCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        form.instance.event = Event.objects.get(pk=self.kwargs['event_pk'])
        return super(GenericCreateView, self).form_valid(form)


class GenericUpdateView(UpdateView):  # pylint: disable=R0901

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(GenericUpdateView, self).dispatch(*args, **kwargs)


class Natura2000EvaluationCreate(GenericCreateView):  # pylint: disable=R0901
    model = Natura2000Evaluation
    form_class = Natura2000EvaluationForm


class Natura2000EvaluationUpdate(GenericUpdateView):  # pylint: disable=R0901
    model = Natura2000Evaluation
    form_class = Natura2000EvaluationForm


class RNREvaluationCreate(GenericCreateView):  # pylint: disable=R0901
    model = RNREvaluation
    form_class = RNREvaluationForm


class RNREvaluationUpdate(GenericUpdateView):  # pylint: disable=R0901
    model = RNREvaluation
    form_class = RNREvaluationForm
