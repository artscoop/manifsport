# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorizations', '0005_auto_20141216_0925'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventauthorization',
            name='ggd_concerned',
            field=models.BooleanField(default=False, verbose_name='ggd agreement required'),
            preserve_default=True,
        ),
    ]
