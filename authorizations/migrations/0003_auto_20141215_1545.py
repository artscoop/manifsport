# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def move_event_relation_to_authorization(apps, schema_editor):
    NonMotorizedEventAuthorization = apps.get_model('authorizations', 'NonMotorizedEventAuthorization')
    MotorizedConcentrationAuthorization = apps.get_model('authorizations', 'MotorizedConcentrationAuthorization')
    MotorizedRaceAuthorization = apps.get_model('authorizations', 'MotorizedRaceAuthorization')
    MotorizedEventAuthorization = apps.get_model('authorizations', 'MotorizedEventAuthorization')
    for authorization in NonMotorizedEventAuthorization.objects.all():
        authorization.event = authorization.related_event
        authorization.save()
    for authorization in MotorizedConcentrationAuthorization.objects.all():
        authorization.event = authorization.related_event
        authorization.save()
    for authorization in MotorizedRaceAuthorization.objects.all():
        authorization.event = authorization.related_event
        authorization.save()
    for authorization in MotorizedEventAuthorization.objects.all():
        authorization.event = authorization.related_event
        authorization.save()


def move_event_relation_to_authorization_subclass(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('authorizations', '0002_eventauthorization_event'),
    ]

    operations = [
        migrations.RunPython(move_event_relation_to_authorization, move_event_relation_to_authorization_subclass)
    ]
