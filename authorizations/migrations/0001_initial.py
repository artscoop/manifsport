# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('administrative_division', '0001_initial'),
        ('administration', '0001_initial'),
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventAuthorization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateField(auto_now_add=True, verbose_name='creation date')),
                ('dispatch_date', models.DateField(null=True, verbose_name='dispatch date', blank=True)),
                ('ddsp_concerned', models.BooleanField(default=False, verbose_name='ddsp agreement required')),
                ('edsr_concerned', models.BooleanField(default=False, verbose_name='edsr agreement required')),
                ('sdis_concerned', models.BooleanField(default=False, verbose_name='sdis agreement required')),
                ('cg_concerned', models.BooleanField(default=False, verbose_name='cg agreement required')),
                ('bylaw', models.FileField(upload_to='arretes/%Y/%m/%d', null=True, verbose_name='bylaw', blank=True)),
                ('state', django_fsm.FSMField(default='created', max_length=50, verbose_name='authorization state')),
            ],
            options={
                'verbose_name': 'event authorization',
                'verbose_name_plural': 'event authorizations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MotorizedConcentrationAuthorization',
            fields=[
                ('eventauthorization_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='authorizations.EventAuthorization')),
                ('related_event', models.OneToOneField(verbose_name='related event', to='events.MotorizedConcentration')),
            ],
            options={
                'verbose_name': 'motorized concentration authorization',
                'verbose_name_plural': 'motorized concentration authorizations',
            },
            bases=('authorizations.eventauthorization',),
        ),
        migrations.CreateModel(
            name='MotorizedEventAuthorization',
            fields=[
                ('eventauthorization_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='authorizations.EventAuthorization')),
                ('related_event', models.OneToOneField(verbose_name='related event', to='events.MotorizedEvent')),
            ],
            options={
                'verbose_name': 'motorized event authorization',
                'verbose_name_plural': 'motorized event authorizations',
            },
            bases=('authorizations.eventauthorization',),
        ),
        migrations.CreateModel(
            name='MotorizedRaceAuthorization',
            fields=[
                ('eventauthorization_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='authorizations.EventAuthorization')),
                ('related_event', models.OneToOneField(verbose_name='related event', to='events.MotorizedRace')),
            ],
            options={
                'verbose_name': 'motorized race authorization',
                'verbose_name_plural': 'motorized race authorizations',
            },
            bases=('authorizations.eventauthorization',),
        ),
        migrations.CreateModel(
            name='NonMotorizedEventAuthorization',
            fields=[
                ('eventauthorization_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='authorizations.EventAuthorization')),
                ('related_event', models.OneToOneField(verbose_name='related event', to='events.AuthorizedNonMotorizedEvent')),
            ],
            options={
                'verbose_name': 'non motorized event authorization',
                'verbose_name_plural': 'non motorized event authorizations',
            },
            bases=('authorizations.eventauthorization',),
        ),
        migrations.AddField(
            model_name='eventauthorization',
            name='concerned_cities',
            field=models.ManyToManyField(to='administrative_division.Commune', null=True, verbose_name='concerned cities', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='eventauthorization',
            name='concerned_services',
            field=models.ManyToManyField(to='administration.Service', null=True, verbose_name='concerned services', blank=True),
            preserve_default=True,
        ),
    ]
