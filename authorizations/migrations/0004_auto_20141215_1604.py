# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorizations', '0003_auto_20141215_1545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventauthorization',
            name='event',
            field=models.OneToOneField(verbose_name='related event', to='events.Event'),
            preserve_default=True,
        ),
    ]
