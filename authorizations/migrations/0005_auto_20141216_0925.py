# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0001_initial'),
        ('authorizations', '0004_auto_20141215_1604'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='motorizedconcentrationauthorization',
            name='eventauthorization_ptr',
        ),
        migrations.RemoveField(
            model_name='motorizedconcentrationauthorization',
            name='related_event',
        ),
        migrations.DeleteModel(
            name='MotorizedConcentrationAuthorization',
        ),
        migrations.RemoveField(
            model_name='motorizedeventauthorization',
            name='eventauthorization_ptr',
        ),
        migrations.RemoveField(
            model_name='motorizedeventauthorization',
            name='related_event',
        ),
        migrations.DeleteModel(
            name='MotorizedEventAuthorization',
        ),
        migrations.RemoveField(
            model_name='motorizedraceauthorization',
            name='eventauthorization_ptr',
        ),
        migrations.RemoveField(
            model_name='motorizedraceauthorization',
            name='related_event',
        ),
        migrations.DeleteModel(
            name='MotorizedRaceAuthorization',
        ),
        migrations.RemoveField(
            model_name='nonmotorizedeventauthorization',
            name='eventauthorization_ptr',
        ),
        migrations.RemoveField(
            model_name='nonmotorizedeventauthorization',
            name='related_event',
        ),
        migrations.DeleteModel(
            name='NonMotorizedEventAuthorization',
        ),
    ]
