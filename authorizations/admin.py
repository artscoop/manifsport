from __future__ import unicode_literals

from django.contrib import admin

from .models import EventAuthorization


class EventAuthorizationInline(admin.StackedInline):
    model = EventAuthorization
    extra = 0
    max_num = 1
    readonly_fields = ('creation_date',)
