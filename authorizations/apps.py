from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AuthorizationsConfig(AppConfig):
    name = 'authorizations'
    verbose_name = _('Authorizations')
