��    5      �  G   l      �     �     �     �     �     �            	   .     8     G     c     }     �     �     �     �     �     �          .     G     M     ]     n     �     �     �     �  	   �     �     �     �          !     /     G     [     p     �  	   �  5   �     �     �               *     =     K     Y     g     �     �  �  �     9
  7   G
  -   
     �
  .   �
     �
          '     ,  %   D     j     �     �     �     �     �  !   �          "     :     S     k  (   �     �  '   �     �          !     3     C     T     b     ~      �     �  &   �  '   �          8  	   T  >   ^     �     �     �     �     �     
          ,  %   D     j     {               )   *   5          4       "                            .          0       	       !         %          /                                   #      ,      
   &          '       (       1                       -   2          3   $   +          Authorizations No authorizations to manage No declarations to manage Raise agreement request You must specify a bylaw file actions available add agreements requests agreement agreement date agreement has been sent the agreement is under review agreements requests sent agreements required ask for the authorization attached document authorization authorization request date authorization request sent authorization state authorizations requested bylaw bylaw published bylaw publishing cg agreement required choose the bylaw file concerned cities concerned services creation date dashboard ddsp agreement required declarations dispatch dispatch agreements requests dispatch date edsr agreement required event authorization event authorizations event creation date event detail favorable find here all actions available related to this event ggd agreement required go back to the dashboard instruction started instructor dashboard missing agreements prescriptions publish bylaw related event request more information sdis agreement required type of event Project-Id-Version: authorization
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-11 10:30+0100
PO-Revision-Date: 2016-01-11 10:36+0100
Last-Translator: Steve Kossouho <steve.kossouho@gmail.com>
Language-Team: français <>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Gtranslator 2.91.7
 Autorisations Pas de demandes d'autorisation en attente de traitement Pas de déclarations en attente de traitement Relancer la demande d'avis Vous devez sélectionner l'arrêté à publier actions disponibles ajouter des demandes d'avis avis date de rendu de l'avis la demande d'avis a été envoyée le l'avis est en cours d'examen demandes d'avis envoyées avis requis demander l'autorisation pièce jointe autorisation date de la demande d'autorisation demande d'autorisation envoyée état de l'autorisation autorisations demandées arrêté d'autorisation arrêté d'autorisation publié publication de l'arrêté d'autorisation avis Conseil Général requis sélection de l'arrêté d'autorisation villes concernées services concernés date de création tableau de bord avis DDSP requis déclarations envoyer les demandes d'avis envoyer les demandes d'avis date d'envoi des demandes d'avis avis EDSR requis autorisation de manifestation sportive autorisations de manifestation sportive date de création du dossier détail de la manifestation favorable voici les actions disponibles relatives à cette manifestation avis GGD requis retour au tableau de bord début de l'instruction tableau de bord instructeur avis manquants préscriptions publier l'arrêté manifestation associée Demander un complément d'information avis SDIS requis type de manifestation 