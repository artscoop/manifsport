from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

from declarations.models import EventDeclaration

from events.decorators import promoter_required
from events.models import Event
from events.models import News

from .decorators import instructor_required

from .forms import AuthorizationPublishBylawForm
from .forms import AuthorizationForm
from .forms import AuthorizationDispatchForm

from .models import EventAuthorization


class Dashboard(ListView):  # pylint: disable=R0901
    model = EventAuthorization

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        instructor = self.request.user.instructor
        return EventAuthorization.objects.to_process().for_instructor(instructor).closest_first()

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        instructor = self.request.user.instructor
        context['declarations'] = EventDeclaration.objects.to_process().for_instructor(instructor).closest_first()
        context['last_news'] = News.objects.last()
        return context


class AuthorizationCreateView(CreateView):  # pylint: disable=R0901
    model = EventAuthorization
    form_class = AuthorizationForm

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.event = get_object_or_404(
            Event,
            pk=self.kwargs['event_pk']
        )
        return super(AuthorizationCreateView, self).form_valid(form)

    def get_success_url(self):
        try:
            self.object.event.authorizednonmotorizedevent
        except:
            try:
                self.object.event.motorizedconcentration
            except:
                try:
                    self.object.event.motorizedevent
                except:
                    try:
                        self.object.event.motorizedrace
                    except:
                        pass
                    else:
                        return reverse('events:motorizedrace_detail', kwargs={'pk': self.object.event.pk})
                else:
                    return reverse('events:motorizedevent_detail', kwargs={'pk': self.object.event.pk})
            else:
                return reverse('events:motorizedconcentration_detail', kwargs={'pk': self.object.event.pk})
        else:
            return reverse('events:authorizednonmotorizedevent_detail', kwargs={'pk': self.object.event.pk})


class AuthorizationDetailView(DetailView):  # pylint: disable=R0901
    model = EventAuthorization

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationDetailView, self).dispatch(*args, **kwargs)


class AuthorizationUpdateView(UpdateView):  # pylint: disable=R0901
    model = EventAuthorization
    form_class = AuthorizationDispatchForm
    success_url = "/authorizations/%(id)s"

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.dispatch()
        return super(AuthorizationUpdateView, self).form_valid(form)


class AuthorizationPublishBylawView(UpdateView):  # pylint: disable=R0901
    model = EventAuthorization
    form_class = AuthorizationPublishBylawForm
    template_name_suffix = '_publish_form'

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationPublishBylawView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.publish()
        return super(AuthorizationPublishBylawView, self).form_valid(form)
