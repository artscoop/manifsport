from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django_fsm import FSMField, transition

from administration.models import Service, Prefecture
from administrative_division.models import Commune
from administrative_division.models import CHOICES

from events.models import Event

from ddcs_loire.utils import add_notification_entry
from ddcs_loire.utils import add_log_entry

from .signals import authorization_published


class EventAuthorizationQuerySet(models.QuerySet):
    def for_instructor(self, instructor):
        return self.filter(event__departure_city__arrondissement=instructor.prefecture.arrondissement)

    def to_process(self):
        return self.filter(event__end_date__gt=timezone.now())

    def closest_first(self):
        return self.order_by('event__begin_date')


@python_2_unicode_compatible
class EventAuthorization(models.Model):
    creation_date = models.DateField(_('creation date'), auto_now_add=True)
    dispatch_date = models.DateField(_('dispatch date'), blank=True, null=True)
    event = models.OneToOneField(Event, verbose_name=_('related event'))

    concerned_services = models.ManyToManyField(
        Service,
        verbose_name=_('concerned services'),
        blank=True,
        null=True,
    )
    concerned_cities = models.ManyToManyField(
        Commune,
        verbose_name=_('concerned cities'),
        limit_choices_to=CHOICES,
        blank=True,
        null=True,
    )
    ddsp_concerned = models.BooleanField(
        _('ddsp agreement required'),
        default=False,
    )
    edsr_concerned = models.BooleanField(
        _('edsr agreement required'),
        default=False,
    )
    ggd_concerned = models.BooleanField(
        _('ggd agreement required'),
        default=False,
    )
    sdis_concerned = models.BooleanField(
        _('sdis agreement required'),
        default=False,
    )
    cg_concerned = models.BooleanField(
        _('cg agreement required'),
        default=False,
    )
    bylaw = models.FileField(
        upload_to='arretes/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('bylaw'),
        max_length=100,
    )

    state = FSMField(default='created', verbose_name=_('authorization state'))
    objects = EventAuthorizationQuerySet.as_manager()

    class Meta:  # pylint: disable=C1001
        verbose_name = _('event authorization')
        verbose_name_plural = _('event authorizations')

    def __str__(self):
        return self.event.name

    def get_absolute_url(self):
        url = 'authorizations:dashboard'
        return reverse(url)

    def get_concerned_prefecture(self):
        return self.event.departure_city.arrondissement.prefecture

    def agreements_validated(self):
        return self.agreement_set.filter(state='acknowledged').count()

    def agreements_issued(self):
        return self.agreement_set.all().count()

    def all_agreements_validated(self):
        return self.agreements_validated() == self.agreements_issued()

    def agreements_missing(self):
        return self.agreements_issued() - self.agreements_validated()

    def log_creation(self):
        add_log_entry(agents=[self.event.structure.promoter],
                      action=_('authorization request sent'),
                      event=self.event)

    def log_dispatch(self, prefecture):
        add_log_entry(agents=prefecture.instructor_set.all(),
                      action=_('agreements requests sent'),
                      event=self.event)

    def log_publish(self, prefecture):
        add_log_entry(agents=prefecture.instructor_set.all(),
                      action=_('bylaw published'),
                      event=self.event)

    def notify_creation(self):
        try:
            prefecture = self.get_concerned_prefecture()
        except Prefecture.DoesNotExist:
            pass
        else:
            add_notification_entry(
                subject=_('authorization request sent'),
                agents=prefecture.instructor_set.all(),
                event=self.event,
                content_object=self.event.structure,
                institutional=prefecture,
            )

    def notify_dispatch(self, prefecture):
        add_notification_entry(
            agents=[self.event.structure.promoter],
            event=self.event,
            subject=_('instruction started'),
            content_object=prefecture,
        )

    def notify_publish(self, prefecture):
        add_notification_entry(
            agents=[self.event.structure.promoter],
            event=self.event,
            subject=_('bylaw published'),
            content_object=prefecture,
        )

    @transition(field=state, source=['created', 'dispatched'],
                target='dispatched')
    def dispatch(self):
        try:
            prefecture = self.get_concerned_prefecture()
        except Prefecture.DoesNotExist:
            pass
        else:
            if self.state == 'created':
                self.notify_dispatch(prefecture)
                self.log_dispatch(prefecture)
                self.dispatch_date = timezone.now()
                self.save

    @transition(field=state, source='dispatched', target='published')
    def publish(self):
        try:
            prefecture = self.get_concerned_prefecture()
        except Prefecture.DoesNotExist:
            pass
        else:
            self.notify_publish(prefecture)
            self.log_publish(prefecture)
            authorization_published.send(sender=self.__class__, instance=self)


@receiver(post_save, sender=EventAuthorization)
def log_authorization_creation(created, instance, **kwargs):
    if created:
        instance.log_creation()
        instance.notify_creation()
