from __future__ import unicode_literals

import factory

from events.factories import AuthorizedNonMotorizedEventFactory

from .models import EventAuthorization


class EventAuthorizationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = EventAuthorization

    event = factory.SubFactory(AuthorizedNonMotorizedEventFactory)
