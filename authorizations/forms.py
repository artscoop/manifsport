from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Submit, Layout, Fieldset
from crispy_forms.bootstrap import FormActions

from .models import EventAuthorization


class AuthorizationForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = EventAuthorization
        exclude = [
            'event',
            'creation_date',
            'dispatch_date',
            'state',
            'concerned_cities',
            'concerned_services',
            'ddsp_concerned',
            'edsr_concerned',
            'ggd_concerned',
            'sdis_concerned',
            'cg_concerned',
            'bylaw',
        ]

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(AuthorizationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                FormActions(
                        Submit('save', _('ask for the authorization').capitalize())
                )
        )


class AuthorizationDispatchForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = EventAuthorization
        exclude = [
            'event',
            'creation_date',
            'dispatch_date',
            'state',
            'bylaw',
        ]

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(AuthorizationDispatchForm, self).__init__(*args, **kwargs)
        if settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'] == 'GGD':
            self.fields['ggd_concerned'].initial = True
            self.fields['ggd_concerned'].widget.attrs['disabled'] = True
            self.fields['edsr_concerned'].initial = False
            self.fields['edsr_concerned'].widget.attrs['disabled'] = False
        elif settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'] == 'GGD':
            self.fields['ggd_concerned'].initial = False
            self.fields['ggd_concerned'].widget.attrs['disabled'] = False
            self.fields['edsr_concerned'].initial = True
            self.fields['edsr_concerned'].widget.attrs['disabled'] = True
        self.helper.layout = Layout(
            Fieldset(
                _('agreements required').capitalize(),
                'ddsp_concerned',
                'edsr_concerned',
                'ggd_concerned',
                'sdis_concerned',
                'cg_concerned',
                'concerned_cities',
                'concerned_services',
            ),
            FormActions(
                Submit('save', _('dispatch').capitalize())
            )
        )


class AuthorizationPublishBylawForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = EventAuthorization
        fields = ('bylaw', )

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(AuthorizationPublishBylawForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                _('choose the bylaw file').capitalize(),
                'bylaw',
            ),
            FormActions(
                Submit('save', _('publish bylaw').capitalize())
            )
        )

    def clean_bylaw(self):
        data = self.cleaned_data['bylaw']
        if not data:
            raise forms.ValidationError(_('You must specify a bylaw file'))
        return data
