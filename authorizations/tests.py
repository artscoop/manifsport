from __future__ import unicode_literals

from django.test import TestCase

from administration.factories import InstructorFactory
from administration.factories import PrefectureFactory

from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory

from events.factories import AuthorizedNonMotorizedEventFactory

from notifications.models import Action
from notifications.models import Notification

from .factories import EventAuthorizationFactory

from .models import EventAuthorization


def create_authorization_tree():
    arrondissement = ArrondissementFactory.create()
    prefecture = PrefectureFactory.create(
        arrondissement=arrondissement
    )
    instructor = InstructorFactory.create(prefecture=prefecture)
    commune = CommuneFactory.create(
        arrondissement=arrondissement
    )
    event = AuthorizedNonMotorizedEventFactory.create(
        departure_city=commune
    )
    authorization = EventAuthorizationFactory.create(
        event=event
    )
    return authorization, event, instructor


class EventAuthorizationMethodTests(TestCase):

    def test_for_instructor(self):
        authorization, _event, instructor = create_authorization_tree()
        self.assertEqual(EventAuthorization.objects.for_instructor(instructor)[0].pk,
                         authorization.pk)

    def test_str_(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.__str__(),
                         authorization.event.__str__())

    def test_get_concerned_prefecture(self):
        authorization = EventAuthorizationFactory.create()
        prefecture = PrefectureFactory.create(
            arrondissement=authorization.event.departure_city.arrondissement
        )
        self.assertEqual(authorization.get_concerned_prefecture(), prefecture)

    def test_not_all_agreements_validated(self):
        authorization = EventAuthorizationFactory.create()
        self.assertFalse(authorization.all_agreements_validated())

    def test_all_agreements_validated(self):
        authorization = EventAuthorizationFactory.create()
        a = authorization.agreement_set.first()
        a.state = 'acknowledged'
        a.save()
        self.assertTrue(authorization.all_agreements_validated())

    def test_log_authorization_creation(self):
        authorization = EventAuthorizationFactory.create()
        action = Action.objects.last()
        self.assertEqual(action.user,
                         authorization.event.structure.promoter.user)
        self.assertEqual(action.event,
                         authorization.event.event_ptr)
        self.assertEqual(action.action, "authorization request sent")
        # when authorization is only updated,
        # no log action is created
        authorization.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)

    def test_notify_authorization_creation(self):
        authorization, event, instructor = create_authorization_tree()
        notification = Notification.objects.last()
        self.assertEqual(notification.user, instructor.user)
        self.assertEqual(notification.event, event.event_ptr)
        self.assertEqual(notification.subject, "authorization request sent")
        self.assertEqual(notification.content_object,
                         authorization.event.structure)

    def test_notify_authorization_creation_with_no_prefecture(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(Notification.objects.filter(event=authorization.event).count(),
                         0)
