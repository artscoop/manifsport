from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import Dashboard

from .views import AuthorizationCreateView
from .views import AuthorizationDetailView
from .views import AuthorizationUpdateView
from .views import AuthorizationPublishBylawView

urlpatterns = patterns(
    # pylint: disable=E1120
    '',
    url(r'^dashboard/$', Dashboard.as_view(), name="dashboard"),
    url(r'^add/(?P<event_pk>\d+)$', AuthorizationCreateView.as_view(), name='authorization_add'),
    url(r'^(?P<pk>\d+)/$', AuthorizationDetailView.as_view(), name="authorization_detail"),
    url(r'^(?P<pk>\d+)/publish/$', AuthorizationPublishBylawView.as_view(), name='authorization_publish'),
    url(r'^(?P<pk>\d+)/edit/$', AuthorizationUpdateView.as_view(), name="authorization_update"),
    # pylint: enable=E1120
)
