from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AgreementsConfig(AppConfig):
    name = 'agreements'
    verbose_name = _('Agreements')
