from __future__ import unicode_literals

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render

from allauth.account.decorators import verified_email_required

from administration.models import Agent
from administration.models import CGAgent
from administration.models import CGSuperior
from administration.models import GGDAgent
from administration.models import DDSPAgent
from administration.models import EDSRAgent
from administration.models import BrigadeAgent
from administration.models import FederationAgent
from administration.models import ServiceAgent
from administration.models import SDISAgent
from administration.models import CODISAgent
from administration.models import CISAgent
from administration.models import TownHallAgent


def agent_required(function=None,
                   login_url=None,
                   redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @verified_email_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent
            except Agent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cg_required(function=None,
                login_url=None,
                redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.cgagent
            except CGAgent.DoesNotExist:
                try:
                    request.user.agent.cgsuperior
                except CGSuperior.DoesNotExist:
                    return render(request,
                                  'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cg_superior_required(function=None,
                         login_url=None,
                         redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.cgsuperior
            except CGSuperior.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cg_agent_required(function=None,
                      login_url=None,
                      redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.cgagent
            except CGAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def ddsp_agent_required(function=None,
                        login_url=None,
                        redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.ddspagent
            except DDSPAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def edsr_agent_required(function=None,
                        login_url=None,
                        redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.edsragent
            except EDSRAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def federal_agent_required(function=None,
                           login_url=None,
                           redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.federationagent
            except FederationAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def townhall_agent_required(function=None,
                            login_url=None,
                            redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.townhallagent
            except TownHallAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def ggd_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.ggdagent
            except GGDAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def gendarmerie_agent_required(function=None,
                               login_url=None,
                               redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.edsragent
            except EDSRAgent.DoesNotExist:
                try:
                    request.user.agent.ggdagent
                except GGDAgent.DoesNotExist:
                    try:
                        request.user.agent.brigadeagent
                    except BrigadeAgent.DoesNotExist:
                        return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def service_agent_required(function=None,
                           login_url=None,
                           redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.serviceagent
            except ServiceAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def sdis_agent_required(function=None,
                        login_url=None,
                        redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.sdisagent
            except SDISAgent.DoesNotExist:
                return render(request,
                              'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def fireman_required(function=None,
                     login_url=None,
                     redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agent.sdisagent
            except SDISAgent.DoesNotExist:
                try:
                    request.user.agent.codisagent
                except CODISAgent.DoesNotExist:
                    try:
                        request.user.agent.cisagent
                    except CISAgent.DoesNotExist:
                        return render(request,
                                      'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
