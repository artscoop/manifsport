from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from agreements.models import GGDAgreement
from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Submit, Layout
from crispy_forms.bootstrap import FormActions

from .models import Agreement
from .models import CGAgreement
from .models import DDSPAgreement
from .models import EDSRAgreement
from .models import SDISAgreement


class AgreementForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = Agreement
        fields = ('favorable', 'prescriptions', 'attached_document')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(AgreementForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            'attached_document',
            FormActions(
                Submit('save', _('acknowledge agreement').capitalize())
            )
        )


class AgreementFormatForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = Agreement
        fields = ('favorable', 'prescriptions', 'attached_document')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(AgreementFormatForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            'attached_document',
            FormActions(
                Submit('save', _('format agreement').capitalize())
            )
        )


class DDSPAgreementDispatchForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = DDSPAgreement
        fields = ('concerned_commissariat', )

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(DDSPAgreementDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'concerned_commissariat',
            FormActions(
                Submit('save', _('send sub-agreements request').capitalize())
            )
        )


class EDSRAgreementDispatchForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = EDSRAgreement
        fields = ('concerned_cgd', )

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(EDSRAgreementDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'concerned_cgd',
            FormActions(
                Submit('save', _('send sub-agreements request').capitalize())
            )
        )


class GGDAgreementDispatchForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = GGDAgreement
        fields = ('concerned_cgd', 'concerned_edsr')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(GGDAgreementDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'concerned_cgd',
            'concerned_edsr',
            FormActions(
                Submit('save', _('send sub-agreements request').capitalize())
            )
        )


class SDISAgreementDispatchForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = SDISAgreement
        fields = ('concerned_company', )

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(SDISAgreementDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'concerned_company',
            FormActions(
                Submit('save', _('send sub-agreements request').capitalize())
            )
        )


class CGAgreementDispatchForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = CGAgreement
        fields = ('concerned_services', )

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(CGAgreementDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'concerned_services',
            FormActions(
                Submit('save', _('send sub-agreements request').capitalize())
            )
        )
