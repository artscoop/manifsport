# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('administrative_division', '0001_initial'),
        ('administration', '0001_initial'),
        ('authorizations', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agreement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_date', models.DateField(auto_now_add=True, verbose_name='request date')),
                ('reply_date', models.DateField(null=True, verbose_name='reply date', blank=True)),
                ('favorable', models.BooleanField(default=False, help_text='favorable help text', verbose_name='favorable')),
                ('prescriptions', models.TextField(verbose_name='prescriptions', blank=True)),
                ('state', django_fsm.FSMField(default='created', max_length=50)),
                ('attached_document', models.FileField(upload_to='avis/%Y/%m/%d', null=True, verbose_name='attached document', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CGAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('concerned_services', models.ManyToManyField(to='administration.CGService', verbose_name='concerned services')),
            ],
            options={
                'verbose_name': 'cg agreement',
                'verbose_name_plural': 'cg agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.CreateModel(
            name='DDSPAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('concerned_commissariat', models.ManyToManyField(to='administration.Commissariat', verbose_name='concerned commissariat')),
            ],
            options={
                'verbose_name': 'ddsp agreement',
                'verbose_name_plural': 'ddsp agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.CreateModel(
            name='EDSRAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('concerned_cgd', models.ManyToManyField(to='administration.GendarmerieCompany', verbose_name='concerned gendarmerie company')),
            ],
            options={
                'verbose_name': 'edsr agreement',
                'verbose_name_plural': 'edsr agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.CreateModel(
            name='FederalAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
            ],
            options={
                'verbose_name': 'federal agreement',
                'verbose_name_plural': 'federal agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.CreateModel(
            name='SDISAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('concerned_company', models.ManyToManyField(to='administration.Company', verbose_name='concerned company')),
            ],
            options={
                'verbose_name': 'sdis agreement',
                'verbose_name_plural': 'sdis agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.CreateModel(
            name='ServiceAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('service', models.ForeignKey(verbose_name='service', to='administration.Service')),
            ],
            options={
                'verbose_name': 'service agreement',
                'verbose_name_plural': 'service agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.CreateModel(
            name='TownHallAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('commune', models.ForeignKey(verbose_name='commune', to='administrative_division.Commune')),
            ],
            options={
                'verbose_name': 'townhall agreement',
                'verbose_name_plural': 'townhall agreements',
            },
            bases=('agreements.agreement',),
        ),
        migrations.AddField(
            model_name='agreement',
            name='authorization',
            field=models.ForeignKey(verbose_name='authorization', to='authorizations.EventAuthorization'),
            preserve_default=True,
        ),
    ]
