# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20160108_1334'),
        ('agreements', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GGDAgreement',
            fields=[
                ('agreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agreements.Agreement')),
                ('concerned_cgd', models.ManyToManyField(to='administration.GendarmerieCompany', verbose_name='concerned gendarmerie company')),
                ('concerned_edsr', models.ForeignKey(verbose_name='concerned road safety squadron', to='administration.RoadSafetySquadron')),
            ],
            options={
                'verbose_name': 'ggd agreement',
                'verbose_name_plural': 'ggd agreements',
            },
            bases=('agreements.agreement',),
        ),
    ]
