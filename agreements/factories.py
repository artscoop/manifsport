from __future__ import unicode_literals

import factory

from administration.factories import ServiceFactory

from administrative_division.factories import CommuneFactory
from agreements.models import GGDAgreement

from authorizations.factories import EventAuthorizationFactory

from .models import Agreement
from .models import FederalAgreement
from .models import ServiceAgreement
from .models import TownHallAgreement
from .models import EDSRAgreement
from .models import SDISAgreement
from .models import DDSPAgreement
from .models import CGAgreement


class AgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Agreement

    authorization = factory.SubFactory(EventAuthorizationFactory)


class FederalAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = FederalAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)


class ServiceAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = ServiceAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)
    service = factory.SubFactory(ServiceFactory)


class TownHallAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = TownHallAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)
    commune = factory.SubFactory(CommuneFactory)


class SDISAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = SDISAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)


class DDSPAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = DDSPAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)


class EDSRAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = EDSRAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)


class CGAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = CGAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)


class GGDAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = GGDAgreement

    authorization = factory.SubFactory(EventAuthorizationFactory)
