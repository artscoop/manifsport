from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django_fsm import FSMField, transition

from administrative_division.models import Commune
from administrative_division.models import CHOICES

from administration.models import Commissariat, RoadSafetySquadron
from administration.models import Company
from administration.models import GendarmerieCompany
from administration.models import Service
from administration.models import CGService

from authorizations.models import EventAuthorization
from authorizations.signals import authorization_published

from ddcs_loire.utils import add_notification_entry
from ddcs_loire.utils import add_log_entry


class AbstractBaseAgreement(models.Model):
    '''
    This model stores common data needed by Agreements and PreAgreements
    It is not intended to be instanced
    '''
    request_date = models.DateField(
        _('request date'),
        auto_now_add=True,
    )
    reply_date = models.DateField(
        _('reply date'),
        blank=True,
        null=True,
    )
    favorable = models.BooleanField(
        _('favorable'),
        default=False,
        help_text=_('favorable help text'),
    )
    prescriptions = models.TextField(_('prescriptions'), blank=True)
    state = FSMField(default='created')

    class Meta:
        abstract = True


class AgreementQuerySet(models.QuerySet):
    def to_process(self):
        return self.filter(authorization__event__end_date__gt=timezone.now())

    def acknowledged(self):
        return self.filter(state='acknowledged')

    def to_acknowledge(self):
        return self.filter(state__in=['formatted', 'acknowledged'])

    def to_dispatch(self):
        return self.filter(state__in=['acknowledged', 'dispatched'])


class Agreement(AbstractBaseAgreement):
    class Meta:
        ordering = ["authorization__event__begin_date"]

    authorization = models.ForeignKey(EventAuthorization,
                                      verbose_name=_('authorization'))
    attached_document = models.FileField(
        upload_to='avis/%Y/%m/%d',
        blank=True,
        null=True,
        verbose_name=_('attached document'),
        max_length=100,
    )
    objects = AgreementQuerySet.as_manager()

    def get_deadline(self):
        if hasattr(self, 'federalagreement'):
            delay = settings.FEDERAL_AGREEMENT_DELAY
        else:
            delay = settings.AGREEMENT_DELAY
        return self.request_date + delay

    def get_days_left_before_deadline(self):
        return self.get_deadline() - datetime.date.today()

    def delay_exceeded(self):
        deadline = self.get_deadline()
        return datetime.date.today() > deadline

    def sub_agreements_validated(self):
        validated = self.subagreement_set.filter(state='acknowledged').count()
        return validated == self.subagreement_set.all().count()

    def sub_agreements_issued(self):
        return self.subagreement_set.all().count()

    def sub_agreements_missing(self):
        return self.sub_agreements_issued() - self.subagreement_set.filter(state='acknowledged').count()

    def get_event(self):
        return self.authorization.event

    def log_ack(self, agents):
        add_log_entry(agents=agents,
                      action=_('agreement acknowledged'),
                      event=self.get_event())

    def log_dispatch(self, agents):
        add_log_entry(agents=agents,
                      action=_('subagreement requests sent'),
                      event=self.get_event())

    def log_format(self, agents):
        add_log_entry(agents=agents,
                      action=_('agreement formated'),
                      event=self.get_event())

    def log_resend(self, recipient):
        prefecture = self.authorization.get_concerned_prefecture()
        add_log_entry(agents=prefecture.instructor_set.all(),
                      action=_('agreement resent : ') + recipient,
                      event=self.get_event())

    def notify_creation(self, agents, institutional=None):
        try:
            prefecture = self.authorization.get_concerned_prefecture()
        except:
            pass
        else:
            add_notification_entry(agents=agents,
                                   event=self.get_event(),
                                   subject=_('agreement requested'),
                                   content_object=prefecture,
                                   institutional=institutional)

    def notify_ack(self, content_object):
        try:
            prefecture = self.authorization.get_concerned_prefecture()
        except:
            pass
        else:
            add_notification_entry(agents=prefecture.instructor_set.all(),
                                   event=self.get_event(),
                                   subject=_('agreement acknowledged'),
                                   content_object=content_object,
                                   institutional=prefecture)

    def notify_format(self, agents, content_object):
        add_notification_entry(agents=agents,
                               event=self.get_event(),
                               subject=_('agreement formated'),
                               content_object=content_object)

    def notify_publication(self, agents, institutional=None):
        prefecture = self.authorization.get_concerned_prefecture()
        add_notification_entry(agents=agents,
                               event=self.get_event(),
                               subject=_('bylaw published'),
                               content_object=prefecture,
                               institutional=institutional)


@python_2_unicode_compatible
class ServiceAgreement(Agreement):
    service = models.ForeignKey(Service, verbose_name=_('service'))
    objects = AgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('service agreement')
        verbose_name_plural = _('service agreements')

    def __str__(self):
        return ' - '.join([
            self.get_event().__str__().decode('utf8'),
            self.service.__str__().decode('utf8')
        ])

    def get_absolute_url(self):
        url = 'agreements:service_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_agents(self):
        return self.service.serviceagent_set.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(content_object=self.service)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=ServiceAgreement)
def notify_service_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(m2m_changed, sender=EventAuthorization.concerned_services.through)
def create_service_agreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            ServiceAgreement.objects.get_or_create(
                authorization=instance,
                service=Service.objects.get(pk=pk)
            )


@python_2_unicode_compatible
class TownHallAgreement(Agreement):
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )
    objects = AgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('townhall agreement')
        verbose_name_plural = _('townhall agreements')

    def __str__(self):
        return ' - '.join([
            self.get_event().__str__().decode('utf8'),
            self.commune.__str__().decode('utf8')
        ])

    def get_absolute_url(self):
        url = 'agreements:townhall_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_agents(self):
        return self.commune.townhallagent_set.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(content_object=self.commune)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=TownHallAgreement)
def notify_townhall_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(m2m_changed, sender=EventAuthorization.concerned_cities.through)
def create_townhall_agreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            TownHallAgreement.objects.get_or_create(
                authorization=instance,
                commune=Commune.objects.get(pk=pk)
            )


class FederalAgreementQuerySet(AgreementQuerySet):
    def for_federation(self, federation):
        return self.filter(authorization__event__activity__sport__federation=federation)


@python_2_unicode_compatible
class FederalAgreement(Agreement):
    objects = FederalAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('federal agreement')
        verbose_name_plural = _('federal agreements')

    def __str__(self):
        event = self.get_event()
        return ' - '.join([
            event.__str__().decode('utf8'),
            event.activity.sport.federation.__str__().decode('utf8')
        ])

    def get_absolute_url(self):
        url = 'agreements:federal_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_federation(self):
        return self.get_event().activity.sport.federation

    def get_agents(self):
        return self.get_federation().federationagent_set.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(content_object=self.get_federation())
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=FederalAgreement)
def notify_federal_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=EventAuthorization)
def create_federal_agreement(created, instance, **kwargs):
    if created:
        FederalAgreement.objects.create(authorization=instance)


@python_2_unicode_compatible
class EDSRAgreement(Agreement):
    concerned_cgd = models.ManyToManyField(
        GendarmerieCompany,
        verbose_name=_('concerned gendarmerie company'),
    )
    objects = AgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('edsr agreement')
        verbose_name_plural = _('edsr agreements')

    def __str__(self):
        event = self.get_event()
        departure_city = event.departure_city
        department = departure_city.arrondissement.department
        return ' - '.join([
            event.__str__().decode('utf8'),
            ' '.join(['EDSR', department.name]),
        ])

    def get_absolute_url(self):
        url = 'agreements:edsr_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        return self.get_event().departure_city.arrondissement.department.roadsafetysquadron

    def get_agents(self):
        return self.get_edsr().edsragent_set.all()

    def sub_agreements_validated(self):
        return super(EDSRAgreement, self).sub_agreements_validated()

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='formatted',
                conditions=[sub_agreements_validated])
    def format(self):
        department = self.get_event().departure_city.arrondissement.department
        self.notify_format(
            agents=department.departmentalgendarmeriegroup.ggdagent_set.all(),
            content_object=self.get_edsr(),
        )

        self.log_format(agents=self.get_agents())

    @transition(field='state', source='formatted', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        ggd = self.get_event().departure_city.arrondissement.department.departmentalgendarmeriegroup
        self.notify_ack(content_object=ggd)

        self.log_ack(agents=ggd.ggdagent_set.all())


@receiver(post_save, sender=EDSRAgreement)
def notify_edsr_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=EventAuthorization)
def create_edsr_agreement(created, instance, **kwargs):
    if instance.edsr_concerned:
        EDSRAgreement.objects.get_or_create(authorization=instance)


@python_2_unicode_compatible
class GGDAgreement(Agreement):
    concerned_cgd = models.ManyToManyField(
        GendarmerieCompany,
        verbose_name=_('concerned gendarmerie company'),
    )
    concerned_edsr = models.ForeignKey(
        RoadSafetySquadron,
        verbose_name=_('concerned road safety squadron')
    )
    objects = AgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('ggd agreement')
        verbose_name_plural = _('ggd agreements')

    def __str__(self):
        event = self.get_event()
        departure_city = event.departure_city
        department = departure_city.arrondissement.department
        return ' - '.join([
            event.__str__().decode('utf8'),
            ' '.join(['EDSR', department.name]),
        ])

    def get_absolute_url(self):
        url = 'agreements:ggd_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        return self.get_event().departure_city.arrondissement.department.roadsafetysquadron

    def get_ggd(self):
        return self.get_event().departure_city.arrondissement.department.departmentalgendarmeriegroup

    def get_agents(self):
        return self.get_event().departure_city.arrondissement.department.departmentalgendarmeriegroup.ggdagent_set.all()

    def sub_agreements_validated(self):
        return super(GGDAgreement, self).sub_agreements_validated()

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='formatted', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        ggd = self.get_event().departure_city.arrondissement.department.departmentalgendarmeriegroup
        self.notify_ack(content_object=ggd)

        self.log_ack(agents=ggd.ggdagent_set.all())


@receiver(post_save, sender=GGDAgreement)
def notify_ggd_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=EventAuthorization)
def create_ggd_agreement(created, instance, **kwargs):
    if instance.ggd_concerned:
        department = instance.event.departure_city.arrondissement.department
        edsr = department.roadsafetysquadron
        GGDAgreement.objects.get_or_create(authorization=instance, concerned_edsr=edsr)


class SDISAgreementQuerySet(AgreementQuerySet):
    def for_fire_service(self, fire_service):
        return self.filter(subagreement__companysubagreement__concerned_cis=fire_service)


@python_2_unicode_compatible
class SDISAgreement(Agreement):
    concerned_company = models.ManyToManyField(
        Company,
        verbose_name=_('concerned company'),
    )
    objects = SDISAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('sdis agreement')
        verbose_name_plural = _('sdis agreements')

    def __str__(self):
        event = self.get_event()
        department = event.departure_city.arrondissement.department
        return ' - '.join([
            event.__str__().decode('utf8'),
            ' '.join(['SDIS', department.name]),
        ])

    def get_absolute_url(self):
        url = 'agreements:sdis_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_sdis(self):
        return self.get_event().departure_city.arrondissement.department.fireandrescueservice

    def get_agents(self):
        return self.get_sdis().sdisagent_set.all()

    def sub_agreements_validated(self):
        return super(SDISAgreement, self).sub_agreements_validated()

    def notify_ack(self, content_object):
        super(SDISAgreement, self).notify_ack(content_object)
        codis = self.get_event().departure_city.arrondissement.department.departmentalfireopcenter.codisagent_set.all()
        # notification to codis agents
        add_notification_entry(
            agents=codis,
            event=self.get_event(),
            subject=_('agreement acknowledged'),
            content_object=content_object,
        )
        # notification to company agents
        add_notification_entry(
            agents=[agent for company in self.concerned_company.all() for agent in company.companyagent_set.all()],
            event=self.get_event(),
            subject=_('agreement acknowledged'),
            content_object=content_object,
        )

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='acknowledged',
                conditions=[sub_agreements_validated])
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(content_object=self.get_sdis())
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=SDISAgreement)
def notify_sdis_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=EventAuthorization)
def create_sdis_agreement(created, instance, **kwargs):
    if instance.sdis_concerned:
        SDISAgreement.objects.get_or_create(authorization=instance)


@python_2_unicode_compatible
class DDSPAgreement(Agreement):
    concerned_commissariat = models.ManyToManyField(
        Commissariat,
        verbose_name=_('concerned commissariat'),
    )
    objects = AgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('ddsp agreement')
        verbose_name_plural = _('ddsp agreements')

    def __str__(self):
        event = self.get_event()
        departure_city = event.departure_city
        department = departure_city.arrondissement.department
        return ' - '.join([
            event.__str__().decode('utf8'),
            ' '.join(['DDSP', department.name]),
        ])

    def get_absolute_url(self):
        url = 'agreements:ddsp_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_agents(self):
        arrondissement = self.get_event().departure_city.arrondissement
        return arrondissement.department.publicsafetydirection.ddspagent_set.all()

    def get_ddsp(self):
        arrondissement = self.get_event().departure_city.arrondissement
        return arrondissement.department.publicsafetydirection

    def sub_agreements_validated(self):
        return super(DDSPAgreement, self).sub_agreements_validated()

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='acknowledged',
                conditions=[sub_agreements_validated])
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(content_object=self.get_ddsp())
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=DDSPAgreement)
def notify_ddsp_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 institutional=instance.get_ddsp())


@receiver(post_save, sender=EventAuthorization)
def create_ddsp_agreement(created, instance, **kwargs):
    if instance.ddsp_concerned:
        DDSPAgreement.objects.get_or_create(authorization=instance)


@python_2_unicode_compatible
class CGAgreement(Agreement):
    concerned_services = models.ManyToManyField(
        CGService,
        verbose_name=_('concerned services'),
    )
    objects = AgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('cg agreement')
        verbose_name_plural = _('cg agreements')

    def __str__(self):
        event = self.get_event()
        departure_city = event.departure_city
        department = departure_city.arrondissement.department
        return ' - '.join([
            event.__str__().decode('utf8'),
            ' '.join(['CG', department.name]),
        ])

    def get_absolute_url(self):
        url = 'agreements:cg_agreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_general_council(self):
        arrondissement = self.get_event().departure_city.arrondissement
        return arrondissement.department.generalcouncil

    def get_agents(self):
        return self.get_general_council().cgagent_set.all()

    def sub_agreements_validated(self):
        return super(CGAgreement, self).sub_agreements_validated()

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='formatted',
                conditions=[sub_agreements_validated])
    def format(self):
        general_council = self.get_general_council()

        self.notify_format(agents=general_council.cgsuperior_set.all(),
                           content_object=general_council)
        self.log_format(agents=self.get_agents())

    @transition(field='state', source='formatted', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()
        general_council = self.get_general_council()

        self.notify_ack(content_object=general_council)
        self.log_ack(agents=general_council.cgsuperior_set.all())


@receiver(post_save, sender=CGAgreement)
def notify_cg_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 institutional=instance.get_general_council())


@receiver(post_save, sender=EventAuthorization)
def create_cg_agreement(created, instance, **kwargs):
    if instance.cg_concerned:
        CGAgreement.objects.get_or_create(authorization=instance)


@receiver(authorization_published, sender=EventAuthorization)
def notify_authorization_publication(instance, **kwargs):
    for agreement in ServiceAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents())
    for agreement in TownHallAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents())
    for agreement in FederalAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents())
    for agreement in EDSRAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents())
    for agreement in SDISAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents())
    for agreement in DDSPAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents(),
                                     institutional=agreement.get_ddsp())
    for agreement in CGAgreement.objects.filter(authorization=instance):
        agreement.notify_publication(agents=agreement.get_agents(),
                                     institutional=agreement.get_general_council())
