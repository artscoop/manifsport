from __future__ import unicode_literals

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView

from administration.models import FederationAgent
from administration.models import ServiceAgent
from administration.models import TownHallAgent
from administration.models import CGAgent
from administration.models import CGSuperior
from administration.models import GGDAgent
from administration.models import EDSRAgent
from administration.models import BrigadeAgent
from administration.models import DDSPAgent
from administration.models import SDISAgent
from administration.models import CODISAgent
from administration.models import CISAgent
from agreements.forms import GGDAgreementDispatchForm
from agreements.models import GGDAgreement

from authorizations.decorators import instructor_required

from events.models import News

from .decorators import agent_required
from .decorators import cg_required
from .decorators import cg_agent_required
from .decorators import cg_superior_required
from .decorators import ddsp_agent_required
from .decorators import edsr_agent_required
from .decorators import federal_agent_required
from .decorators import gendarmerie_agent_required
from .decorators import ggd_agent_required
from .decorators import sdis_agent_required
from .decorators import fireman_required
from .decorators import service_agent_required
from .decorators import townhall_agent_required

from .forms import AgreementForm
from .forms import AgreementFormatForm
from .forms import EDSRAgreementDispatchForm
from .forms import SDISAgreementDispatchForm
from .forms import DDSPAgreementDispatchForm
from .forms import CGAgreementDispatchForm

from .models import Agreement
from .models import FederalAgreement
from .models import ServiceAgreement
from .models import CGAgreement
from .models import EDSRAgreement
from .models import DDSPAgreement
from .models import SDISAgreement
from .models import TownHallAgreement


class Dashboard(ListView):  # pylint: disable=R0901
    model = Agreement

    @method_decorator(agent_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        agent = self.request.user.agent
        try:
            agent.federationagent
        except FederationAgent.DoesNotExist:
            try:
                agent.serviceagent
            except ServiceAgent.DoesNotExist:
                try:
                    agent.townhallagent
                except TownHallAgent.DoesNotExist:
                    try:
                        agent.edsragent
                    except EDSRAgent.DoesNotExist:
                        try:
                            agent.sdisagent
                        except SDISAgent.DoesNotExist:
                            try:
                                agent.ddspagent
                            except DDSPAgent.DoesNotExist:
                                try:
                                    agent.ggdagent
                                except GGDAgent.DoesNotExist:
                                    try:
                                        agent.cgagent
                                    except CGAgent.DoesNotExist:
                                        try:
                                            agent.cgsuperior
                                        except CGSuperior.DoesNotExist:
                                            try:
                                                agent.codisagent
                                            except CODISAgent.DoesNotExist:
                                                try:
                                                    agent.cisagent
                                                except CISAgent.DoesNotExist:
                                                    try:
                                                        agent.brigadeagent
                                                    except BrigadeAgent.DoesNotExist:  # noqa
                                                        pass
                                                    return EDSRAgreement.objects.to_process().to_dispatch()  # noqa
                                                fire_service = agent.cisagent.fire_service  # noqa
                                                return SDISAgreement.objects.to_process().for_fire_service(fire_service).acknowledged()  # noqa
                                            return SDISAgreement.objects.to_process().acknowledged()  # noqa
                                        return CGAgreement.objects.to_process().to_acknowledge()  # noqa
                                    return CGAgreement.objects.to_process()
                                return GGDAgreement.objects.to_process()  # noqa
                            return DDSPAgreement.objects.to_process()
                        return SDISAgreement.objects.to_process()
                    return EDSRAgreement.objects.to_process()
                commune = agent.townhallagent.commune
                return TownHallAgreement.objects.to_process().filter(commune=commune)  # noqa
            service = agent.serviceagent.service
            return ServiceAgreement.objects.to_process().filter(service=service)  # noqa
        federation = agent.federationagent.federation
        return FederalAgreement.objects.to_process().for_federation(federation)

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['last_news'] = News.objects.last()
        return context


class BaseAcknowledgeView(UpdateView):  # pylint: disable=R0901
    model = Agreement
    form_class = AgreementForm
    template_name_suffix = '_ack_form'

    def form_valid(self, form):
        if form.instance.state == 'acknowledged':
            info_message = _('This agreement has allready been acknwoledged.'
                             ' There is no need to do it again')
            messages.error(self.request, info_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.acknowledge()
            return super(BaseAcknowledgeView, self).form_valid(form)


class BaseFormatView(UpdateView):  # pylint: disable=R0901
    model = Agreement
    form_class = AgreementFormatForm
    template_name_suffix = '_format_form'

    def form_valid(self, form):
        if form.instance.state == 'formatted':
            info_message = _('This agreement has allready been formatted.'
                             ' There is no need to do it again')
            messages.error(self.request, info_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.format()
            return super(BaseFormatView, self).form_valid(form)


class BaseDispatchView(UpdateView):  # pylint: disable=R0901
    template_name_suffix = '_dispatch_form'

    def form_valid(self, form):
        if form.instance.state == 'dispatched':
            info_message = _('This agreement has allready been dispatched.'
                             ' There is no need to do it again')
            messages.error(self.request, info_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.dispatch()
            return super(BaseDispatchView, self).form_valid(form)


class BaseResendView(SingleObjectMixin, View):
    model = Agreement

    @method_decorator(instructor_required)
    def dispatch(self, *args, **kwargs):
        return super(BaseResendView, self).dispatch(*args, **kwargs)


###########################################################
# Service Agreements Views
###########################################################
class ServiceAgreementDetail(DetailView):  # pylint: disable=R0901
    model = ServiceAgreement

    @method_decorator(service_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(ServiceAgreementDetail, self).dispatch(*args, **kwargs)


class ServiceAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = ServiceAgreement

    @method_decorator(service_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(ServiceAgreementAcknowledge, self).dispatch(*args,
                                                                 **kwargs)


class ServiceAgreementResend(BaseResendView):
    model = ServiceAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.service.serviceagent_set.all())
        agreement.log_resend(recipient=agreement.service.__str__().decode('utf8'))
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# Federal Agreements Views
###########################################################
class FederalAgreementDetail(DetailView):  # pylint: disable=R0901
    model = FederalAgreement

    @method_decorator(federal_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(FederalAgreementDetail, self).dispatch(*args, **kwargs)


class FederalAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = FederalAgreement

    @method_decorator(federal_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(FederalAgreementAcknowledge, self).dispatch(*args,
                                                                 **kwargs)


class FederalAgreementResend(BaseResendView):
    model = FederalAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.authorization.event.activity.sport.federation.federationagent_set.all())
        agreement.log_resend(recipient=agreement.get_federation().__str__().decode('utf8'))
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# TownHall Agreements Views
###########################################################
class TownHallAgreementDetail(DetailView):  # pylint: disable=R0901
    model = TownHallAgreement

    @method_decorator(townhall_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(TownHallAgreementDetail, self).dispatch(*args, **kwargs)


class TownHallAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = TownHallAgreement

    @method_decorator(townhall_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(TownHallAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)


class TownHallAgreementResend(BaseResendView):
    model = TownHallAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.commune.townhallagent_set.all())
        agreement.log_resend(recipient=agreement.commune.__str__().decode('utf8'))
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# EDSR Agreements Views
###########################################################
class EDSRAgreementDetail(DetailView):  # pylint: disable=R0901
    model = EDSRAgreement

    @method_decorator(gendarmerie_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementDetail, self).dispatch(*args, **kwargs)


class EDSRAgreementDispatch(BaseDispatchView):  # pylint: disable=R0901
    model = EDSRAgreement
    form_class = EDSRAgreementDispatchForm

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementDispatch, self).dispatch(*args, **kwargs)


class EDSRAgreementFormat(BaseFormatView):  # pylint: disable=R0901
    model = EDSRAgreement

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementFormat, self).dispatch(*args, **kwargs)


class EDSRAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = EDSRAgreement

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementAcknowledge, self).dispatch(*args, **kwargs)


class EDSRAgreementResend(BaseResendView):
    model = EDSRAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.authorization.event.departure_city.arrondissement.department.roadsafetysquadron.edsragent_set.all())
        agreement.log_resend(recipient=agreement.get_edsr().__str__())
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# GGD Agreements Views
###########################################################
class GGDAgreementDetail(DetailView):  # pylint: disable=R0901
    model = GGDAgreement

    @method_decorator(gendarmerie_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementDetail, self).dispatch(*args, **kwargs)


class GGDAgreementDispatch(BaseDispatchView):  # pylint: disable=R0901
    model = GGDAgreement
    form_class = GGDAgreementDispatchForm

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementDispatch, self).dispatch(*args, **kwargs)


class GGDAgreementFormat(BaseFormatView):  # pylint: disable=R0901
    model = GGDAgreement

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementFormat, self).dispatch(*args, **kwargs)


class GGDAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = GGDAgreement

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementAcknowledge, self).dispatch(*args, **kwargs)


class GGDAgreementResend(BaseResendView):
    model = GGDAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.authorization.event.departure_city.arrondissement.department.departmentalgendarmeriegroup.ggdagent_set.all())
        agreement.log_resend(recipient=agreement.get_ggd().__str__())
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# SDIS Agreements Views
###########################################################
class SDISAgreementDetail(DetailView):  # pylint: disable=R0901
    model = SDISAgreement

    @method_decorator(fireman_required)
    def dispatch(self, *args, **kwargs):
        return super(SDISAgreementDetail, self).dispatch(*args, **kwargs)


class SDISAgreementDispatch(BaseDispatchView):  # pylint: disable=R0901
    model = SDISAgreement
    form_class = SDISAgreementDispatchForm

    @method_decorator(sdis_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(SDISAgreementDispatch, self).dispatch(*args, **kwargs)


class SDISAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = SDISAgreement

    @method_decorator(sdis_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(SDISAgreementAcknowledge, self).dispatch(*args, **kwargs)


class SDISAgreementResend(BaseResendView):
    model = SDISAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.authorization.event.departure_city.arrondissement.department.fireandrescueservice.sdisagent_set.all())
        agreement.log_resend(recipient=agreement.get_sdis().__str__())
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# DDSP Agreements Views
###########################################################
class DDSPAgreementDetail(DetailView):  # pylint: disable=R0901
    model = DDSPAgreement

    @method_decorator(ddsp_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(DDSPAgreementDetail, self).dispatch(*args, **kwargs)


class DDSPAgreementDispatch(BaseDispatchView):  # pylint: disable=R0901
    model = DDSPAgreement
    form_class = DDSPAgreementDispatchForm

    @method_decorator(ddsp_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(DDSPAgreementDispatch, self).dispatch(*args, **kwargs)


class DDSPAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = DDSPAgreement

    @method_decorator(ddsp_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(DDSPAgreementAcknowledge, self).dispatch(*args, **kwargs)


class DDSPAgreementResend(BaseResendView):
    model = DDSPAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.authorization.event.departure_city.arrondissement.department.publicsafetydirection.ddspagent_set.all())
        agreement.log_resend(recipient=agreement.get_ddsp().__str__())
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))


###########################################################
# CG Agreements Views
###########################################################
class CGAgreementDetail(DetailView):  # pylint: disable=R0901
    model = CGAgreement

    @method_decorator(cg_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementDetail, self).dispatch(*args, **kwargs)


class CGAgreementDispatch(BaseDispatchView):  # pylint: disable=R0901
    model = CGAgreement
    form_class = CGAgreementDispatchForm

    @method_decorator(cg_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementDispatch, self).dispatch(*args, **kwargs)


class CGAgreementFormat(BaseFormatView):  # pylint: disable=R0901
    model = CGAgreement

    @method_decorator(cg_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementFormat, self).dispatch(*args, **kwargs)


class CGAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = CGAgreement

    @method_decorator(cg_superior_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementAcknowledge, self).dispatch(*args, **kwargs)


class CGAgreementResend(BaseResendView):
    model = CGAgreement

    def get(self, request, *args, **kwargs):
        agreement = self.get_object()
        agreement.notify_creation(agents=agreement.authorization.event.departure_city.arrondissement.department.generalcouncil.cgagent_set.all())
        agreement.log_resend(recipient=agreement.get_general_council().__str__())
        messages.success(request, _('Agreement request sent successfully'))
        return redirect('/authorizations/{id}'.format(id=agreement.authorization.id))
