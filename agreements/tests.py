# coding: utf-8
from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.translation import ugettext_lazy as _

from administration.factories import InstructorFactory, DepartmentalGendarmerieGroupFactory
from administration.factories import FederationAgentFactory
from administration.factories import PrefectureFactory
from administration.factories import ServiceFactory
from administration.factories import ServiceAgentFactory
from administration.factories import TownHallAgentFactory
from administration.factories import GeneralCouncilFactory
from administration.factories import RoadSafetySquadronFactory
from administration.factories import FireAndRescueServiceFactory
from administration.factories import PublicSafetyDirectionFactory

from administrative_division.factories import CommuneFactory
from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import DepartmentFactory
from agreements.factories import GGDAgreementFactory

from authorizations.factories import EventAuthorizationFactory

from events.factories import AuthorizedNonMotorizedEventFactory

from sports.factories import ActivityFactory

from .factories import AgreementFactory
from .factories import FederalAgreementFactory
from .factories import ServiceAgreementFactory
from .factories import TownHallAgreementFactory
from .factories import SDISAgreementFactory
from .factories import DDSPAgreementFactory
from .factories import EDSRAgreementFactory
from .factories import CGAgreementFactory


class AgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_deadline(self):
        agreement = AgreementFactory.create()
        self.assertEqual(agreement.get_deadline(),
                         datetime.date.today() + settings.AGREEMENT_DELAY)

    def test_delay_not_exceeded(self):
        agreement = AgreementFactory.create()
        self.assertEqual(agreement.delay_exceeded(), False)

    def test_delay_exceeded(self):
        agreement = AgreementFactory.create()
        agreement.request_date = datetime.date.today() - settings.AGREEMENT_DELAY - datetime.timedelta(days=2)
        self.assertEqual(agreement.delay_exceeded(), True)

    def test_sub_agreements_validated(self):
        agreement = AgreementFactory.create()
        self.assertTrue(agreement.sub_agreements_validated())


class FederalAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        agreement = FederalAgreementFactory.create()
        event = agreement.get_event()
        self.assertEqual(
            agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                event.activity.sport.federation.__str__().decode('utf8')
            ])
        )

    def test_get_absolute_url(self):
        agreement = FederalAgreementFactory.create()
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:federal_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_get_deadline(self):
        agreement = FederalAgreementFactory.create()
        self.assertEqual(agreement.get_deadline(),
                         datetime.date.today() + settings.FEDERAL_AGREEMENT_DELAY)

    def test_creation(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.agreement_set.all().count(), 1)
        authorization.save()
        self.assertEqual(authorization.agreement_set.all().count(), 1)

    def test_life_cycle(self):
        activity = ActivityFactory.create()
        _federal_agent = FederationAgentFactory.create(
            federation=activity.sport.federation
        )
        city = CommuneFactory.create()
        prefecture = PrefectureFactory.create(
            arrondissement=city.arrondissement,
        )
        _instructor = InstructorFactory(prefecture=prefecture)
        event = AuthorizedNonMotorizedEventFactory.create(activity=activity,
                                                          departure_city=city)
        authorization = EventAuthorizationFactory.create(
            event=event
        )
        for agent in event.activity.sport.federation.federationagent_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
        authorization.agreement_set.first().federalagreement.save()
        for agent in event.activity.sport.federation.federationagent_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
        # test dispatch
        f_agreement = authorization.agreement_set.first().federalagreement
        f_agreement.acknowledge()
        f_agreement.save()
        self.assertEqual(f_agreement.reply_date, datetime.date.today())
        # test federal agent action created
        for agent in event.activity.sport.federation.federationagent_set.all():
            self.assertEqual(agent.user.action_set.all().count(), 1)
            self.assertEqual(agent.user.action_set.first().action,
                             _('agreement acknowledged'))
        # test instructor notification created
        for agent in prefecture.instructor_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 2)
            self.assertEqual(
                agent.user.notification_set.last().content_object,
                activity.sport.federation,
            )


class ServiceAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        agreement = ServiceAgreementFactory.create()
        event = agreement.get_event()
        self.assertEqual(
            agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                agreement.service.__str__().decode('utf8')
            ])
        )

    def test_get_absolute_url(self):
        agreement = ServiceAgreementFactory.create()
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:service_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_creation(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.agreement_set.all().count(), 1)
        service = ServiceFactory.create()
        authorization.concerned_services.add(service)
        self.assertEqual(authorization.agreement_set.all().count(), 2)

    def test_life_cycle(self):
        # test notify after creation
        service = ServiceFactory.create()
        _service_agent = ServiceAgentFactory.create(service=service)
        authorization = EventAuthorizationFactory.create()
        city = authorization.event.departure_city
        prefecture = PrefectureFactory.create(
            arrondissement=city.arrondissement,
        )
        _instructor = InstructorFactory(prefecture=prefecture)
        agreement = ServiceAgreementFactory.create(service=service,
                                                   authorization=authorization)
        for agent in agreement.service.serviceagent_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
        agreement.save()
        for agent in agreement.service.serviceagent_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
        # test dispatch
        agreement.acknowledge()
        self.assertEqual(agreement.reply_date, datetime.date.today())
        # test service agent action created
        for agent in agreement.service.serviceagent_set.all():
            self.assertEqual(agent.user.action_set.all().count(), 1)
            self.assertEqual(agent.user.action_set.first().action,
                             _('agreement acknowledged'))
        # test instructor notification created
        for agent in prefecture.instructor_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
            self.assertEqual(
                agent.user.notification_set.first().content_object,
                agreement.service,
            )


class TownHallAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        agreement = TownHallAgreementFactory.create()
        event = agreement.get_event()
        self.assertEqual(
            agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                agreement.commune.__str__().decode('utf8')
            ])
        )

    def test_get_absolute_url(self):
        agreement = TownHallAgreementFactory.create()
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:townhall_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_creation(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.agreement_set.all().count(), 1)
        authorization.concerned_cities.add(
            CommuneFactory.create()
        )
        self.assertEqual(authorization.agreement_set.all().count(), 2)

    def test_life_cycle(self):
        # test notify after creation
        commune = CommuneFactory.create()
        _townhall_agent = TownHallAgentFactory.create(commune=commune)
        authorization = EventAuthorizationFactory.create()
        city = authorization.event.departure_city
        prefecture = PrefectureFactory.create(
            arrondissement=city.arrondissement,
        )
        _instructor = InstructorFactory(prefecture=prefecture)
        agreement = TownHallAgreementFactory.create(
            commune=commune,
            authorization=authorization,
        )
        for agent in agreement.commune.townhallagent_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
        agreement.save()
        for agent in agreement.commune.townhallagent_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
        # test dispatch
        agreement.acknowledge()
        self.assertEqual(agreement.reply_date, datetime.date.today())
        # test townhall agent action created
        for agent in agreement.commune.townhallagent_set.all():
            self.assertEqual(agent.user.action_set.all().count(), 1)
            self.assertEqual(agent.user.action_set.first().action,
                             _('agreement acknowledged'))
        # test instructor notification created
        for agent in prefecture.instructor_set.all():
            self.assertEqual(agent.user.notification_set.all().count(), 1)
            self.assertEqual(
                agent.user.notification_set.first().content_object,
                agreement.commune,
            )


def create_tree():
    department = DepartmentFactory.create()
    _sdis = FireAndRescueServiceFactory.create(department=department)
    _edsr = RoadSafetySquadronFactory.create(department=department)
    _ggd = DepartmentalGendarmerieGroupFactory(department=department)
    _ddsp = PublicSafetyDirectionFactory.create(department=department)
    _council = GeneralCouncilFactory.create(department=department)
    arrondissement = ArrondissementFactory.create(department=department)
    city = CommuneFactory.create(arrondissement=arrondissement)
    event = AuthorizedNonMotorizedEventFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(event=event)
    return authorization, department


class SDISAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        agreement = SDISAgreementFactory.create(
            authorization = authorization
        )
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:sdis_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_sub_agreements_validated(self):
        authorization, _ = create_tree()
        agreement = SDISAgreementFactory.create(
            authorization=authorization
        )
        self.assertTrue(agreement.sub_agreements_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.sdis_concerned = True
        authorization.save()
        self.assertEqual(authorization.agreement_set.all().count(), 2)


class EDSRAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        agreement = EDSRAgreementFactory.create(
            authorization=authorization
        )
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:edsr_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_sub_agreements_validated(self):
        authorization, _ = create_tree()
        agreement = EDSRAgreementFactory.create(
            authorization=authorization
        )
        self.assertTrue(agreement.sub_agreements_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.edsr_concerned = True
        authorization.save()
        self.assertEqual(authorization.agreement_set.all().count(), 2)


class GGDAgreementMethodTests(TestCase):  # pylint: disable=R0904
    """
    Tests des avis GGD (workflow 7)
    """

    def test_get_absolute_url(self):
        authorization, department = create_tree()
        agreement = GGDAgreementFactory.create(
            authorization=authorization,
            concerned_edsr=department.roadsafetysquadron
        )
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:ggd_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_sub_agreements_validated(self):
        authorization, department = create_tree()
        agreement = GGDAgreementFactory.create(
            authorization=authorization, concerned_edsr=department.roadsafetysquadron
        )
        # Les pré-avis ne sont pas tous validés pour une bonne raison :
        # les pré-avis CGD ne sont pas créés par défaut (pas de CGD ajouté)
        # mais un pré-avis EDSR l'est avec l'état created (!= acknowledged)
        self.assertFalse(agreement.sub_agreements_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.ggd_concerned = True
        authorization.save()
        self.assertEqual(authorization.agreement_set.all().count(), 2)


class DDSPAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        agreement = DDSPAgreementFactory.create(
            authorization=authorization,
        )
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:ddsp_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_sub_agreements_validated(self):
        authorization, _ = create_tree()
        agreement = DDSPAgreementFactory.create(
            authorization=authorization,
        )
        self.assertTrue(agreement.sub_agreements_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.ddsp_concerned = True
        authorization.save()
        self.assertEqual(authorization.agreement_set.all().count(), 2)


class CGAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        agreement = CGAgreementFactory.create(
            authorization=authorization
        )
        self.assertEqual(agreement.get_absolute_url(),
                         reverse('agreements:cg_agreement_detail',
                                 kwargs={'pk': agreement.pk}))

    def test_sub_agreements_validated(self):
        authorization, _ = create_tree()
        agreement = CGAgreementFactory.create(
            authorization=authorization,
        )
        self.assertTrue(agreement.sub_agreements_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.cg_concerned = True
        authorization.save()
        self.assertEqual(authorization.agreement_set.all().count(), 2)
