from __future__ import unicode_literals

from django.contrib import admin

from sub_agreements.admin import CGDSubAgreementInline
from sub_agreements.admin import CompanySubAgreementInline
from sub_agreements.admin import CommissariatSubAgreementInline
from sub_agreements.admin import CGServiceSubAgreementInline

from .models import FederalAgreement
from .models import ServiceAgreement
from .models import TownHallAgreement
from .models import EDSRAgreement
from .models import SDISAgreement
from .models import DDSPAgreement
from .models import CGAgreement
from .models import GGDAgreement


class CGAgreementAdmin(admin.ModelAdmin):
    inlines = [CGServiceSubAgreementInline, ]
    search_fields = ['authorization__event__name']


class DDSPAgreementAdmin(admin.ModelAdmin):
    inlines = [CommissariatSubAgreementInline, ]
    search_fields = ['authorization__event__name']


class SDISAgreementAdmin(admin.ModelAdmin):
    inlines = [CompanySubAgreementInline, ]
    search_fields = ['authorization__event__name']


class EDSRAgreementAdmin(admin.ModelAdmin):
    inlines = [CGDSubAgreementInline, ]
    search_fields = ['authorization__event__name']


class GGDAgreementAdmin(admin.ModelAdmin):
    inlines = [CGDSubAgreementInline, ]
    search_fields = ['authorization__event__name']


admin.site.register(FederalAgreement)
admin.site.register(ServiceAgreement)
admin.site.register(TownHallAgreement)
admin.site.register(EDSRAgreement, EDSRAgreementAdmin)
admin.site.register(DDSPAgreement, DDSPAgreementAdmin)
admin.site.register(SDISAgreement, SDISAgreementAdmin)
admin.site.register(CGAgreement, CGAgreementAdmin)
admin.site.register(GGDAgreement, GGDAgreementAdmin)
