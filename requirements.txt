Django<1.8
Pillow
django-allauth==0.18.0
django-braces==1.4.0
django-crispy-forms==1.4.0
#django-ckeditor==4.4.7
#-e https://github.com/Openscop/django-ckeditor.git@master#egg=django-ckeditor
-e git+git://github.com/Openscop/django-ckeditor.git@master#egg=django-ckeditor
django-extensions==1.3.11
django-extra-views==0.7.1
django-fsm==2.2.0
django-localflavor==1.0
django-model-utils==2.2
psycopg2
pytz
PyYAML==3.10
raven==5.0.0
requests
-e git://github.com/Openscop/django-bootstrap3-datetimepicker.git@a5caba08ef563114dd1ee55090ab68f571d2c7a6#egg=django_bootstrap3_datetimepicker-master
