manifestationsportive.fr
========================

Mettre en place son environnement de développement
--------------------------------------------------

### Cloner le dépôt git

```
#!

git clone git@bitbucket.org:openscop/d-mat.-manif.-sport.-v2.git
git pull
```


### Installer les pacquets python de base

```
#!

sudo aptitude install python python-dev build-essential libpq-dev
```


### Installer pip

```
#!

sudo aptitude install python-pip
```


### Installer virtualenv
	
```
#!

sudo pip install virtualenv
```


### Se placer à la racine du projet

```
#!

cd d-mat.-manif.-sport.-v2/
```


### Créer un virtualenv spécialement pour le projet

Cet environnement virtuel aura pour utilité d'isoler les pacquest python de votre distribution linux des pacquets python utiles au projet
	
```
#!

virtualenv env
source env/bin/activate
```

Cette dernière commande sera utile (et donc vous devrez l'executer) à chaque fois que l'on voudra travailler sur le projet

### Installer les dépendances requises par le projet
dans le fichier requirements.txt se trouvent les dépendances nécessaires à la plateforme de production
et dans requirements-dev.txt sont ajoutées celles nécessaires au développement ainsi qu'aux tests unitaires

```
#!

pip install -r requirements.txt
pip install -r requirements-dev.txt
```


### Création des fichiers de configuration à l'aide des patrons fournis

```
#!

cp ddcs_loire/settings_openrunner.sample.py ddcs_loire/settings_openrunner.py
cp ddcs_loire/settings.sample.py ddcs_loire/settings.py

```

Dans ce dernier fichier, modifier STATIC_ROOT et MEDIA_ROOT de la manière suivante : 

        
```
#!python

STATIC_ROOT = /home/<votre_login>
MEDIA_ROOT = /<path_du_projet>/media/
```


### Effectuer la migration initiale de la base de données qui va créer la base en elle même ainsi que toutes les tables nécessaires au projet. Et lancer le serveur de développement

```
#!

python manage.py migrate
python manage.py runserver_plus
```


### Télécharger les données de production

Cette étape nécessite d'avoir les bons login et mot de passe de la plateforme de production

```
#!

cp fabfile.sample.py fabfile.py
```


Dans le fichier fabfile.py, modifier la variable OWNER avec le login du serveur de production ainsi que les variables env.hostname, env.db_name, env.db_user, env.db_password de la fonction "def production()"

Lancer le script de la manière suivante : 

```
#!

fab production dump_and_restore
```


### Se créer un compte administrateur

```
#!

python manage.py createsuperuser
```


L'application devrait être accessible à l'adresse suivante : http://localhost:8000 et http://localhost:8000/admin pour l'interface d'administration