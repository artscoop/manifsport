from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from administrative_division.models import Commune
from administrative_division.models import CHOICES


@python_2_unicode_compatible
class Contact(models.Model):
    first_name = models.CharField(_('first name'), max_length=200)
    last_name = models.CharField(_('last name'), max_length=200)
    phone = models.CharField(_('phone number'), max_length=14)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:  # pylint: disable=C1001
        verbose_name = _('contact')

    def __str__(self):
        return ' '.join([self.first_name.capitalize(),
                         self.last_name.capitalize()])


@python_2_unicode_compatible
class Address(models.Model):
    address = models.CharField(_('address'), max_length=255)
    commune = models.ForeignKey(
        Commune,
        verbose_name=_('commune'),
        limit_choices_to=CHOICES,
    )
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:  # pylint: disable=C1001
        verbose_name = _('address')

    def __str__(self):
        return ' - '.join([
            self.address,
            ' '.join([self.commune.zip_code,  # pylint: disable=E1101
                      self.commune.name.upper()])
        ])
