from __future__ import unicode_literals

# from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from .forms import ContactAdminForm
from .models import Contact, Address


class ContactInline(GenericTabularInline):
    model = Contact
    form = ContactAdminForm
    extra = 0


class AddressInline(GenericTabularInline):
    model = Address
    extra = 0
    max_num = 1
