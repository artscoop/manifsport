from __future__ import unicode_literals

from django.conf import settings
from django.test import TestCase

from .factories import ContactFactory, AddressFactory


# FIXME : quick and dirty hack preventing test suite failing
settings.LANGUAGE_CODE = 'en-US'


class ContactMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the first and last names of the contact
        '''
        contact = ContactFactory.build()
        self.assertEqual(contact.__str__(), 'John Doe')


class AddressMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the first and last names of the contact
        '''
        address = AddressFactory.build()
        self.assertEqual(address.__str__(),
                         ' '.join(['42, rue John Doe -',
                                   address.commune.zip_code,
                                   address.commune.__str__().upper()]))
