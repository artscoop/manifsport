from __future__ import unicode_literals

import factory

from administrative_division.factories import CommuneFactory

from .models import Contact, Address


class ContactFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Contact

    first_name = 'john'
    last_name = 'doe'
    phone = '06 55 55 55 55'


class AddressFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Address

    address = '42, rue John Doe'
    commune = factory.SubFactory(CommuneFactory)
