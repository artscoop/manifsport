from __future__ import unicode_literals

from django import forms

from ddcs_loire.utils import GenericForm

from extra_views.generic import GenericInlineFormSet

from localflavor.fr.forms import FRPhoneNumberField

from .models import Contact


class ContactAdminForm(forms.ModelForm):
    phone = FRPhoneNumberField()

    class Meta:  # pylint: disable=C1001
        model = Contact
        exclude = ('content_type', 'object_id', 'content_object')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(ContactAdminForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = FRPhoneNumberField()


class ContactForm(GenericForm):
    phone = FRPhoneNumberField()

    class Meta:  # pylint: disable=C1001
        model = Contact
        exclude = ('content_type', 'object_id', 'content_object')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = FRPhoneNumberField()
        self.helper.form_tag = False


class ContactInline(GenericInlineFormSet):
    model = Contact
    fields = ('first_name', 'last_name', 'phone',)
    extra = 1
    max_num = 1
    can_delete = False
    form_class = ContactForm
