from __future__ import unicode_literals

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render

from allauth.account.decorators import verified_email_required

from administration.models import SubAgent
from administration.models import CGDAgent
from administration.models import CommissariatAgent
from administration.models import CompanyAgent
from administration.models import CGServiceAgent


def sub_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @verified_email_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.subagent
            except SubAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def company_agent_required(function=None,
                           login_url=None,
                           redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.subagent.companyagent
            except CompanyAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cgd_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.subagent.cgdagent
            except CGDAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def edsr_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.subagent.edsragent
            except EDSRAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def commissariat_agent_required(function=None,
                                login_url=None,
                                redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.subagent.commissariatagent
            except CommissariatAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cgservice_agent_required(function=None,
                             login_url=None,
                             redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.subagent.cgserviceagent
            except CGServiceAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
