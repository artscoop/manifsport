# coding: utf-8
from __future__ import unicode_literals

import datetime

from django.core.urlresolvers import reverse
from django.test import TestCase

from administration.factories import GeneralCouncilFactory, DepartmentalGendarmerieGroupFactory
from administration.factories import RoadSafetySquadronFactory
from administration.factories import FireAndRescueServiceFactory
from administration.factories import PublicSafetyDirectionFactory

from administration.factories import CGServiceFactory
from administration.factories import CommissariatFactory
from administration.factories import CompanyFactory
from administration.factories import GendarmerieCompanyFactory
from administration.models import DepartmentalGendarmerieGroup

from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory
from administrative_division.factories import DepartmentFactory

from authorizations.factories import EventAuthorizationFactory

from events.factories import AuthorizedNonMotorizedEventFactory

from agreements.factories import CGAgreementFactory, GGDAgreementFactory
from agreements.factories import DDSPAgreementFactory
from agreements.factories import EDSRAgreementFactory
from agreements.factories import SDISAgreementFactory
from sub_agreements.factories import EDSRSubAgreementFactory

from .factories import CommissariatSubAgreementFactory
from .factories import CGDSubAgreementFactory
from .factories import CompanySubAgreementFactory
from .factories import CGServiceSubAgreementFactory


def create_ddsp_tree():
    department = DepartmentFactory.create()
    _ddsp = PublicSafetyDirectionFactory.create(department=department)
    arrondissement = ArrondissementFactory.create(department=department)
    city = CommuneFactory.create(arrondissement=arrondissement)
    commissariat = CommissariatFactory.create(commune=city)
    event = AuthorizedNonMotorizedEventFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        event=event,
    )
    agreement = DDSPAgreementFactory.create(authorization=authorization)
    return agreement, commissariat


class CommissariatSubAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str(self):
        agreement, commissariat = create_ddsp_tree()
        sub_agreement = CommissariatSubAgreementFactory.create(
            agreement=agreement,
            commissariat=commissariat,
        )
        event = sub_agreement.agreement.get_event()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                sub_agreement.commissariat.__str__().decode('utf8'),
            ]),
        )

    def test_get_absolute_url(self):
        agreement, commissariat = create_ddsp_tree()
        sub_agreement = CommissariatSubAgreementFactory.create(
            agreement=agreement,
            commissariat=commissariat,
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:commissariat_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_acknowledge(self):
        agreement, commissariat = create_ddsp_tree()
        sub_agreement = CommissariatSubAgreementFactory.create(
            agreement=agreement,
            commissariat=commissariat,
        )
        sub_agreement.acknowledge()
        self.assertEqual(sub_agreement.reply_date, datetime.date.today())

    def test_creation(self):
        agreement, commissariat = create_ddsp_tree()
        agreement = agreement
        self.assertEqual(agreement.subagreement_set.all().count(), 0)
        agreement.concerned_commissariat.add(commissariat)
        self.assertEqual(agreement.subagreement_set.all().count(), 1)


def create_edsr_tree():
    department = DepartmentFactory.create()
    _edsr = RoadSafetySquadronFactory.create(department=department)
    arrondissement = ArrondissementFactory.create(department=department)
    cgd = GendarmerieCompanyFactory.create(arrondissement=arrondissement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    event = AuthorizedNonMotorizedEventFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        event=event,
    )
    agreement = EDSRAgreementFactory.create(authorization=authorization)
    return agreement, cgd


class CGDSubAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str(self):
        agreement, cgd = create_edsr_tree()
        sub_agreement = CGDSubAgreementFactory.create(
            agreement=agreement,
            gendarmerie_company=cgd,
        )
        event = sub_agreement.agreement.get_event()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                sub_agreement.gendarmerie_company.__str__().decode('utf8'),
            ]),
        )

    def test_get_absolute_url(self):
        agreement, cgd = create_edsr_tree()
        sub_agreement = CGDSubAgreementFactory.create(
            agreement=agreement,
            gendarmerie_company=cgd,
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:cgd_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        agreement, cgd = create_edsr_tree()
        agreement = agreement
        self.assertEqual(agreement.subagreement_set.all().count(), 0)
        agreement.concerned_cgd.add(cgd)
        self.assertEqual(agreement.subagreement_set.all().count(), 1)


def create_sdis_tree():
    department = DepartmentFactory.create()
    _sdis = FireAndRescueServiceFactory.create(department=department)
    arrondissement = ArrondissementFactory.create(department=department)
    city = CommuneFactory.create(arrondissement=arrondissement)
    event = AuthorizedNonMotorizedEventFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        event=event,
    )
    agreement = SDISAgreementFactory.create(authorization=authorization)
    return agreement


class CompanySubAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str(self):
        sub_agreement = CompanySubAgreementFactory.create(
            agreement=create_sdis_tree(),
        )
        event = sub_agreement.agreement.get_event()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                sub_agreement.company.__str__().decode('utf8'),
            ]),
        )

    def test_get_absolute_url(self):
        sub_agreement = CompanySubAgreementFactory.create(
            agreement=create_sdis_tree(),
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:company_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        agreement = create_sdis_tree()
        self.assertEqual(agreement.subagreement_set.all().count(), 0)
        agreement.concerned_company.add(
            CompanyFactory.create()
        )
        self.assertEqual(agreement.subagreement_set.all().count(), 1)


def create_cg_tree():
    department = DepartmentFactory.create()
    arrondissement = ArrondissementFactory.create(department=department)
    city = CommuneFactory.create(arrondissement=arrondissement)
    _council = GeneralCouncilFactory.create(department=department)
    event = AuthorizedNonMotorizedEventFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        event=event,
    )
    agreement = CGAgreementFactory.create(authorization=authorization)
    return agreement


class CGServiceSubAgreementMethodTests(TestCase):  # pylint: disable=R0904

    def test_str(self):
        sub_agreement = CGServiceSubAgreementFactory.create(
            agreement=create_cg_tree()
        )
        event = sub_agreement.agreement.authorization.event
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                sub_agreement.cg_service.__str__().decode('utf8'),
            ]),
        )

    def test_get_absolute_url(self):
        sub_agreement = CGServiceSubAgreementFactory.create(
            agreement=create_cg_tree(),
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:cgservice_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        agreement = create_cg_tree()
        self.assertEqual(agreement.subagreement_set.all().count(), 0)
        agreement.concerned_services.add(
            CGServiceFactory.create()
        )
        self.assertEqual(agreement.subagreement_set.all().count(), 1)


def create_ggd_tree():
    department = DepartmentFactory.create()
    _ggd = DepartmentalGendarmerieGroupFactory.create(department=department)
    arrondissement = ArrondissementFactory.create(department=department)
    cgd = GendarmerieCompanyFactory.create(arrondissement=arrondissement)
    edsr = RoadSafetySquadronFactory.create(department=department)
    city = CommuneFactory.create(arrondissement=arrondissement)
    event = AuthorizedNonMotorizedEventFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        event=event
    )
    agreement = GGDAgreementFactory.create(authorization=authorization, concerned_edsr=edsr)
    return agreement, cgd, edsr


class EDSRSubAgreementMethodTests(TestCase):  # pylint: disable=R0904
    """
    Tests pour les préavis EDSR
    """

    def test_str(self):
        agreement, cgd, edsr = create_ggd_tree()
        sub_agreement = EDSRSubAgreementFactory.create(
            agreement=agreement,
            road_safety_squadron=edsr,
        )
        event = sub_agreement.agreement.get_event()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                event.__str__().decode('utf8'),
                sub_agreement.road_safety_squadron.__str__().decode('utf8'),
            ]),
        )

    def test_get_absolute_url(self):
        agreement, cgd, edsr = create_ggd_tree()
        sub_agreement = EDSRSubAgreementFactory.create(
            agreement=agreement,
            road_safety_squadron=edsr,
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:edsr_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        """
        Créer un une arborescence impliquant un sous-avis d'EDSR
        """
        agreement, cgd, edsr = create_ggd_tree()
        self.assertEqual(agreement.subagreement_set.all().count(), 1)
        agreement.concerned_cgd.add(cgd)
        self.assertEqual(agreement.subagreement_set.all().count(), 2)
