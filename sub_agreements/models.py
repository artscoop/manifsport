# coding: utf-8
from __future__ import unicode_literals

import datetime

from django.core.urlresolvers import reverse

from django.db import models
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django_fsm import transition

from administration.models import Company, RoadSafetySquadron
from administration.models import Commissariat
from administration.models import GendarmerieCompany
from administration.models import CGService
from administration.models import FireService
from administration.models import Brigade

from agreements.models import AbstractBaseAgreement, GGDAgreement
from agreements.models import Agreement
from agreements.models import CGAgreement
from agreements.models import DDSPAgreement
from agreements.models import EDSRAgreement
from agreements.models import SDISAgreement

from authorizations.models import EventAuthorization
from authorizations.signals import authorization_published

from ddcs_loire.utils import add_notification_entry
from ddcs_loire.utils import add_log_entry


class SubAgreementQuerySet(models.QuerySet):
    def to_process(self):
        return self.filter(agreement__authorization__event__end_date__gt=timezone.now())


class SubAgreement(AbstractBaseAgreement):
    agreement = models.ForeignKey(Agreement,
                                  verbose_name=_('agreement'))
    objects = SubAgreementQuerySet.as_manager()

    def log_ack(self, agents):
        add_log_entry(
            agents=agents,
            action=_('subagreement acknowledged'),
            event=self.agreement.get_event(),
        )

    def notify_creation(self, agents, content_object):
        add_notification_entry(agents=agents,
                               event=self.agreement.get_event(),
                               content_object=content_object,
                               subject=_('subagreement requested'))

    def notify_ack(self, agents, content_object, institutional=None):
        add_notification_entry(agents=agents,
                               event=self.agreement.get_event(),
                               content_object=content_object,
                               subject=_('subagreement acknowledged'),
                               institutional=institutional)

    def notify_publication(self, agents):
        prefecture = self.agreement.authorization.get_concerned_prefecture()
        add_notification_entry(agents=agents,
                               event=self.agreement.get_event(),
                               subject=_('bylaw published'),
                               content_object=prefecture)


@python_2_unicode_compatible
class CGDSubAgreement(SubAgreement):
    gendarmerie_company = models.ForeignKey(
        GendarmerieCompany,
        verbose_name=_('gendarmerie company'),
    )
    concerned_brigades = models.ManyToManyField(
        Brigade,
        verbose_name=_('concerned brigades'),
    )
    objects = SubAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('cgd sub-agreement')
        verbose_name_plural = _('cgd sub-agreements')

    def __str__(self):
        event = self.agreement.get_event()
        cgd = self.gendarmerie_company
        return ' - '.join([
            event.__str__().decode('utf8'),
            cgd.__str__().decode('utf8'),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:cgd_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        return self.gendarmerie_company.arrondissement.department.roadsafetysquadron

    def get_agents(self):
        return self.gendarmerie_company.cgdagent_set.all()

    @transition(field='state',
                source=['created', 'notified'], target='notified')
    def notify_brigades(self):
        add_notification_entry(agents=[agent for brigade in self.concerned_brigades.all() for agent in brigade.brigadeagent_set.all()],
                               event=self.agreement.get_event(),
                               content_object=self.gendarmerie_company,
                               subject=_('take knowledge'))
        add_log_entry(
            agents=self.get_agents(),
            action=_('brigades notified'),
            event=self.agreement.get_event(),
        )

    @transition(field='state', source='notified', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_edsr().edsragent_set.all(),
                        content_object=self.gendarmerie_company)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=CGDSubAgreement)
def notify_cgd_subagreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_edsr())


@receiver(m2m_changed, sender=EDSRAgreement.concerned_cgd.through)
def create_cgd_subagreements_1(sender, instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            agreement, _ = CGDSubAgreement.objects.get_or_create(
                    agreement=instance,
                    gendarmerie_company=GendarmerieCompany.objects.get(pk=pk)
            )


@receiver(m2m_changed, sender=GGDAgreement.concerned_cgd.through)
def create_cgd_subagreements_2(sender, instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            agreement, _ = CGDSubAgreement.objects.get_or_create(
                agreement=instance,
                gendarmerie_company=GendarmerieCompany.objects.get(pk=pk)
            )


@python_2_unicode_compatible
class EDSRSubAgreement(SubAgreement):
    road_safety_squadron = models.ForeignKey(
        RoadSafetySquadron,
        verbose_name=_('road safety squadron'),
    )
    objects = SubAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('edsr sub-agreement')
        verbose_name_plural = _('edsr sub-agreements')

    def __str__(self):
        event = self.agreement.get_event()
        edsr = self.road_safety_squadron
        return ' - '.join([
            event.__str__().decode('utf8'),
            edsr.__str__().decode('utf8'),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:edsr_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        return self.road_safety_squadron

    def get_ggd(self):
        return self.road_safety_squadron.department.departmentalgendarmeriegroup

    def get_agents(self):
        return self.road_safety_squadron.edsragent_set.all()

    @transition(field='state',
                source=['created', 'notified'], target='notified')
    def notify_edsr_agents(self):
        add_notification_entry(agents=[agent for agent in self.get_agents()],
                               event=self.agreement.get_event(),
                               content_object=self.road_safety_squadron,
                               subject=_('take knowledge'))
        add_log_entry(
            agents=self.get_agents(),
            action=_('road safety squadron notified'),
            event=self.agreement.get_event(),
        )

    @transition(field='state', source='notified', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_ggd().ggdagent_set.all(),
                        content_object=self.road_safety_squadron)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=EDSRSubAgreement)
def notify_edsr_subagreement(sender, instance, created, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_ggd())

@receiver(post_save, sender=GGDAgreement)
def create_edsr_subagreement(sender, instance, created, **kwargs):
    """
    Créer un sous-avis EDSR pour un nouvel avis GGD
    """
    if created:
        agreement, _ = EDSRSubAgreement.objects.get_or_create(
            agreement=instance,
            road_safety_squadron=instance.concerned_edsr
        )


@python_2_unicode_compatible
class CompanySubAgreement(SubAgreement):
    company = models.ForeignKey(
        Company,
        verbose_name=_('company'),
    )
    concerned_cis = models.ManyToManyField(
        FireService,
        verbose_name=_('concerned cis'),
    )
    objects = SubAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('company sub-agreement')
        verbose_name_plural = _('companies sub-agreements')

    def __str__(self):
        event = self.agreement.get_event()
        company = self.company
        return ' - '.join([
            event.__str__().decode('utf8'),
            company.__str__().decode('utf8'),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:company_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_sdis(self):
        return self.company.fire_and_rescue_service

    def get_agents(self):
        return self.company.companyagent_set.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_sdis().sdisagent_set.all(),
                        content_object=self.company)
        self.log_ack(agents=self.get_agents())

    @transition(field='state', source='acknowledged', target='acknowledged')
    def notify_cis(self):
        add_notification_entry(agents=[agent for cis in self.concerned_cis.all() for agent in cis.cisagent_set.all()],
                               event=self.agreement.get_event(),
                               content_object=self.company,
                               subject=_('take knowledge'))
        add_log_entry(agents=self.get_agents(),
                      action=_('cis notified'),
                      event=self.agreement.get_event())


@receiver(post_save, sender=CompanySubAgreement)
def notify_company_subagreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_sdis())


@receiver(m2m_changed, sender=SDISAgreement.concerned_company.through)
def create_company_subagreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            CompanySubAgreement.objects.get_or_create(
                agreement=instance,
                company=Company.objects.get(pk=pk)
            )


@python_2_unicode_compatible
class CommissariatSubAgreement(SubAgreement):
    commissariat = models.ForeignKey(
        Commissariat,
        verbose_name=_('commissariat'),
    )
    objects = SubAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('commissariat sub-agreement')
        verbose_name_plural = _('commissariats sub-agreements')

    def __str__(self):
        event = self.agreement.get_event()
        commissariat = self.commissariat
        return ' - '.join([
            event.__str__().decode('utf8'),
            commissariat.__str__().decode('utf8'),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:commissariat_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_ddsp(self):
        return self.commissariat.commune.arrondissement.department.publicsafetydirection

    def get_agents(self):
        return self.commissariat.commissariatagent_set.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_ddsp().ddspagent_set.all(),
                        content_object=self.commissariat,
                        institutional=self.get_ddsp())
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=CommissariatSubAgreement)
def notify_commissariat_subagreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_ddsp())


@receiver(m2m_changed, sender=DDSPAgreement.concerned_commissariat.through)
def create_commissariat_subagreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            CommissariatSubAgreement.objects.get_or_create(
                agreement=instance,
                commissariat=Commissariat.objects.get(pk=pk)
            )


@python_2_unicode_compatible
class CGServiceSubAgreement(SubAgreement):
    cg_service = models.ForeignKey(
        CGService,
        verbose_name=_('cg service'),
    )
    objects = SubAgreementQuerySet.as_manager()

    class Meta:
        verbose_name = _('cg service sub-agreement')
        verbose_name_plural = _('cg service sub-agreements')

    def __str__(self):
        event = self.agreement.get_event()
        cg_service = self.cg_service
        return ' - '.join([
            event.__str__().decode('utf8'),
            cg_service.__str__().decode('utf8'),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:cgservice_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_agents(self):
        return self.cg_service.cgserviceagent_set.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        council = self.cg_service.general_council
        self.notify_ack(agents=council.cgagent_set.all(),
                        content_object=self.cg_service,
                        institutional=council)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=CGServiceSubAgreement)
def notify_cgservice_subagreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.cg_service.general_council)


@receiver(m2m_changed, sender=CGAgreement.concerned_services.through)
def create_cgservice_subagreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            CGServiceSubAgreement.objects.get_or_create(
                agreement=instance,
                cg_service=CGService.objects.get(pk=pk)
            )


@receiver(authorization_published, sender=EventAuthorization)
def notify_authorization_publication(instance, **kwargs):
    for subagreement in CGDSubAgreement.objects.filter(agreement__authorization=instance):
        subagreement.notify_publication(agents=subagreement.get_agents())
    for subagreement in CompanySubAgreement.objects.filter(agreement__authorization=instance):
        subagreement.notify_publication(agents=subagreement.get_agents())
    for subagreement in CommissariatSubAgreement.objects.filter(agreement__authorization=instance):
        subagreement.notify_publication(agents=subagreement.get_agents())
    for subagreement in CGServiceSubAgreement.objects.filter(agreement__authorization=instance):
        subagreement.notify_publication(agents=subagreement.get_agents())
