from __future__ import unicode_literals

from django.contrib import admin

from .models import CGDSubAgreement
from .models import EDSRSubAgreement
from .models import CompanySubAgreement
from .models import CommissariatSubAgreement
from .models import CGServiceSubAgreement


class CGDSubAgreementInline(admin.StackedInline):
    model = CGDSubAgreement
    extra = 0


class ESRSubAgreementInline(admin.StackedInline):
    model = EDSRSubAgreement
    extra = 0


class CompanySubAgreementInline(admin.StackedInline):
    model = CompanySubAgreement
    extra = 0


class CommissariatSubAgreementInline(admin.StackedInline):
    model = CommissariatSubAgreement
    extra = 0


class CGServiceSubAgreementInline(admin.StackedInline):
    model = CGServiceSubAgreement
    extra = 0
