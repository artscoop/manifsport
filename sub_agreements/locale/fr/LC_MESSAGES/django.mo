��    )      d  ;   �      �     �     �     �     �     �  	   	  	          
   /     :     S     m          �     �     �     �     �     �          ,     4     J     ]  	   k     u  5   �     �     �  `   �  <   F     �     �     �     �     �     �     
          0  �  ?  &   �  	   �     �     	     $	     8	  	   =	     G	  
   [	     f	     {	     �	     �	  #   �	     �	     �	     �	     	
      
     8
  	   M
     W
     k
     �
     �
     �
  >   �
  (   �
     "  x   <  M   �  !        %     A  	   U     _     o  $   �     �     �                                     "       #   '                               $                                     )   (                     	      &   !   
                              %                  No sub-agreements to manage Sub Agreements acknowledge sub-agreement acknowledge subagreement actions available agreement attention brigades notified cg service cg service sub-agreement cg service sub-agreements cgd sub-agreement cgd sub-agreements choose concerned brigades choose concerned cis cis notified commissariat commissariat sub-agreement commissariats sub-agreements companies sub-agreements company company sub-agreement concerned brigades concerned cis dashboard event detail find here all actions available related to this event gendarmerie company go back to the dashboard instructor won't have to take your sub-agreement into account when authorizing or not the event. it is now too late to give your sub-agreement to this event. notify concerned brigades notify concerned cis sub agreements requested sub-agreement subagreement acknowledged subagreement requested take knowledge to give your agreement you have until Project-Id-Version: sub_agreements
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-01-22 12:32+0100
PO-Revision-Date: 2015-01-22 12:32+0100
Last-Translator: Simon Panay <simon.panay@openscop.fr>
Language-Team: fr <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.5.4
 Aucune demande de pré-avis à traiter Pré-Avis rendre le pré-avis rendre le pré-avis actions disponibles avis attention brigades informées service CG pré-avis service CG pré-avis services CG pré-avis CGD pré-avis CGD choisissez les brigades concernées choisissez les CIS concernés CIS informé commissariat pré-avis commissariat pré-avis commissariats pré-avis compagnies compagnie pré-avis compagnie brigades concernées CIS concerné tableau de bord détail de la manifestation voici les actions disponibles relatives à cette manifestation compagnie de gendarmerie départementale retour au tableau de bord les instructeurs du dossier ne seront pas obligés de prendre en compte votre pré-avis lors de l'instruction du dossier il est maintenant trop tard pour rendre le pré-avis pour cette manifestation informer les brigades concernées Informer les CIS concernés pré-avis demandés pré-avis pré-avis rendu pré-avis demandés prenez connaissance des informations pour rendre le pré-avis vous avez jusqu'au 