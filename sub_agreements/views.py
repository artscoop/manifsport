from __future__ import unicode_literals

from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.views.generic.edit import UpdateView

from administration.models import CompanyAgent
from administration.models import CGDAgent
from administration.models import CommissariatAgent
from administration.models import CGServiceAgent

from agreements.views import BaseAcknowledgeView
from ddcs_loire.utils import GenericForm

from events.models import News
from sub_agreements.decorators import edsr_agent_required
from sub_agreements.models import EDSRSubAgreement

from .decorators import sub_agent_required
from .decorators import cgd_agent_required
from .decorators import company_agent_required
from .decorators import commissariat_agent_required
from .decorators import cgservice_agent_required

from .forms import SubAgreementForm
from .forms import NotifyCISForm
from .forms import NotifyBrigadesForm

from .models import SubAgreement
from .models import CompanySubAgreement
from .models import CGDSubAgreement
from .models import CommissariatSubAgreement
from .models import CGServiceSubAgreement


class Dashboard(ListView):  # pylint: disable=R0901
    model = SubAgreement

    @method_decorator(sub_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        subagent = self.request.user.subagent
        try:
            subagent.companyagent
        except CompanyAgent.DoesNotExist:
            try:
                subagent.cgdagent
            except CGDAgent.DoesNotExist:
                try:
                    subagent.commissariatagent
                except CommissariatAgent.DoesNotExist:
                    try:
                        subagent.cgserviceagent
                    except CGServiceAgent.DoesNotExist:
                        pass
                    return CGServiceSubAgreement.objects.to_process().filter(
                        cg_service=subagent.cgserviceagent.cg_service
                    )
                return CommissariatSubAgreement.objects.to_process().filter(
                    commissariat=subagent.commissariatagent.commissariat
                )
            return CGDSubAgreement.objects.to_process().filter(
                gendarmerie_company=subagent.cgdagent.cgd
            )
        return CompanySubAgreement.objects.to_process().filter(
            company=subagent.companyagent.company
        )

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['last_news'] = News.objects.last()
        return context


###########################################################
# CGDSub Agreements Views
###########################################################
class CGDSubAgreementDetail(DetailView):  # pylint: disable=R0901
    model = CGDSubAgreement

    @method_decorator(cgd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGDSubAgreementDetail, self).dispatch(*args, **kwargs)


class CGDSubAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = CGDSubAgreement
    form_class = SubAgreementForm

    @method_decorator(cgd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGDSubAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)


class NotifyBrigadesView(UpdateView):  # pylint: disable=R0901
    model = CGDSubAgreement
    form_class = NotifyBrigadesForm
    template_name_suffix = '_notify_form'

    @method_decorator(cgd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(NotifyBrigadesView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyBrigadesView, self).form_valid(form)
        form.instance.notify_brigades()
        form.instance.save()
        return response


###########################################################
# EDSR Sub Agreements Views
###########################################################
class EDSRSubAgreementDetail(DetailView):  # pylint: disable=R0901
    model = EDSRSubAgreement

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRSubAgreementDetail, self).dispatch(*args, **kwargs)


class EDSRSubAgreementAcknowledge(BaseAcknowledgeView):  # pylint: disable=R0901
    model = EDSRSubAgreement
    form_class = SubAgreementForm

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRSubAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)


class NotifyEDSRAgentsView(UpdateView):  # pylint: disable=R0901
    model = EDSRSubAgreement
    form_class = GenericForm
    template_name_suffix = '_notify_form'

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(NotifyEDSRAgentsView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyEDSRAgentsView, self).form_valid(form)
        form.instance.notify_edsr_agents()
        form.instance.save()
        return response


###########################################################
# CompanySub Agreements Views
###########################################################
class CompanySubAgreementDetail(DetailView):  # pylint: disable=R0901
    model = CompanySubAgreement

    @method_decorator(company_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanySubAgreementDetail, self).dispatch(*args, **kwargs)


class CompanySubAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = CompanySubAgreement
    form_class = SubAgreementForm

    @method_decorator(company_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanySubAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)


class NotifyCISView(UpdateView):  # pylint: disable=R0901
    model = CompanySubAgreement
    form_class = NotifyCISForm
    template_name_suffix = '_notify_form'

    @method_decorator(company_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(NotifyCISView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyCISView, self).form_valid(form)
        form.instance.notify_cis()
        form.instance.save()
        return response


###########################################################
# CommissariatSub Agreements Views
###########################################################
class CommissariatSubAgreementDetail(DetailView):  # pylint: disable=R0901
    model = CommissariatSubAgreement

    @method_decorator(commissariat_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CommissariatSubAgreementDetail,
                     self).dispatch(*args, **kwargs)


class CommissariatSubAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = CommissariatSubAgreement
    form_class = SubAgreementForm

    @method_decorator(commissariat_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CommissariatSubAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)


###########################################################
# CGServiceSub Agreements Views
###########################################################
class CGServiceSubAgreementDetail(DetailView):  # pylint: disable=R0901
    model = CGServiceSubAgreement

    @method_decorator(cgservice_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGServiceSubAgreementDetail,
                     self).dispatch(*args, **kwargs)


class CGServiceSubAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = CGServiceSubAgreement
    form_class = SubAgreementForm

    @method_decorator(cgservice_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGServiceSubAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)
