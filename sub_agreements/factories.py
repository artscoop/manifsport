from __future__ import unicode_literals

import factory

from administration.factories import CommissariatFactory, RoadSafetySquadronFactory
from administration.factories import GendarmerieCompanyFactory
from administration.factories import CompanyFactory
from administration.factories import CGServiceFactory
from agreements.factories import EDSRAgreementFactory
from agreements.factories import DDSPAgreementFactory
from agreements.factories import SDISAgreementFactory
from agreements.factories import CGAgreementFactory
from sub_agreements.models import EDSRSubAgreement

from .models import CGDSubAgreement
from .models import CommissariatSubAgreement
from .models import CompanySubAgreement
from .models import CGServiceSubAgreement


class CGDSubAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = CGDSubAgreement

    agreement = factory.SubFactory(EDSRAgreementFactory)
    gendarmerie_company = factory.SubFactory(GendarmerieCompanyFactory)


class EDSRSubAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = EDSRSubAgreement

    agreement = factory.SubFactory(EDSRAgreementFactory)
    road_safety_squadron = factory.SubFactory(RoadSafetySquadronFactory)


class CommissariatSubAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = CommissariatSubAgreement

    agreement = factory.SubFactory(DDSPAgreementFactory)
    commissariat = factory.SubFactory(CommissariatFactory)


class CompanySubAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = CompanySubAgreement

    agreement = factory.SubFactory(SDISAgreementFactory)
    company = factory.SubFactory(CompanyFactory)


class CGServiceSubAgreementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = CGServiceSubAgreement

    agreement = factory.SubFactory(CGAgreementFactory)
    cg_service = factory.SubFactory(CGServiceFactory)
