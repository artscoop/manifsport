from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SubAgreementsConfig(AppConfig):
    name = 'sub_agreements'
    verbose_name = _('Sub Agreements')
