# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20160108_1334'),
        ('sub_agreements', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EDSRSubAgreement',
            fields=[
                ('subagreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sub_agreements.SubAgreement')),
                ('road_safety_squadron', models.ForeignKey(verbose_name='road safety squadron', to='administration.RoadSafetySquadron')),
            ],
            options={
                'verbose_name': 'edsr sub-agreement',
                'verbose_name_plural': 'edsr sub-agreements',
            },
            bases=('sub_agreements.subagreement',),
        ),
    ]
