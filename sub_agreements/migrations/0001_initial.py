# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
        ('agreements', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubAgreement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_date', models.DateField(auto_now_add=True, verbose_name='request date')),
                ('reply_date', models.DateField(null=True, verbose_name='reply date', blank=True)),
                ('favorable', models.BooleanField(default=False, help_text='favorable help text', verbose_name='favorable')),
                ('prescriptions', models.TextField(verbose_name='prescriptions', blank=True)),
                ('state', django_fsm.FSMField(default='created', max_length=50)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CompanySubAgreement',
            fields=[
                ('subagreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sub_agreements.SubAgreement')),
                ('company', models.ForeignKey(verbose_name='company', to='administration.Company')),
                ('concerned_cis', models.ManyToManyField(to='administration.FireService', verbose_name='concerned cis')),
            ],
            options={
                'verbose_name': 'company sub-agreement',
                'verbose_name_plural': 'companies sub-agreements',
            },
            bases=('sub_agreements.subagreement',),
        ),
        migrations.CreateModel(
            name='CommissariatSubAgreement',
            fields=[
                ('subagreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sub_agreements.SubAgreement')),
                ('commissariat', models.ForeignKey(verbose_name='commissariat', to='administration.Commissariat')),
            ],
            options={
                'verbose_name': 'commissariat sub-agreement',
                'verbose_name_plural': 'commissariats sub-agreements',
            },
            bases=('sub_agreements.subagreement',),
        ),
        migrations.CreateModel(
            name='CGServiceSubAgreement',
            fields=[
                ('subagreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sub_agreements.SubAgreement')),
                ('cg_service', models.ForeignKey(verbose_name='cg service', to='administration.CGService')),
            ],
            options={
                'verbose_name': 'cg service sub-agreement',
                'verbose_name_plural': 'cg service sub-agreements',
            },
            bases=('sub_agreements.subagreement',),
        ),
        migrations.CreateModel(
            name='CGDSubAgreement',
            fields=[
                ('subagreement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sub_agreements.SubAgreement')),
                ('concerned_brigades', models.ManyToManyField(to='administration.Brigade', verbose_name='concerned brigades')),
                ('gendarmerie_company', models.ForeignKey(verbose_name='gendarmerie company', to='administration.GendarmerieCompany')),
            ],
            options={
                'verbose_name': 'cgd sub-agreement',
                'verbose_name_plural': 'cgd sub-agreements',
            },
            bases=('sub_agreements.subagreement',),
        ),
        migrations.AddField(
            model_name='subagreement',
            name='agreement',
            field=models.ForeignKey(verbose_name='agreement', to='agreements.Agreement'),
            preserve_default=True,
        ),
    ]
