from __future__ import unicode_literals

from django.conf.urls import patterns, url

from sub_agreements.views import EDSRSubAgreementDetail, EDSRSubAgreementAcknowledge, NotifyEDSRAgentsView
from .views import Dashboard

from .views import CGDSubAgreementAcknowledge
from .views import CGDSubAgreementDetail
from .views import NotifyBrigadesView

from .views import CompanySubAgreementAcknowledge
from .views import CompanySubAgreementDetail
from .views import NotifyCISView

from .views import CommissariatSubAgreementAcknowledge
from .views import CommissariatSubAgreementDetail

from .views import CGServiceSubAgreementAcknowledge
from .views import CGServiceSubAgreementDetail

urlpatterns = patterns(
    # pylint: disable=E1120
    '',
    url(r'^dashboard/$', Dashboard.as_view(),
        name='dashboard'),
    # CGD SubAgreements
    url(r'^cgd/(?P<pk>\d+)/$',
        CGDSubAgreementDetail.as_view(),
        name='cgd_subagreement_detail'),
    url(r'^cgd/(?P<pk>\d+)/acknowledge/$',
        CGDSubAgreementAcknowledge.as_view(),
        name='cgd_subagreement_acknowledge'),
    url(r'^cgd/(?P<pk>\d+)/notify_brigades/$',
        NotifyBrigadesView.as_view(),
        name='notify_brigades'),
    # EDSR SubAgreements
    url(r'^edsr/(?P<pk>\d+)/$',
        EDSRSubAgreementDetail.as_view(),
        name='edsr_subagreement_detail'),
    url(r'^edsr/(?P<pk>\d+)/acknowledge/$',
        EDSRSubAgreementAcknowledge.as_view(),
        name='edsr_subagreement_acknowledge'),
    url(r'^edsr/(?P<pk>\d+)/notify_agents/$',
        NotifyEDSRAgentsView.as_view(),
        name='notify_edsr_agents'),
    # Company SubAgreements
    url(r'^company/(?P<pk>\d+)/$',
        CompanySubAgreementDetail.as_view(),
        name='company_subagreement_detail'),
    url(r'^company/(?P<pk>\d+)/acknowledge/$',
        CompanySubAgreementAcknowledge.as_view(),
        name='company_subagreement_acknowledge'),
    url(r'^company/(?P<pk>\d+)/notify_cis/$',
        NotifyCISView.as_view(),
        name='notify_cis'),
    # Commissariat SubAgreements
    url(r'^commissariat/(?P<pk>\d+)/$',
        CommissariatSubAgreementDetail.as_view(),
        name='commissariat_subagreement_detail'),
    url(r'^commissariat/(?P<pk>\d+)/acknowledge/$',
        CommissariatSubAgreementAcknowledge.as_view(),
        name='commissariat_subagreement_acknowledge'),
    # CG Service SubAgreements
    url(r'^cgservice/(?P<pk>\d+)/$',
        CGServiceSubAgreementDetail.as_view(),
        name='cgservice_subagreement_detail'),
    url(r'^cgservice/(?P<pk>\d+)/acknowledge/$',
        CGServiceSubAgreementAcknowledge.as_view(),
        name='cgservice_subagreement_acknowledge'),
)
