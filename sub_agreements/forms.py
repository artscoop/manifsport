from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Submit, Layout
from crispy_forms.bootstrap import FormActions

from sub_agreements.models import EDSRSubAgreement
from .models import SubAgreement
from .models import CompanySubAgreement
from .models import CGDSubAgreement


class SubAgreementForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = SubAgreement
        fields = ('favorable', 'prescriptions',)

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(SubAgreementForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(
                Submit('save', _('acknowledge subagreement').capitalize())
            )
        )


class NotifyCISForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = CompanySubAgreement
        fields = ('concerned_cis',)

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(NotifyCISForm, self).__init__(*args, **kwargs)
        self.fields['concerned_cis'].queryset = self.instance.company.fireservice_set.all()  # noqa pylint: disable=E1101
        self.helper.layout = Layout(
            'concerned_cis',
            FormActions(
                Submit('save', _('notify concerned cis').capitalize())
            )
        )


class NotifyBrigadesForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = CGDSubAgreement
        fields = ('concerned_brigades',)

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(NotifyBrigadesForm, self).__init__(*args, **kwargs)
        self.fields['concerned_brigades'].queryset = self.instance.gendarmerie_company.brigade_set.all()  # noqa pylint: disable=E1101
        self.helper.layout = Layout(
            'concerned_brigades',
            FormActions(
                Submit('save', _('notify concerned brigades').capitalize())
            )
        )
