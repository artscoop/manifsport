'''
Django Production settings for ddcs_loire project.
'''
from ddcs_loire.settings import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'

# Needed for SSL
# See https://docs.djangoproject.com/en/1.6/ref/settings/#std:setting-SESSION_COOKIE_SECURE
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['www.manifestationsportive.fr',
                 'manifestationsportive.fr']

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'raven.contrib.django.raven_compat',
    'localflavor',
    'allauth',
    'allauth.account',
    'bootstrap3_datetime',
    'crispy_forms',
    'ckeditor',
    'administrative_division',
    'administration',
    'agreements',
    'contacts',
    'emergencies',
    'evaluations',
    'notifications',
    'protected_areas',
    'sports',
    'events',
    'authorizations',
    'declarations',
    'sub_agreements',
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'localhost',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATICFILES_DIRS = (
    '/home/rtadmin/ddcs_loire/env/lib/python2.7/site-packages/django/contrib/admin/static/',
    os.path.join(BASE_DIR, "static"),
)

STATIC_ROOT = '/home/rtadmin/ddcs_loire/releases/current/static/'

MEDIA_ROOT = '/var/www/ddcs/media/'


# Email backend
# https://docs.djangoproject.com/en/1.6/topics/email/#email-backends
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True


# Exceptions logging :
# https://app.getsentry.com/manifestationsportiveloire/ddcs42-production/
RAVEN_CONFIG = {
    'dsn': '',
}
