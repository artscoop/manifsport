# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper


def add_notification_entry(agents, subject, content_object, event,
                           institutional=None):
    mail_subject = '{sender} : {subject} {liaison} \"{event}\"'.format(
        sender=content_object,
        subject=subject,
        liaison=_('for event'),
        event=event.name,  # pylint: disable=E1101
    )
    # Todo : à tester
    mail_footer = "".join([(
        "\n\n=== NE PAS RÉPONDRE À CET E-MAIL ===\n"
        "Pour effectuer la suite de votre démarche : \n"
        "- connectez vous sur la plateforme Manifestation Sportive\n"
        "- rendez vous sur votre tableau de bord\n"), settings.MAIN_URL])
    mail_message = "".join([mail_subject, mail_footer])
    if institutional:
        send_mail(mail_subject,
                  mail_message,
                  settings.DEFAULT_FROM_EMAIL,
                  [institutional.email])
    for agent in agents:
        agent.user.notification_set.create(
            event=event,
            subject=subject,
            content_object=content_object,
        )
        send_mail(mail_subject,
                  mail_message,
                  settings.DEFAULT_FROM_EMAIL,
                  [agent.user.email])


def add_log_entry(agents, action, event):
    for agent in agents:
        agent.user.action_set.create(
            event=event,
            action=action,
        )


class GenericForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(GenericForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
