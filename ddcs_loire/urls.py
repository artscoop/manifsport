from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.sitemaps import FlatPageSitemap

from events.views import ProfileDetailView
from events.views import UserUpdateView
from events.views import StructureUpdateView

admin.autodiscover()

sitemaps = {
    'flatpages': FlatPageSitemap,
}

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ddcs_loire.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'^ckeditor/', include('ckeditor.urls')),
    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/$', ProfileDetailView.as_view(), name='profile'),
    url(r'^accounts/profile/edit$', UserUpdateView.as_view(), name='profile_edit'),
    url(r'^accounts/profile/structure/edit', StructureUpdateView.as_view(), name='structure_edit'),
    url(r'^agreements/', include('agreements.urls', namespace='agreements')),
    url(r'^sub_agreements/', include('sub_agreements.urls', namespace='sub_agreements')),
    url(r'^authorizations/', include('authorizations.urls', namespace='authorizations')),
    url(r'^declarations/', include('declarations.urls', namespace='declarations')),
    url(r'^evaluations/', include('evaluations.urls', namespace='evaluations')),
    url(r'^', include('events.urls', namespace='events')),
    url(r'^protected_areas/', include('protected_areas.urls', namespace='protected_areas')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + patterns(
    'django.contrib.flatpages.views', (r'^(?P<url>.*/)$', 'flatpage'),
)
