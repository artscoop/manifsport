# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Natura2000Site',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='name')),
                ('site_type', models.CharField(max_length=1, verbose_name='type', choices=[('s', 'SIC'), ('z', 'ZPS')])),
                ('index', models.CharField(unique=True, max_length=9, verbose_name='index')),
            ],
            options={
                'verbose_name': 'natura 2000 site',
                'verbose_name_plural': 'natura 2000 sites',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Natura2000SiteOperator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
                ('site', models.OneToOneField(verbose_name='natura 2000 site', to='protected_areas.Natura2000Site')),
            ],
            options={
                'verbose_name': 'natura 2000 site operator',
                'verbose_name_plural': 'natura 2000 site operators',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegionalNaturalReserve',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='name')),
                ('code', models.PositiveIntegerField(unique=True, verbose_name='code')),
            ],
            options={
                'verbose_name': 'regional natural reserve',
                'verbose_name_plural': 'regional natural reserves',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RNRAdministrator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_name', models.CharField(max_length=10, verbose_name='short_name')),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('email', models.EmailField(max_length=200, verbose_name='e-mail')),
                ('rnr', models.OneToOneField(verbose_name='RNR', to='protected_areas.RegionalNaturalReserve')),
            ],
            options={
                'verbose_name': 'RNR administrator',
                'verbose_name_plural': 'RNR administrators',
            },
            bases=(models.Model,),
        ),
    ]
