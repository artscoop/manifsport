from __future__ import unicode_literals

import factory

from .models import RNRAdministrator, RegionalNaturalReserve
from .models import Natura2000Site, Natura2000SiteOperator


class RegionalNaturalReserveFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = RegionalNaturalReserve

    name = 'Gorges de la Loire'


class RNRAdministratorFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = RNRAdministrator

    short_name = 'FRAPNA'
    name = 'Federation Rhone-Alpes de Protection de la Nature de la Loire'
    email = 'john.doe@frapna.org'
    rnr = factory.SubFactory(RegionalNaturalReserveFactory)


class Natura2000SiteOperatorFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Natura2000SiteOperator

    name = 'Parc Naturel Regional du Livradois-Forez'
    email = 'john.doe@parc-livradois-forez.org'


class Natura2000SiteFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Natura2000Site

    name = 'Bois-Noirs'
    site_type = 's'
    index = 'FR8301045'
