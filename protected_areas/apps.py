from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ProtectedAreasConfig(AppConfig):
    name = 'protected_areas'
    verbose_name = _('Protected Areas')
