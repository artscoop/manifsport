from __future__ import unicode_literals

from django.test import TestCase

from .factories import RNRAdministratorFactory, RegionalNaturalReserveFactory

from .factories import Natura2000SiteOperatorFactory, Natura2000SiteFactory


class RegionalNaturalReserveMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the RNR
        '''
        rnr = RegionalNaturalReserveFactory.build()
        self.assertEqual(rnr.__str__(), 'Gorges de la Loire')


class RNRAdministratorMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the short name of the Administrator
        '''
        rnr_administrator = RNRAdministratorFactory.build()
        self.assertEqual(rnr_administrator.__str__(), 'FRAPNA')


class Natura2000SiteMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the natura 2000 site
        '''
        natura_2000_site = Natura2000SiteFactory.build()
        self.assertEqual(natura_2000_site.__str__(), 'Bois-Noirs')


class Natura2000SiteOperatorMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the short name of the operator
        '''
        natura_2000_site_operator = Natura2000SiteOperatorFactory.build()
        self.assertEqual(
            natura_2000_site_operator.__str__(),
            'Parc Naturel Regional du Livradois-Forez',
        )
