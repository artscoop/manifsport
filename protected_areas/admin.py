from __future__ import unicode_literals

from django.contrib import admin

from contacts.admin import ContactInline, AddressInline

from .models import RegionalNaturalReserve, RNRAdministrator
from .models import Natura2000Site, Natura2000SiteOperator


class RNRAdministratorAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [
        AddressInline,
        ContactInline,
    ]


class Natura2000SiteOperatorAdmin(admin.ModelAdmin):  # pylint: disable=R0904
    inlines = [
        AddressInline,
        ContactInline,
    ]


admin.site.register(RegionalNaturalReserve)
admin.site.register(RNRAdministrator, RNRAdministratorAdmin)
admin.site.register(Natura2000Site)
admin.site.register(Natura2000SiteOperator, Natura2000SiteOperatorAdmin)
