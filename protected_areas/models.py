from __future__ import unicode_literals

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from contacts.models import Contact, Address


@python_2_unicode_compatible
class RegionalNaturalReserve(models.Model):
    name = models.CharField(_('name'), max_length=255, unique=True)
    code = models.PositiveIntegerField(_('code'), unique=True)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('regional natural reserve')
        verbose_name_plural = _('regional natural reserves')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class RNRAdministrator(models.Model):
    short_name = models.CharField(_('short_name'), max_length=10)
    name = models.CharField(_('name'), max_length=255)
    address = GenericRelation(Address, verbose_name=_('address'))
    email = models.EmailField(_('e-mail'), max_length=200)
    conservator = GenericRelation(Contact, verbose_name=_('conservator'))
    rnr = models.OneToOneField(RegionalNaturalReserve,
                               verbose_name=_('RNR'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('RNR administrator')
        verbose_name_plural = _('RNR administrators')

    def __str__(self):
        return self.short_name


@python_2_unicode_compatible
class Natura2000Site(models.Model):
    TYPE_CHOICES = (
        ('s', 'SIC'),
        ('z', 'ZPS'),
    )

    name = models.CharField(_('name'), max_length=255, unique=True)
    site_type = models.CharField(_('type'), max_length=1,
                                 choices=TYPE_CHOICES)
    index = models.CharField(_('index'), max_length=9, unique=True)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('natura 2000 site')
        verbose_name_plural = _('natura 2000 sites')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Natura2000SiteOperator(models.Model):
    name = models.CharField(_('name'), max_length=255)
    address = GenericRelation(Address, verbose_name=_('address'))
    email = models.EmailField(_('e-mail'), max_length=200)
    person_in_charge = GenericRelation(Contact,
                                       verbose_name=_('person in charge'))
    site = models.OneToOneField(Natura2000Site,
                                verbose_name=_('natura 2000 site'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('natura 2000 site operator')
        verbose_name_plural = _('natura 2000 site operators')

    def __str__(self):
        return self.name
