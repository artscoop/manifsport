from __future__ import unicode_literals

from django.views.generic import ListView

from protected_areas.models import RegionalNaturalReserve
from protected_areas.models import Natura2000Site


class RegionalNaturalReserveList(ListView):  # pylint: disable=R0901
    model = RegionalNaturalReserve


class Natura2000SiteList(ListView):  # pylint: disable=R0901
    model = Natura2000Site
