from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import Natura2000SiteList
from .views import RegionalNaturalReserveList

urlpatterns = patterns(
    # pylint: disable=E1120
    '',
    url(r'^regionalnaturalreserve/$',
        RegionalNaturalReserveList.as_view(),
        name='regionalnaturalreserve_list'),
    url(r'^natura2000site/$',
        Natura2000SiteList.as_view(),
        name='natura2000site_list'),
    # pylint: enable=E1120
)
